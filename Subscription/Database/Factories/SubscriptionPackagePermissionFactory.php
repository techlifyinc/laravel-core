<?php

namespace Modules\LaravelCore\Subscription\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\LaravelCore\Entities\Permission;
use Modules\LaravelCore\Subscription\Entities\SubscriptionPackagePermission;

class SubscriptionPackagePermissionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubscriptionPackagePermission::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'package_id' => SubscriptionPackageFactory::new()->create()->id,
            'details' => $this->faker->sentence(),
            'permission_id' => Permission::first()->id,
            'creator_id' => 1,
        ];
    }
}