<?php

namespace Modules\LaravelCore\Subscription\Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\LaravelCore\Subscription\Entities\SubscriptionStatus;
use Modules\LaravelCore\Subscription\Entities\TenantSubscription;

class TenantSubscriptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TenantSubscription::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tenant_id' => 1,
            'package_id' => SubscriptionPackageFactory::new()->create()->id,
            'cost_per_unit' => 100,
            'units' => 2,
            'is_unit_limitation_enforced' => false,
            'total' => 200,
            'started_at' => Carbon::now(),
            'ended_at' => Carbon::now()->addMonth(),
            'details' => $this->faker->sentence(),
            'status_id' => SubscriptionStatus::ACTIVE,
            'creator_id' => 1,
        ];
    }
}