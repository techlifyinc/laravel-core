<?php

namespace Modules\LaravelCore\Subscription\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\LaravelCore\Subscription\Entities\SubscriptionPackage;

class SubscriptionPackageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubscriptionPackage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word(),
            'description' => $this->faker->sentence(),
            'is_enabled' => true,
            'code' => $this->faker->unique()->word(),
            'creator_id' => 1,
        ];
    }
}