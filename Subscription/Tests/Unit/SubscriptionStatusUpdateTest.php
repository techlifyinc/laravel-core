<?php

namespace Modules\LaravelCore\Subscription\Tests\Unit;


use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\LaravelCore\Subscription\Database\Factories\TenantSubscriptionFactory;
use Modules\LaravelCore\Subscription\Entities\SubscriptionStatus;
use Modules\LaravelCore\Subscription\Entities\TenantSubscription;
use Modules\LaravelCore\Tests\Unit\TechlifyTestCase;

class SubscriptionStatusUpdateTest extends TechlifyTestCase
{
    use DatabaseTransactions;

    protected $routeName = "tenant-subscriptions.updateStatus";

    protected $loginUserId = 1;

    public TenantSubscription $tenantSubscription;

    protected function setUp(): void
    {
        parent::setUp();

        $this->tenantSubscription = TenantSubscriptionFactory::new()->create();
        $this->routeParameters = ['tenantSubscription' => $this->tenantSubscription->id, 'status' => SubscriptionStatus::INACTIVE];
    }

    public function testInactive()
    {
        $this->authenticateClientAdmin();
        $route = route($this->routeName, $this->routeParameters, false);
        $response = $this->patch($route);
        $result = json_decode($response->getContent())->item;
        $this->assertEquals(SubscriptionStatus::INACTIVE, $result->status_id);
    }

}
