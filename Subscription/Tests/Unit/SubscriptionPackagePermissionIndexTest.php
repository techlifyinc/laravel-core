<?php

namespace Modules\LaravelCore\Subscription\Tests\Unit;


use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\LaravelCore\Subscription\Database\Factories\SubscriptionPackageFactory;
use Modules\LaravelCore\Subscription\Database\Factories\SubscriptionPackagePermissionFactory;
use Modules\LaravelCore\Subscription\Entities\SubscriptionPackagePermission;
use Modules\LaravelCore\Tests\Traits\HasPermissionRestriction;
use Modules\LaravelCore\Tests\Unit\TechlifyIndexBaseTest;

class SubscriptionPackagePermissionIndexTest extends TechlifyIndexBaseTest
{
    use HasPermissionRestriction;
    use DatabaseTransactions;

    protected $routeName = "subscription-package-permissions.index";

    protected $loginUserId = 1;

    protected function setUp(): void
    {
        parent::setUp();

        //Setup filter requirement
        $this->modelFactory = SubscriptionPackagePermissionFactory::class;
        $this->model = SubscriptionPackagePermission::class;
        $packages = [SubscriptionPackageFactory::new()->create()->id, SubscriptionPackageFactory::new()->create()->id];
        $this->filters = [
            [
                'filterKey' => 'package_ids',
                'dbColumn' => 'package_id',
                'dbValue' => $packages,
                'filterFor' => $packages,
                'count' => 1
            ],
        ];
    }
}
