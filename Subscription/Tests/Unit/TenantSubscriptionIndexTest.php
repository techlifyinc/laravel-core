<?php

namespace Modules\LaravelCore\Subscription\Tests\Unit;


use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\LaravelCore\Subscription\Database\Factories\SubscriptionPackageFactory;
use Modules\LaravelCore\Subscription\Database\Factories\TenantSubscriptionFactory;
use Modules\LaravelCore\Subscription\Entities\TenantSubscription;
use Modules\LaravelCore\Tests\Traits\HasPermissionRestriction;
use Modules\LaravelCore\Tests\Unit\TechlifyIndexBaseTest;

class TenantSubscriptionIndexTest extends TechlifyIndexBaseTest
{
    use HasPermissionRestriction;
    use DatabaseTransactions;

    protected $routeName = "tenant-subscriptions.index";

    protected $loginUserId = 1;

    protected function setUp(): void
    {
        parent::setUp();

        //Setup filter requirement
        $this->modelFactory = TenantSubscriptionFactory::class;
        $this->model = TenantSubscription::class;
        $packages = [SubscriptionPackageFactory::new()->create()->id, SubscriptionPackageFactory::new()->create()->id];
        $this->filters = [
            [
                'filterKey' => 'package_ids',
                'dbColumn' => 'package_id',
                'dbValue' => $packages,
                'filterFor' => $packages,
                'count' => 1
            ],
        ];
    }
}
