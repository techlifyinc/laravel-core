<?php

namespace Modules\LaravelCore\Subscription\Tests\Unit;


use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\LaravelCore\Subscription\Database\Factories\SubscriptionPackageFactory;
use Modules\LaravelCore\Subscription\Entities\SubscriptionPackage;
use Modules\LaravelCore\Tests\Unit\TechlifyTestCase;

class SubscriptionPackageToggleTest extends TechlifyTestCase
{
    use DatabaseTransactions;

    protected $routeName = "subscription-packages.toggle";

    protected $loginUserId = 1;

    public SubscriptionPackage $subsciptionPackage;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subsciptionPackage = SubscriptionPackageFactory::new()->create();
        $this->routeParameters = ['subscriptionPackage' => $this->subsciptionPackage->id];
    }

    public function testPackageToggle()
    {
        $this->authenticateClientAdmin();
        $route = route($this->routeName, $this->routeParameters, false);
        $response = $this->patch($route);
        $result = json_decode($response->getContent())->item;
        $this->assertEquals(!$this->subsciptionPackage->is_enabled, $result->is_enabled);
    }

}
