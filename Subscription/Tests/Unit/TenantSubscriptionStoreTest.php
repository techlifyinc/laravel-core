<?php

namespace Modules\LaravelCore\Subscription\Tests\Unit;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Modules\LaravelCore\Subscription\Database\Factories\SubscriptionPackageFactory;
use Modules\LaravelCore\Subscription\Entities\SubscriptionStatus;
use Modules\LaravelCore\Tests\Traits\HasMinimumDataRequirement;
use Modules\LaravelCore\Tests\Traits\HasPermissionRestriction;
use Modules\LaravelCore\Tests\Unit\TechlifyTestCase;

class TenantSubscriptionStoreTest extends TechlifyTestCase
{
    use HasPermissionRestriction;
    use HasMinimumDataRequirement;
    use DatabaseTransactions;

    protected $loginUserId = 1;

    protected $routeName = "tenant-subscriptions.store";
    protected $httpMethod = 'POST';

    protected $minimumData = [];

    protected function setUp(): void
    {
        parent::setUp();

        $this->minimumData = [
            'package_id' => SubscriptionPackageFactory::new()->create()->id,
            'tenant_id' => 1,
            'status_id' => SubscriptionStatus::ACTIVE,
            "started_at" => Carbon::now()->format('Y-m-d'),
        ];
    }


    public function testForTrailFailure()
    {
        $this->authenticateClientAdmin();

        $data = $this->minimumData;
        $data['is_trail'] = true;

        $response = $this->post(
            route($this->routeName),
            $data,
            ['Accept' => 'application/json']
        );

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testForTrailSuccess()
    {
        $this->authenticateClientAdmin();

        $data = $this->minimumData;
        $data['is_trail'] = true;
        $data['trail_started_at'] = Carbon::now()->format('Y-m-d');
        $data['trail_ended_at'] = Carbon::now()->addDays(2)->format('Y-m-d');

        $response = $this->post(
            route($this->routeName),
            $data,
            ['Accept' => 'application/json']
        );

        $response->assertStatus(Response::HTTP_OK);
    }

}
