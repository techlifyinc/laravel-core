<?php

namespace Modules\LaravelCore\Subscription\Tests\Unit;


use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Modules\LaravelCore\Subscription\Database\Factories\SubscriptionPackageFactory;
use Modules\LaravelCore\Subscription\Entities\SubscriptionPackage;
use Modules\LaravelCore\Tests\Traits\HasPermissionRestriction;
use Modules\LaravelCore\Tests\Unit\TechlifyIndexBaseTest;

class SubscriptionPackageIndexTest extends TechlifyIndexBaseTest
{
    use HasPermissionRestriction;
    use DatabaseTransactions;

    protected $routeName = "subscription-packages.index";

    protected $loginUserId = 1;

    protected function setUp(): void
    {
        parent::setUp();

        //Setup filter requirement
        $this->modelFactory = SubscriptionPackageFactory::class;
        $this->model = SubscriptionPackage::class;
        $this->filters = [
            [
                'filterKey' => 'search',
                'dbColumn' => 'title',
                'dbValue' => ['Test-Package', 'Package Test'],
                'filterFor' => 'Test',
                'filterQuery' => DB::table('subscription_packages')
                    ->where('title', 'LIKE', '%Test%')
                    ->orWhere('description', 'LIKE', '%Test%'),
            ],
        ];
    }
}
