<?php

namespace Modules\LaravelCore\Subscription\Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\LaravelCore\Entities\Permission;
use Modules\LaravelCore\Subscription\Database\Factories\SubscriptionPackageFactory;
use Modules\LaravelCore\Tests\Traits\HasMinimumDataRequirement;
use Modules\LaravelCore\Tests\Traits\HasPermissionRestriction;
use Modules\LaravelCore\Tests\Unit\TechlifyTestCase;

class SubscriptionPackagePermissionStoreTest extends TechlifyTestCase
{
    use HasPermissionRestriction;
    use HasMinimumDataRequirement;
    use DatabaseTransactions;

    protected $loginUserId = 1;

    protected $routeName = "subscription-package-permissions.store";
    protected $httpMethod = 'POST';

    protected $minimumData = [];

    protected function setUp(): void
    {
        parent::setUp();

        $this->minimumData = [
            'package_id' => SubscriptionPackageFactory::new()->create()->id,
            'permission_id' => Permission::first()->id,
        ];
    }

}
