<?php
namespace Modules\LaravelCore\Subscription\Entities;

use Illuminate\Database\Eloquent\Model;

class SubscriptionStatus extends Model
{
    protected $table = 'subscription_subscription_statuses';

    const ACTIVE = 1;
    const INACTIVE = 2;

}
