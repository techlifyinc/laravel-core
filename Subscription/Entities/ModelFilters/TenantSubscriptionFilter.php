<?php

namespace Modules\LaravelCore\Subscription\Entities\ModelFilters;

use Carbon\Carbon;
use Modules\LaravelCore\Http\Filters\BaseFilter;

class TenantSubscriptionFilter extends BaseFilter
{
    public function packageIds($packageIds)
    {
        return $this->whereIn('package_id', explode(',', $packageIds));
    }

    public function tenantIds($tenantIds)
    {
        return $this->whereIn('tenant_id', explode(',', $tenantIds));
    }

    public function statusIds($statusIds)
    {
        return $this->whereIn('status_id', explode(',', $statusIds));
    }

    public function fromDate($fromDate)
    {
        $this->whereDate($this->qualifyColumn('started_at'), '>=', Carbon::parse($fromDate)->format('Y-m-d'));
    }

    public function toDate($toDate)
    {
        $this->whereDate($this->qualifyColumn('ended_at'), '<=', Carbon::parse($toDate)->format('Y-m-d'));
    }

}