<?php

namespace Modules\LaravelCore\Subscription\Entities\ModelFilters;

use Modules\LaravelCore\Http\Filters\BaseFilter;

class SubscriptionPackagePermissionFilter extends BaseFilter
{
    public function search($search)
    {
        return $this->where(function ($q) use ($search) {
            $q->whereHas("permission", function ($q1) use ($search) {
                $q1->where('permissions.label', 'LIKE', '%' . $search . '%')
                    ->orWhere('permissions.description', 'LIKE', '%' . $search . '%')
                    ->orWhere('permissions.slug', 'LIKE', '%' . $search . '%');
            });
        });
    }

    public function packageIds($packageIds)
    {
        return $this->whereIn('package_id', explode(',', $packageIds));
    }


    /**
     * Overriden method
     */
    public function sortBy($sortBy)
    {
        $sort = explode("|", $sortBy);

        switch ($sort[0]) {

            case 'permission':
                $this->join(
                    'permissions',
                    'subscription_package_permissions.permission_id',
                    '=',
                    'permissions.id'
                )
                    ->select('subscription_package_permissions.*')
                    ->orderBy('permissions.label', $sort[1]);
                break;
            default:
                $this->orderBy($this->qualifyColumn($sort[0]), $sort[1]);
                break;
        }
    }
}