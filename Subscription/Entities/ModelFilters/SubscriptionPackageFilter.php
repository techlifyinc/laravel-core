<?php

namespace Modules\LaravelCore\Subscription\Entities\ModelFilters;

use Modules\LaravelCore\Http\Filters\BaseFilter;

class SubscriptionPackageFilter extends BaseFilter
{
    public function search($search)
    {
        return $this->where(function ($q) use ($search) {
            $q->where('title', 'LIKE', '%' . $search . '%')
                ->orWhere('description', 'LIKE', '%' . $search . '%');
        });
    }
}