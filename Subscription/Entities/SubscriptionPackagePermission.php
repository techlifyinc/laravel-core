<?php
namespace Modules\LaravelCore\Subscription\Entities;

use App\Models\User;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\LaravelCore\Entities\Permission;
use Modules\LaravelCore\Subscription\Database\Factories\SubscriptionPackagePermissionFactory;
use Modules\LaravelCore\Subscription\Entities\ModelFilters\SubscriptionPackagePermissionFilter;

class SubscriptionPackagePermission extends Model
{
    use Filterable;
    use HasFactory;
    use SoftDeletes;

    protected $table = 'subscription_package_permissions';

    protected static function newFactory(): Factory
    {
        return SubscriptionPackagePermissionFactory::new();
    }

    public function modelFilter()
    {
        return $this->provideFilter(SubscriptionPackagePermissionFilter::class);
    }

    public function permission()
    {
        return $this->belongsTo(Permission::class, 'permission_id', 'id');
    }

    public function package()
    {
        return $this->belongsTo(SubscriptionPackage::class, 'package_id', 'id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

}
