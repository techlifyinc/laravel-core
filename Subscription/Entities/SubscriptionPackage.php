<?php
namespace Modules\LaravelCore\Subscription\Entities;

use App\Models\User;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\LaravelCore\Subscription\Database\Factories\SubscriptionPackageFactory;
use Modules\LaravelCore\Subscription\Entities\ModelFilters\SubscriptionPackageFilter;
use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\LaravelCore\Entities\Client;
use Modules\LaravelCore\Entities\Permission;

class SubscriptionPackage extends Model
{
    use Filterable;
    use HasFactory;
    use SoftDeletes;

    protected $table = 'subscription_packages';

    protected $casts = ['is_enabled' => 'boolean'];

    protected static function newFactory(): Factory
    {
        return SubscriptionPackageFactory::new();
    }

    public function modelFilter()
    {
        return $this->provideFilter(SubscriptionPackageFilter::class);
    }
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function permissions()
    {
        return $this->hasManyThrough(Permission::class, SubscriptionPackagePermission::class, 'package_id', 'id', 'id', 'permission_id');
    }

    public function tenants()
    {
        return $this->hasManyThrough(Client::class, TenantSubscription::class, 'package_id', 'id', 'id', 'tenant_id');
    }

}
