<?php
namespace Modules\LaravelCore\Subscription\Entities;

use App\Models\User;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\LaravelCore\Entities\Client;
use Modules\LaravelCore\Subscription\Database\Factories\TenantSubscriptionFactory;
use Modules\LaravelCore\Subscription\Entities\ModelFilters\TenantSubscriptionFilter;

class TenantSubscription extends Model
{
    use Filterable;
    use HasFactory;
    use SoftDeletes;

    protected $table = 'subscription_tenant_subscriptions';

    protected $casts = [
        'is_unit_limitation_enforced' => 'boolean',
        'is_trail' => 'boolean',
    ];

    protected static function newFactory(): Factory
    {
        return TenantSubscriptionFactory::new();
    }

    public function modelFilter()
    {
        return $this->provideFilter(TenantSubscriptionFilter::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function tenant()
    {
        return $this->belongsTo(Client::class, 'tenant_id', 'id');
    }

    public function package()
    {
        return $this->belongsTo(SubscriptionPackage::class, 'package_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(SubscriptionStatus::class, 'status_id', 'id');
    }

}
