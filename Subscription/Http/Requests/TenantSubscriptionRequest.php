<?php

namespace Modules\LaravelCore\Subscription\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Closure;
use Illuminate\Validation\Rule;
use Modules\LaravelCore\Entities\Client;

class TenantSubscriptionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "package_id" => "required|exists:subscription_packages,id",
            "tenant_id" => [
                "required",
                function (string $attribute, mixed $value, Closure $fail) {
                    //Since client table will be on different DB connection, we need to query through model to avoid conncection issues
                    $tenant = Client::find($value);
                    if (!$tenant) {
                        $fail("The {$attribute} is invalid.");
                    }
                }
            ],
            "cost_per_unit" => "nullable|numeric",
            "units" => "nullable|numeric",
            "total" => "nullable|numeric",
            "is_trail" => "nullable|boolean",
            "trail_started_at" => ["nullable", "date", Rule::requiredIf(request('is_trail', false))],
            "trail_ended_at" => ["nullable", "date", Rule::requiredIf(request('is_trail', false))],
            "started_at" => ["nullable", "date", Rule::requiredIf(!request('is_trail', false))],
            "ended_at" => "nullable|date",
            "status_id" => "required|exists:subscription_subscription_statuses,id"
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}