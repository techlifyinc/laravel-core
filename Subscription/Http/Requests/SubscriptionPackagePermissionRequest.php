<?php

namespace Modules\LaravelCore\Subscription\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscriptionPackagePermissionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "package_id" => "required|exists:subscription_packages,id",
            "permission_id" => "required|exists:permissions,id",
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}