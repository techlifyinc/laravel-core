<?php

namespace Modules\LaravelCore\Subscription\Http\Controllers;

use Modules\LaravelCore\Http\Controllers\BaseController;
use Modules\LaravelCore\Subscription\Entities\SubscriptionPackage;

class SubscriptionPackageController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return mixed ["data"=>[results]]
     */
    public function index()
    {
        $filters = request([
            'search',
            'sort_by',
        ]);

        $builder = SubscriptionPackage::filter($filters)
            ->withCount(['permissions', 'tenants'])
            ->with(['creator']);
        return $this->getPaginatedResults($builder);
    }


    public function show(SubscriptionPackage $subscriptionPackage)
    {
        $subscriptionPackage->load(['creator']);
        $subscriptionPackage->loadCount(['permissions', 'tenants']);
        return ['item' => $subscriptionPackage];
    }


    public function toggle(SubscriptionPackage $subscriptionPackage)
    {
        $subscriptionPackage->is_enabled = !$subscriptionPackage->is_enabled;
        $subscriptionPackage->save();
        return ['item' => $subscriptionPackage];
    }

}