<?php

namespace Modules\LaravelCore\Subscription\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response;
use Modules\LaravelCore\Http\Controllers\BaseController;
use Modules\LaravelCore\Subscription\Entities\SubscriptionStatus;
use Modules\LaravelCore\Subscription\Entities\TenantSubscription;
use Modules\LaravelCore\Subscription\Http\Requests\TenantSubscriptionRequest;

class TenantSubscriptionController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return mixed ["data"=>[results]]
     */
    public function index()
    {
        $filters = request([
            'sort_by',
            'package_ids',
            'status_ids',
            'tenant_ids'
        ]);

        $builder = TenantSubscription::filter($filters)
            ->with(['tenant', 'package', 'status', 'creator']);
        return $this->getPaginatedResults($builder);
    }

    public function store(TenantSubscriptionRequest $request)
    {
        try {
            $tenantSubscription = new TenantSubscription();
            $tenantSubscription->tenant_id = $request->tenant_id;
            $tenantSubscription->package_id = $request->package_id;
            $tenantSubscription->cost_per_unit = $request->cost_per_unit ?? null;
            $tenantSubscription->units = $request->units ?? (($tenantSubscription->cost_per_unit != null) ? 1 : null);
            $tenantSubscription->total = ($tenantSubscription->cost_per_unit != null && $tenantSubscription->units != null) ? $tenantSubscription->cost_per_unit * $tenantSubscription->units : null;
            $tenantSubscription->status_id = $request->status_id;
            $tenantSubscription->is_unit_limitation_enforced = $request->is_unit_limitation_enforced ?? false;
            $tenantSubscription->is_trail = $request->is_trail ?? false;
            $tenantSubscription->details = $request->details ?? '';
            $tenantSubscription->trail_started_at = $request->trail_started_at != null ? Carbon::parse($request->trail_started_at)->format('Y-m-d') : null;
            $tenantSubscription->trail_ended_at = $request->trail_ended_at != null ? Carbon::parse($request->trail_ended_at)->format('Y-m-d') : null;
            $tenantSubscription->started_at = $request->started_at != null ? Carbon::parse($request->started_at)->format('Y-m-d') : null;
            $tenantSubscription->ended_at = $request->ended_at != null ? Carbon::parse($request->ended_at)->format('Y-m-d') : null;
            $tenantSubscription->creator_id = auth()->id();
            $tenantSubscription->save();
            return ["item" => $tenantSubscription];
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    public function update(TenantSubscriptionRequest $request, TenantSubscription $tenantSubscription)
    {
        try {
            $tenantSubscription->tenant_id = $request->tenant_id;
            $tenantSubscription->package_id = $request->package_id;
            $tenantSubscription->cost_per_unit = $request->cost_per_unit ?? null;
            $tenantSubscription->units = $request->units ?? (($tenantSubscription->cost_per_unit != null) ? 1 : null);
            $tenantSubscription->total = ($tenantSubscription->cost_per_unit != null && $tenantSubscription->units != null) ? $tenantSubscription->cost_per_unit * $tenantSubscription->units : null;
            $tenantSubscription->status_id = $request->status_id;
            $tenantSubscription->is_unit_limitation_enforced = $request->is_unit_limitation_enforced ?? false;
            $tenantSubscription->is_trail = $request->is_trail ?? false;
            $tenantSubscription->details = $request->details ?? '';
            $tenantSubscription->trail_started_at = $request->trail_started_at != null ? Carbon::parse($request->trail_started_at)->format('Y-m-d') : null;
            $tenantSubscription->trail_ended_at = $request->trail_ended_at != null ? Carbon::parse($request->trail_ended_at)->format('Y-m-d') : null;
            $tenantSubscription->started_at = $request->started_at != null ? Carbon::parse($request->started_at)->format('Y-m-d') : null;
            $tenantSubscription->ended_at = $request->ended_at != null ? Carbon::parse($request->ended_at)->format('Y-m-d') : null;
            $tenantSubscription->save();
            return ["item" => $tenantSubscription];
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy(TenantSubscription $tenantSubscription)
    {
        try {
            $tenantSubscription->delete();
            return ["item" => $tenantSubscription];
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function updateStatus(TenantSubscription $tenantSubscription, SubscriptionStatus $status)
    {
        try {
            $tenantSubscription->status_id = $status->id;
            $tenantSubscription->save();
            $tenantSubscription->load('status');
            return ["item" => $tenantSubscription];
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}