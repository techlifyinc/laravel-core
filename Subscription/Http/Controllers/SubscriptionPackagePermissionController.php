<?php

namespace Modules\LaravelCore\Subscription\Http\Controllers;

use Exception;
use Illuminate\Http\Response;
use Modules\LaravelCore\Http\Controllers\BaseController;
use Modules\LaravelCore\Subscription\Entities\SubscriptionPackagePermission;
use Modules\LaravelCore\Subscription\Http\Requests\SubscriptionPackagePermissionRequest;

class SubscriptionPackagePermissionController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return mixed ["data"=>[results]]
     */
    public function index()
    {
        $filters = request([
            'search',
            'sort_by',
            'package_ids'
        ]);

        $builder = SubscriptionPackagePermission::filter($filters)
            ->with(['permission', 'package', 'creator']);
        return $this->getPaginatedResults($builder);
    }

    public function store(SubscriptionPackagePermissionRequest $request)
    {
        try {
            $packagePermission = new SubscriptionPackagePermission();
            $packagePermission->package_id = $request->package_id;
            $packagePermission->permission_id = $request->permission_id;
            $packagePermission->details = $request->details ?? '';
            $packagePermission->creator_id = auth()->id();
            $packagePermission->save();
            return ["item" => $packagePermission];
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }


    public function update(SubscriptionPackagePermissionRequest $request, SubscriptionPackagePermission $packagePermission)
    {
        try {
            $packagePermission->package_id = $request->package_id;
            $packagePermission->permission_id = $request->permission_id;
            $packagePermission->details = $request->details ?? '';
            $packagePermission->save();
            return ["item" => $packagePermission];
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy(SubscriptionPackagePermission $packagePermission)
    {
        try {
            $packagePermission->delete();
            return ["item" => $packagePermission];
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}