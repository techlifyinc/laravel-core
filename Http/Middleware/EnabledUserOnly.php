<?php

namespace Modules\LaravelCore\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Modules\LaravelCore\Entities\ClientStatus;
use Modules\LaravelCore\Entities\UserClient;

class EnabledUserOnly
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        // If not logged in, we don't need to check if a user is enabled or not
        // With this check, we can apply this middleware on public routes as well.
        if (!auth()->check()) {
            return $next($request);
        }
        // check if user has any active client record.
        $user = auth()->user();
        $userClient = UserClient::where('user_id', $user->id)
            ->where('client_id', $user->client_id)
            ->where('is_enabled', true)
            ->whereHas('client', function ($q) {
                $q->where('status_id', ClientStatus::ACTIVE);
            })
            ->first();
        if (!$userClient) {
            $userClient = UserClient::where('user_id', $user->id)
                ->where('is_enabled', true)
                ->whereHas('client', function ($q) {
                    $q->where('status_id', ClientStatus::ACTIVE);
                })
                ->first();
        }

        if (!$userClient) {
            return response()->json(
                ['error' => "Your account has been locked, please contact your System Administrator."],
                403
            );
        }
        return $next($request);
    }
}
