<?php

namespace Modules\LaravelCore\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\LaravelCore\Entities\RestrictorType;
use Modules\LaravelCore\Http\Helpers\RestrictorHelper;
use Throwable;

class TechlifyAccessRestrictor
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Throwable
     */
    public function handle(Request $request, Closure $next): mixed
    {
        // If a user is not authenticated, no restrictor is applied
        if (!auth()->check()) {
            return $next($request);
        }

        // Load all relevant restrictors for the user
        $restrictors = RestrictorHelper::loadRelevantRestrictors();
        if (count($restrictors) == 0) {
            // return the request if no restrictors are applied.
            return $next($request);
        }
        // Get the time based restrictors
        $timeBasedRestrictors = $restrictors->where('type_id', RestrictorType::TYPE_TIME_BASED);
        $isTimeBasedAllowed = true;
        if (count($timeBasedRestrictors) > 0) {
            $isTimeBasedAllowed = RestrictorHelper::allowTimeBasedAccess($timeBasedRestrictors);
        }
        // Get the ip based restrictors
        $ipBasedRestrictors = $restrictors->where('type_id', RestrictorType::TYPE_IP_BASED);
        $isIpBasedAllowed = true;
        if (count($ipBasedRestrictors) > 0) {
            $isIpBasedAllowed = RestrictorHelper::allowIpBasedAccess($ipBasedRestrictors);
        }

        if ($isIpBasedAllowed && $isTimeBasedAllowed) {
            return $next($request);
        }

        return response()->json(['error' => "You're restricted to perform this action."], 403);
    }
}
