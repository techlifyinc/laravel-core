<?php

namespace Modules\LaravelCore\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RestrictorRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'type_id' => 'required|exists:techlify_rbac_restrictor_types,id',
            'start_time' => 'nullable|required_if:type_id,1|date_format:Y-m-d H:i:s',
            'end_time' => 'nullable|required_if:type_id,1|date_format:Y-m-d H:i:s',
            'ip_range' => 'required_if:type_id,2',
            'level_id' => 'required:exists:techlify_rbac_restrictor_levels,id',
            'restricted_object_id' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function messages(): array
    {
        return [
            'type_id.required' => 'The restrictor type field is required.',
            'level_id.required' => 'The restrictor level field is required.',
            'restricted_object_id' => 'The Object field is required.',
            'ip_range.required_if' => 'The IP Range field is required when type is IP Based.',
            'start_time.required_if' => 'The start time field is required when type is Time Based.',
            'end_time.required_if' => 'The end time field is required when type is Time Based.',
        ];
    }
}
