<?php

namespace Modules\LaravelCore\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\Validator;
use Modules\LaravelCore\Entities\LoginAttemptService;
use Modules\LaravelCore\Entities\RbacClientConfigService;
use Modules\LaravelCore\Rules\NotSameAsEmail;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        // setup password rule
        $config = RbacClientConfigService::fetchCurrentClientConfig(auth()?->user()?->client_id);
        $passwordRule = Password::min($config->min_password_length)
            ->uncompromised();
        if ($config->is_password_require_number) {
            $passwordRule = $passwordRule->numbers();
        }
        if ($config->is_password_require_symbol) {
            $passwordRule = $passwordRule->symbols();
        }
        if ($config->is_password_require_capital_letter) {
            $passwordRule = $passwordRule->mixedCase();
        }

        return [
            "current_password" => [
                "required",
                "current_password:api",
            ],
            "password" => [
                "required",
                "confirmed",
                new NotSameAsEmail(auth()?->user()?->email),
                $passwordRule,
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function withValidator(Validator $validator): void
    {
        $messages = $this->messages();
        if ($validator->errors()->has('current_password')) {
            $user = User::find(auth()->id());
            // User entered incorrect password, let's populate the remaining attempts left error.
            LoginAttemptService::logUserAttempt($user, false);
            $attemptsLeft = LoginAttemptService::numberOfLoginAttemptsRemaining($user);
            if ($attemptsLeft > 0) {
                $message = 'The password entered is incorrect, you have '.$attemptsLeft.
                    ' more attempts. Your account would be locked afterwards.';
            } else {
                $message = 'Your account has been blocked. Please contact your system administrator.';
            }
            $messages['current_password'] = $message;
        }
        $validator->setCustomMessages($messages);
    }

    public function messages(): array
    {
        return [
            'current_password' => 'The current password is invalid.',
        ];
    }
}
