<?php

namespace Modules\LaravelCore\Http\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Modules\LaravelCore\Entities\TechlifyImport;
use Modules\LaravelCore\Entities\TechlifyImportEntity;

trait Importable
{
    /**
     * Get the import entries.
     *
     * @return MorphMany
     */
    public function importEntities(): MorphMany
    {
        return $this->morphMany(TechlifyImportEntity::class, 'model');
    }

    /**
     * Create a new TechlifyImport record.
     *
     * @return TechlifyImport
     */
    public function initImport(): TechlifyImport
    {
        // Create import record.
        $import = new TechlifyImport();
        $import->date = Carbon::now();
        $import->client_id = auth()->user()->client_id;
        $import->creator_id = auth()->id();
        $import->save();

        return $import;
    }

    /**
     * Create import entity record.
     *
     * @param TechlifyImport $import
     * @param string $modelType
     * @param Model $model
     * @return TechlifyImportEntity
     */
    public function createImportEntity(TechlifyImport $import, string $modelType, Model $model): TechlifyImportEntity
    {
        $importEntity = new TechlifyImportEntity();
        $importEntity->import_id = $import->id;
        $importEntity->model_type = $modelType;
        $importEntity->model_id = $model?->id;
        $importEntity->raw_data = $model;
        $importEntity->save();

        return $importEntity;
    }
}
