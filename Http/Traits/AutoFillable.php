<?php

namespace Modules\LaravelCore\Http\Traits;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * A trait used in controller to fill the model data from request.
 */
trait AutoFillable
{
    /**
     * Autofill the necessary fields directly from request.
     *
     * @param Model $model
     * @param Request $request
     * @param array $excludeFields
     * @return Model
     * @throws Exception
     */
    protected function fillModelFromRequest(Model $model, Request $request, array $excludeFields = []): Model
    {
        $fillable = $model->getFillable();

        if (!is_array($fillable) || empty($fillable)) {
            throw new Exception('Fillable array is empty or not defined in the model.');
        }

        // Merge the fields to exclude from the request with predefined fields
        $fieldsToRemove = array_merge($excludeFields, ['creator_id', 'client_id']);
        $fillable = array_diff($fillable, $fieldsToRemove);
        $data = $request->only($fillable);
        $model->fill($data);

        return $model;
    }
}
