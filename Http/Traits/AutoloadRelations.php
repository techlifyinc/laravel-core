<?php

namespace Modules\LaravelCore\Http\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait AutoloadRelations
{
    /**
     * Automatically loads requested relations based on the include parameter in the request.
     *
     * @return Builder
     */
    public function newQuery(): Builder
    {
        $query = parent::newQuery();
        $query->with($this->getRelationsToLoad());
        $query->withCount($this->getRelationAggregateToLoad());

        return $query;
    }

    /**
     * Autoload the relation and aggregate count after save.
     *
     * @param array $options
     * @return $this
     */
    public function save(array $options = []): static
    {
        parent::save($options);
        $this->load($this->getRelationsToLoad());
        $this->loadCount($this->getRelationAggregateToLoad());

        return $this;
    }

    /**
     * Autoload the relation and aggregate count after create.
     *
     * @param array $attributes
     * @return Model $model
     */
    public static function create(array $attributes = []): Model
    {
        $model = parent::create($attributes);
        $model->load($model->getRelationsToLoad());
        $model->loadCount($model->getRelationAggregateToLoad());

        return $model;
    }

    /**
     * Autoload the relation and aggregate count after update.
     *
     * @param array $attributes
     * @param array $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = []): bool
    {
        $result = parent::update($attributes, $options);

        if ($result) {
            $this->load($this->getRelationsToLoad());
            $this->loadCount($this->getRelationAggregateToLoad());
        }

        return $result;
    }

    /**
     * Get the relations to be loaded.
     *
     * @return array
     */
    public function getRelationsToLoad(): array
    {
        return $this->getRelationsForKey('with');
    }

    public function getRelationsForKey($key): array
    {
        if (request()->filled($key)) {
            $allowedRelations = $this->getAllowedRelations();
            $requestedRelations = explode(',', request()->get($key, ''));
            return array_intersect($requestedRelations, $allowedRelations);
        }
        return [];
    }


    /**
     * Get the aggregate to be loaded.
     *
     * @return array
     */
    public function getRelationAggregateToLoad(): array
    {
        return $this->getRelationsForKey('withCount');
    }


    /**
     * Get the allowed relations for autoload.
     *
     * @return array
     */
    abstract protected function getAllowedRelations(): array;
}
