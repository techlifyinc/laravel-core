<?php

namespace Modules\LaravelCore\Http\Helpers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use Modules\LaravelCore\Entities\Restrictor;
use Modules\LaravelCore\Entities\RestrictorLevel;
use Modules\LaravelCore\Entities\RestrictorStatus;
use Modules\LaravelCore\Entities\Role;
use Modules\LaravelCore\Entities\TechlifyUser;
use Modules\LaravelCore\Entities\UserType;
use Throwable;

class RestrictorHelper
{

    /**
     * Get the restrictable type from the level id.
     *
     * @param $levelId
     * @return string
     */
    public static function getRestrictableTypeFromNumber($levelId): string
    {
        return match ($levelId) {
            RestrictorLevel::USER_TYPE => UserType::class,
            RestrictorLevel::ROLE => Role::class,
            RestrictorLevel::INDIVIDUAL => TechlifyUser::class,
            default => TechlifyUser::class
        };
    }

    /**
     * Get the relevant restrictors for the current user.
     *
     * @return Collection
     */
    public static function loadRelevantRestrictors(): Collection
    {
        $user = auth()->user();

        return Restrictor::where('status_id', RestrictorStatus::ACTIVE)
            ->where(function ($query) use ($user) {
                $query->where(function ($q) use ($user) {
                    $q->where('restrictable_type', UserType::class)
                        ->where('restrictable_id', $user->user_type_id);
                })->orWhere(function ($q) use ($user) {
                    $q->where('restrictable_type', Role::class)
                        ->whereIn('restrictable_id', $user->roles()->pluck('roles.id'));
                })->orWhere(function ($q) use ($user) {
                    $q->where('restrictable_type', TechlifyUser::class)
                        ->where('restrictable_id', $user->id);
                });
            })
            ->with('type')
            ->get();
    }


    /**
     * If the current time is within the restriction's time range, allow access.
     *
     * @param Collection $restrictors
     * @return bool true if the access allowed, false otherwise
     */
    public static function allowTimeBasedAccess(Collection $restrictors): bool
    {
        foreach ($restrictors as $restrictor) {
            $startTime = Carbon::parse($restrictor->data['start_time']);
            $endTime = Carbon::parse($restrictor->data['end_time']);
            if (Carbon::now()->between($startTime, $endTime)) {
                Log::debug('checking for '. $startTime .' and '. $endTime . ' and it allowed.');
                return true;
            }
        }
        return false;
    }

    /**
     * Allow access to user if ip address falls in the allowed range.
     *
     * @param Collection $restrictors
     * @return bool  true if the restrictor is applied
     * @throws Throwable
     */
    public static function allowIpBasedAccess(Collection $restrictors): bool
    {
        foreach ($restrictors as $restrictor) {
            if (RestrictorHelper::isRequestIpInRange($restrictor->data['ip_range'])) {
                Log::debug('checking for '. request()->ip().' and it allowed in range.'. $restrictor->data['ip_range']);
                return true;
            }
        }
        return false;
    }

    /**
     * Check if a given ip is in a network
     *
     * @param string $range IP/CIDR netmask eg. 127.0.0.0/24, also 127.0.0.1 is accepted and /32 assumed
     * @return boolean true if the ip is in this range / false if not.
     * @throws Throwable
     */
    public static function isRequestIpInRange(string $range): bool
    {
        try {
            // For testing use this $ip = request()->header('X-Forwarded-For') ?? request()->ip();
            $ip = request()->ip();
            // Validate for localhost IP and return true if the request is from the same server.
            if ($ip == '::1') {
                return true;
            }
            if (!strpos($range, '/')) {
                $range .= '/32';
            }
            // $range is in IP/CIDR format eg 127.0.0.1/24
            list($range, $netmask) = explode('/', $range, 2);
            $range_decimal = ip2long($range);
            $ip_decimal = ip2long($ip);
            $wildcard_decimal = pow(2, (32 - $netmask)) - 1;
            $netmask_decimal = ~$wildcard_decimal;
            return (($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal));
        } catch (Throwable $th) {
            throw $th;
        }
    }
}
