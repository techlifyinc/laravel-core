<?php

namespace Modules\LaravelCore\Http\Filters;

use EloquentFilter\ModelFilter;

class RestrictorFilter extends ModelFilter
{
    protected $drop_id = false;

    /**
     * Apply filter by status id.
     *
     * @param $val
     * @return RestrictorFilter
     */
    public function statusIds($val): RestrictorFilter
    {
        return $this->whereIn('status_id', explode(',', $val));
    }

    /**
     * Apply filter by type id.
     *
     * @param $val
     * @return RestrictorFilter
     */
    public function typeIds($val): RestrictorFilter
    {
        return $this->whereIn('type_id', explode(',', $val));
    }

    /**
     * Apply filter by level id.
     *
     * @param $val
     * @return RestrictorFilter
     */
    public function levelIds($val): RestrictorFilter
    {
        return $this->whereIn('level_id', explode(',', $val));
    }

    /**
     * Apply sort on restrictors.
     *
     * @param $val
     * @return RestrictorFilter
     */
    public function sortBy($val): RestrictorFilter
    {
        $column = 'id';
        $direction = 'desc';
        $sortBy = explode('|', $val);
        if (count($sortBy) > 1) {
            $column = $sortBy[0];
            $direction = $sortBy[1];
        }
        return $this->orderBy($column, $direction);
    }
}
