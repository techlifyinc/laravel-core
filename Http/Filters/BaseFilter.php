<?php

namespace Modules\LaravelCore\Http\Filters;

use EloquentFilter\ModelFilter;

class BaseFilter extends ModelFilter
{

    /**
     * Whether to drop the _id suffix from filters
     */
    protected $drop_id = false;

    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    /**
     * Any basic setups needed
     * Provides a default setup functionality, override it if required
     */
    public function setup()
    {
        // If we don't have a sort option set, let's setup one
        if ($this->isSortEmpty()) {
            $this->input['sort_by'] = "id|DESC";
        }
    }

    public function importId($val)
    {
        return $this->whereHas('importEntities', function ($q) use ($val) {
            $q->where('import_id', $val);
        });
    }

    /**
     * Limit the number of items returning
     *
     * @param $numItems
     *
     * @return mixed
     */
    public function numItems($numItems)
    {
        $this->limit($numItems);
    }

    /**
     * Check if a given input is a valid date
     *
     * @param $myDateString
     *
     * @return mixed
     */
    public function checkIsAValidDate($myDateString): bool
    {
        return (bool) strtotime($myDateString);
    }

    /**
     * Filter the model without the ids
     * @param $ids
     * @return mixed
     */
    public function ignoreIds($ids)
    {
        $ids = explode(',', $ids);
        $this->whereNotIn('id', $ids);
    }

    /**
     * Filter the model with the ids
     * @param $ids
     * @return mixed
     */
    public function includeIds($ids)
    {
        $ids = explode(',', $ids);
        $this->whereIn('id', $ids);
    }

    /**
     * Filter the model with the creator_ids
     * @param $ids
     * @return mixed
     */
    public function creatorIds($creatorIds)
    {
        $ids = explode(',', $creatorIds);
        $this->whereIn('creator_id', $ids);

    }

    /**
     * @param $sort
     * Provides a default sort functionality, override it if required
     */
    public function sortBy($sort)
    {
        $sort = explode('|', $sort);
        $column = 'id';
        $direction = 'desc';
        if (count($sort) > 1) {
            $column = $sort[0];
            $direction = $sort[1];
        }
        return $this->orderBy($column, $direction);
    }

    /**
     * @return boolean is sort empty
     * Check and resolve if we need a custom default sort
     */
    public function isSortEmpty()
    {
        return !isset($this->input['sort_by']) && !($this->input['skip_sort'] ?? false);
    }
}
