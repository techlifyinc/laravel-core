<?php

namespace Modules\LaravelCore\Http\Filters;

use EloquentFilter\ModelFilter;

class TechlifyImportFilter extends ModelFilter
{
    /**
     * Apply filter on model_type column.
     *
     * @param $val
     * @return TechlifyImportFilter
     */
    public function modelType($val): TechlifyImportFilter
    {
        return $this->whereHas('records', function ($q) use ($val) {
            $q->where('model_type', $val);
        });
    }

    /**
     * Apply sort on restrictors.
     *
     * @param $val
     * @return TechlifyImportFilter
     */
    public function sortBy($val): TechlifyImportFilter
    {
        $column = 'id';
        $direction = 'desc';
        $sortBy = explode('|', $val);
        if (count($sortBy) > 1) {
            $column = $sortBy[0];
            $direction = $sortBy[1];
        }
        return $this->orderBy($column, $direction);
    }
}
