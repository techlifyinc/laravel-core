<?php

namespace Modules\LaravelCore\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\Passport;
use Modules\LaravelCore\Emails\ForgotPassword;
use Modules\LaravelCore\Emails\NewCompanyUserEmail;
use Modules\LaravelCore\Emails\WelcomeMail;
use Modules\LaravelCore\Entities\Client;
use Modules\LaravelCore\Entities\ClientStatus;
use Modules\LaravelCore\Entities\Module;
use Modules\LaravelCore\Entities\ModuleUser;
use Modules\LaravelCore\Entities\Permission;
use Modules\LaravelCore\Entities\Role;
use Modules\LaravelCore\Entities\UserClient;
use Modules\LaravelCore\Entities\UserType;
use Modules\LaravelCore\Events\ClientCreatedEvent;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filters = request([
            'name',
            'email',
            'enabled',
            'role_ids',
            'client_id',
            'sort_by',
            'num_items',
            'not_in_module_id',
            'relations',
            'user_type_ids',
            'search',
        ]);

        $parts = [];
        if (isset($filters['num_items']) && "" != trim($filters['num_items'])) {
            $parts = explode("|", $filters['num_items']);
        }

        $clientId = isset($filters['client_id']) && is_numeric($filters['client_id']) ? $filters['client_id'] : auth()->user()->client_id;

        $relations = explode(",", request('relations', ''));

        if (!$relations[0]) {
            $users = User::filter($filters);
        } else {
            $users = User::filter($filters)
                ->with($relations)
                ->with([
                    'roles' => function ($q) use ($clientId) {
                        $q->wherePivot('client_id', $clientId);
                    },
                    'userClient' => function ($q) use ($clientId) {
                        $q->where('client_id', $clientId);
                    },
                ]);
        }

        $users = $users->paginate(count($parts) ? $parts[0] : 25, ['*'], 'page', count($parts) > 1 ? $parts[1] : 1);

        // Set dynamic appends
        if (request('appends')) {
            $appends = explode(',', request('appends'));
            $users->append($appends);
        }

        return response($users);
    }

    public function findOrCreateUserClient($userId, $clientId, $userTypeId)
    {
        $userClient = UserClient::where('user_id', $userId)
            ->where('client_id', $clientId)
            ->first();

        if (!$userClient) {
            $userClient = new UserClient();
            $userClient->user_id = $userId;
            $userClient->client_id = $clientId;
            $userClient->creator_id = auth()?->id();
            $userClient->is_enabled = 1;
            $userClient->enabled_on = now();
        }

        $userClient->user_type_id = $userTypeId;
        $userClient->save();

        return $userClient;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "name" => "required|string",
            "email" => "required|email",
        ];

        if (auth()->user()->user_type_id == UserType::APP_ADMIN) {
            $rules["client_id"] = "nullable|exists:clients,id"; //Client_id and usertype_id should be nullable since some apps doesn't have clients or usertype
            $rules["user_type_id"] = "nullable|numeric|min:2";
        }

        $this->validate(request(), $rules);

        $user = User::where('email', request('email'))
            ->first();
        $clientId = auth()->user()->user_type_id == UserType::APP_ADMIN ? request('client_id', auth()->user()->client_id) : auth()->user()->client_id;
        $isExistingUser = true;
        $password = '';
        if ($user) {
            foreach ($user->clients as $client) {
                if ($client->client_id == $clientId) {
                    return response()->json(['error' => "User already exists."], 422);
                }
            }
        } else {
            $isExistingUser = false;
            $user = new User();
            $user->name = request('name');
            $user->email = request('email');
            $password = request('password') ?? $this->randomPassword();
            $user->password = bcrypt($password);
            $user->user_type_id = request('user_type_id', UserType::CLIENT_USER); //if there is no user type id specified, it will fallback to normal client user.
            $user->is_temporary_password = true;
            $user->signature = request('signature', '');
            $user->creator_id = auth()->id();

            if (request('phone')) {
                $user->phone = request('phone', null);
            }

            $user->client_id = $clientId;

            if (!$user->save()) {
                return response()->json(['error' => "Failed to add the new user. "], 422);
            }
        }

        $userClient = new UserClient();
        $userClient->user_id = $user->id;
        $userClient->user_type_id = request('user_type_id', UserType::CLIENT_USER);
        $userClient->client_id = $clientId;
        $userClient->creator_id = auth()?->id();
        $userClient->is_enabled = 1;
        $userClient->enabled_on = now();
        $userClient->save();

        $roles = request('roles') ?: [];
        if (is_array($roles)) {
            foreach ($roles as $rid => $selected) {
                if (!$selected) {
                    continue;
                }
                $role = Role::find($rid);
                $user->assignRole($role->slug, $clientId);
            }
        }

        $modules = Module::get();
        if ($isExistingUser) {
            $client = Client::find($clientId);
            Mail::to($user->email)->queue(new NewCompanyUserEmail($user, $modules, $client));
        } else {
            $user->loadMissing(['client']);
            Mail::to($user->email)->queue(new WelcomeMail($user, $modules, $password));
        }
        return ["item" => $user];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $clientId = request('client_id', auth()->user()->client_id);

        if (!$user) {
            return response()->json(['error' => "Invalid data sent. "], 422);
        }
        $user->load([
            'type',
            'roles' => function ($q) use ($clientId) {
                $q->wherePivot('client_id', $clientId);
            },
            'userClient' => function ($q) use ($clientId) {
                $q->where('client_id', $clientId);
            }
        ]);
        return ["item" => $user];
    }

    public function currentUser()
    {
        $id = auth()->id();
        $user = \Illuminate\Support\Facades\Auth::user();

        if (null == $user) {
            return ["user" => new User()];
        }

        $permissions = new \Illuminate\Database\Eloquent\Collection();

        if ($user->user_type_id === UserType::CLIENT_ADMIN) {
            $permissions = Permission::all();
        } else {
            $roles = $user->roles()->wherePivot('client_id', auth()->user()->client_id)->get();
            if (count($roles)) {
                foreach ($roles as $role) {
                    $permissions = $permissions->merge($role->permissions);
                }
            }
            $user->roles = $roles;
        }

        $user->permissions = $permissions->unique();
        $user->load('client.subscriptions');

        if (request('relations')) {
            $user->load([request('relations')]);
        }

        return ["user" => $user, "id" => $id];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            "name" => "required",
            "email" => "required",
        ];

        $this->validate(request(), $rules);

        $user = User::find($id);
        if (!$user) {
            return response()->json(['error' => "Invalid data sent. "], 422);
        }
        $userData = null;
        $clientId = auth()->user()->user_type_id == UserType::APP_ADMIN ? request('client_id', auth()->user()->client_id) : auth()->user()->client_id;
        if ($user->email != request('email')) {
            $existEmail = User::where('email', request('email'))
                ->whereHas(
                    'clients',
                    function ($q) use ($clientId) {
                        $q->where('client_id', $clientId);
                    }
                )->first();
            if ($existEmail) {
                return response()->json(['error' => "User already exists."], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $userData = User::where('email', request('email'))
                ->whereHas(
                    'clients',
                    function ($q) use ($clientId) {
                        $q->where('client_id', '!=', $clientId);
                    }
                )->first();
            if ($userData) {
                if (count($user->clients) > 1) {
                    UserClient::where('user_id', $user->id)
                        ->where('client_id', $clientId)
                        ->delete();
                } else {
                    $user->delete();
                }
                $user = $userData;
                $userClient = new UserClient();
                $userClient->user_id = $user->id;
                $userClient->user_type_id = request('user_type_id');
                $userClient->client_id = $clientId;
                $userClient->creator_id = auth()?->id();
                $userClient->is_enabled = 1;
                $userClient->enabled_on = now();
                $userClient->save();
                $modules = Module::get();
                $client = Client::find($clientId);
                Mail::to($user->email)->queue(new NewCompanyUserEmail($user, $modules, $client));
            }
        }
        if (!$userData) {
            $user->name = request('name');
            $user->email = request('email');
            $user->signature = request('signature', '');
            if ($user->client_id == $clientId) {
                $user->user_type_id = request('user_type_id', UserType::CLIENT_USER); //if there is no user type id specified, it will fallback to normal client user.
            }
            if (request('phone')) {
                $user->phone = request('phone', null);
            }

            if (request('password') && "" != trim(request("password")) && null != request("password")) {
                $user->password = bcrypt(request('password'));
            }

            if (!$user->save()) {
                return response()->json(['error' => "Failed to add the new user. "], 422);
            }

            $userClient = $this->findOrCreateUserClient($user->id, $clientId, request('user_type_id'));
        }
        $user->roles()->wherePivot('client_id', $clientId)->detach();

        $roles = request('roles') ?: [];
        if (is_array($roles)) {
            foreach ($roles as $rid => $selected) {
                if (!$selected) {
                    continue;
                }
                $role = Role::find($rid);
                $user->assignRole($role->slug, $clientId);
            }
        }

        return ["item" => $user];
    }

    public function updateCurrentUserProfile(Request $request)
    {
        $rules = [
            "name" => "required",
            "email" => "required",
        ];

        $this->validate(request(), $rules);

        $user = auth()->user();
        if (!$user) {
            return response()->json(['error' => "Invalid data sent. "], 422);
        }

        $user->name = request('name');
        $user->email = request('email');
        $user->temporarily_invited = false;

        if (request('password') && "" != trim(request("password")) && null != request("password")) {
            $user->password = bcrypt(request('password'));
            $user->is_temporary_password = false;
        }

        if (!$user->save()) {
            return response()->json(['error' => "Failed to add the new user. "], 422);
        }

        $roles = $user->roles()->wherePivot('client_id', auth()->user()->client_id)->get();

        $permissions = new \Illuminate\Database\Eloquent\Collection();
        if (count($roles)) {
            foreach ($roles as $role) {
                $permissions = $permissions->merge($role->permissions);
            }
        }

        $user->roles = $roles;

        $user->permissions = $permissions->unique();
        $user->client;

        return ["item" => $user];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clientId = auth()->user()->user_type_id == UserType::APP_ADMIN ? request('client_id', auth()->user()->client_id) : auth()->user()->client_id;

        if ($id == auth()->id()) {
            return response()->json(['error' => "You can not delete your own account."], 422);
        }

        $user = User::find($id);
        if (!$user) {
            return response()->json(['error' => "Invalid data sent. "], 422);
        }

        if (UserClient::where('user_id', $user->id)->count() == 1) {
            $user->client_id = $clientId;
            $user->email = Carbon::now()->timestamp . '_' . $user->email;
            $user->save();
        }

        if ($user->client_id == $clientId) {
            $user->tokens->each(function ($token, $key) {
                $token->delete();
            });
        }

        /* Delete all related objects */
        $user->roles()->wherePivot('client_id', $clientId)->detach();

        /* Delete all releated user modules */
        ModuleUser::where('user_id', $user->id)
            ->where('client_id', $clientId)
            ->delete();

        UserClient::where('user_id', $user->id)
            ->where('client_id', $clientId)
            ->delete();

        $userClients = UserClient::where('user_id', $user->id)->get();
        if (!count($userClients)) {
            /* Delete the user object */
            $deleted = $user->delete();
        }

        return ["item" => $user, "msg" => "success"];
    }

    /**
     * Enable the user account
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function enable(User $user, Request $request)
    {
        try {
            $clientId = auth()->user()->client_id;
            if (auth()->user()->user_type_id == UserType::APP_ADMIN) {
                if (!$request->client_id) {
                    return response()->json(['error' => "Client id is required."], Response::HTTP_BAD_REQUEST);
                }
                $clientId = $request->client_id;
            }
            UserClient::where('user_id', $user->id)
                ->where('client_id', $clientId)
                ->update(['is_enabled' => true, 'enabled_on' => now()]);
            $user->load([
                'userClient' => function ($q) use ($clientId) {
                    $q->where('client_id', $clientId);
                },
            ]);

            return response()->json(['item' => $user], Response::HTTP_OK);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Disable the user account
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disable(User $user, Request $request)
    {
        try {
            if ($user->id == auth()->id()) {
                return response()->json(['error' => "You can not disable your own account."], Response::HTTP_BAD_REQUEST);
            }
            $clientId = auth()->user()->client_id;
            if (auth()->user()->user_type_id == UserType::APP_ADMIN) {
                if (!$request->client_id) {
                    return response()->json(['error' => "Client id is required."], Response::HTTP_BAD_REQUEST);
                }
                $clientId = $request->client_id;
            }

            if ($user->client_id == $clientId) {
                $user->tokens->each(function ($token, $key) {
                    $token->delete();
                });
            }

            UserClient::where('user_id', $user->id)
                ->where('client_id', $clientId)
                ->update(['is_enabled' => false]);

            $user->load([
                'userClient' => function ($q) use ($clientId) {
                    $q->where('client_id', $clientId);
                },
            ]);

            return response()->json(['item' => $user], Response::HTTP_OK);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function companySignUp(Request $request)
    {
        $this->validate(request(), [
            "business_name" => "required",
            "business_email" => "required|unique:clients,email",
            "user_name" => "required",
            "user_email" => "required|unique:users,email",
        ]);

        $client = new Client();
        $client->name = request('business_name');
        $client->email = request('business_email');
        $client->phone = "";
        $client->address = "";
        $client->logo = "";
        $client->letterhead_image = "";
        $client->tin = "";

        if (!$client->save()) {
            return response()->json(['error' => 'Failed to save the Client.'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        /* Lets setup our event */
        event(new ClientCreatedEvent($client));

        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pin = mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];
        $originalPassword = str_shuffle($pin);

        $user = new User();
        $user->name = request('user_name');
        $user->email = request('user_email');
        $user->password = bcrypt($originalPassword);
        $user->is_temporary_password = true;
        $user->user_type_id = UserType::CLIENT_ADMIN;
        $user->client_id = $client->id;

        if (!$user->save()) {
            return response()->json(['error' => 'Failed to save the User.'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $userClient = new UserClient();
        $userClient->user_id = $user->id;
        $userClient->user_type_id = $user->user_type_id;
        $userClient->client_id = $user->client_id;
        $userClient->creator_id = auth()?->id();
        $userClient->is_enabled = 1;
        $userClient->enabled_on = now();
        $userClient->save();

        Mail::to($user->email)->queue(new WelcomeMail($user, [], $originalPassword));

        return ["item" => $user];
    }

    private function randomPassword()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $pin = mt_rand(1000000, 9999999)
            . $characters[rand(0, strlen($characters) - 1)];

        return str_shuffle($pin);
    }

    public function forgotPassword()
    {
        $this->validate(request(), [
            "email" => "required",
        ]);

        $user = User::where('email', request('email'))
            ->with(['client'])
            ->first();

        if (!$user) {
            return response()->json(['error' => "Invalid email."], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Check if user is not disabled or blocked.
        $userClient = UserClient::query()
            ->where('user_id', $user->id)
            ->where('client_id', $user->client_id)
            ->whereHas('client', function ($q) {
                $q->where('status_id', ClientStatus::ACTIVE);
            })
            ->where('is_enabled', false)
            ->first();
        if ($userClient) {
            return response()->json([
                'error' => 'Your Account is disabled, you cannot perform any actions. Please contact your administrator to re-enable your account and try again later.',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $originalPassword = $this->randomPassword();
        $user->password = bcrypt($originalPassword);
        $user->is_temporary_password = true;

        if (!$user->save()) {
            return response()->json(['error' => "Failed to update the password. "], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $password = $originalPassword;
        $subject = "Your Password has been reset";

        Mail::to($user->email)->queue(new ForgotPassword($user, $subject, $password));

        return ["item" => $user];
    }

    public function setPassword()
    {
        $this->validate(request(), [
            "password" => "required",
        ]);

        $user = User::find(auth()->id());

        if (empty($user)) {
            return response()->json(['error' => "Email is not registered. "], Response::HTTP_UNPROCESSABLE_ENTITY);
        } else {
            $user->password = bcrypt(request('password'));
            $user->is_temporary_password = false;

            if (!$user->save()) {
                return response()->json(['error' => "Failed to add the new user. "], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            return ["item" => $user, "success" => true];
        }
    }

    public function otherClientUser()
    {
        $filters = request([
            'email',
        ]);

        $users = User::where('email', 'LIKE', "%" . $filters['email'] . "%")
            ->whereHas('clients', function ($q) {
                $q->where('client_id', '!=', auth()->user()->client_id);
            })->get();

        return ["data" => $users];
    }

    public function switchCompany($id)
    {
        $userClient = UserClient::find($id);

        if (!$userClient->is_enabled) {
            return response()->json(['error' => $userClient->client->name . " disabled your account."], Response::HTTP_BAD_REQUEST);
        }
        if ($userClient->client->status_id == ClientStatus::DISABLED) {
            return response()->json(['error' => $userClient->client->name . "'s account disabled."], Response::HTTP_BAD_REQUEST);
        }

        $user = User::find($userClient->user_id);
        $user->client_id = $userClient->client_id;
        $user->user_type_id = $userClient->user_type_id;
        $user->save();

        Passport::tokensExpireIn(Carbon::now()->addDays(30));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(60));

        $objToken = $user->createToken('API Access');
        $strToken = $objToken->accessToken;

        $expiration = $objToken->token->expires_at->diffInSeconds(Carbon::now());

        $authData = [
            "access_token" => $strToken,
            "expires_in" => $expiration,
            "refresh_token" => "",
        ];

        return $authData;
    }
}
