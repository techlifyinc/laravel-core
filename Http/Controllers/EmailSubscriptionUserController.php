<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Response;
use Modules\LaravelCore\Entities\EmailSubscriptionUser;

class EmailSubscriptionUserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $filters = request([
            "sort_by",
            "user_id",
            "email_type_id",
            "creator_id"
        ]);

        $isLoadRelations = filter_var(request()->query('isLoadRelations', true), FILTER_VALIDATE_BOOLEAN);
        $relations = ['emailType', 'user'];
        $perPage = request()->query('perPage', config('app.perPage', 25));

        $relations = request()->query('relations', $relations);
        if (is_string($relations) && !is_array($relations)) {
            $relations = explode(',', $relations);
        }

        if ($isLoadRelations === true && collect($relations)->count() > 0) {
            return EmailSubscriptionUser::with($relations)->filter($filters)
                ->paginate($perPage);
        }
        return EmailSubscriptionUser::filter($filters)->paginate($perPage);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function createOrUpdate(Request $request, EmailSubscriptionUser $esUser)
    {
        $rules = [
            "user_id" => "required|exists:users,id",
            "email_type_id" => "required|exists:email_subscription_types,id",
        ];

        $request->validate($rules);

        /**Check and delete the duplicate */
        $oldSubscription = EmailSubscriptionUser::filter([
            'user_id' => request('user_id'),
            'email_type_id' => request('email_type_id')
        ])->first();
        if (!!$oldSubscription) {
            $oldSubscription->delete();
        }

        if (!$esUser) {
            $esUser =  new EmailSubscriptionUser();
        }

        try {
            $esUser->user_id = request('user_id');
            $esUser->email_type_id = request('email_type_id');
            $esUser->creator_id = auth()->id();
            $esUser->client_id = auth()->user()->client_id;
            $esUser->save();
        } catch (Exception $e) {
            report($e);
            return response()->json(['error' => $e->getMessage(), 'trace' => $e->getTrace()], Response::HTTP_BAD_REQUEST);
        }
        return response()->json(['item' => $esUser], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(EmailSubscriptionUser $esUser)
    {
        try {
            $esUser->delete();
            return ["status" => "success"];
        } catch (Exception $e) {
            report($e);
            return response()->json(['error' => $e->getMessage(), 'trace' => $e->getTrace()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function subscribe(Request $request)
    {
        $request->merge(['user_id' => auth()->id()]);
        return $this->createOrUpdate($request, new EmailSubscriptionUser());
    }
}
