<?php

namespace Modules\LaravelCore\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Modules\LaravelCore\Entities\UserPasswordHistoryService;
use Modules\LaravelCore\Http\Requests\ChangePasswordRequest;

class ChangePasswordController extends Controller
{
    /**
     * Change the password for the current logged-in user.
     *
     * @param ChangePasswordRequest $request
     * @return JsonResponse|array
     * @throws Exception
     */
    public function updatePassword(ChangePasswordRequest $request): JsonResponse|array
    {
        $user = auth()->user();
        // Check for the password existence and block
        if (UserPasswordHistoryService::checkForPasswordBlock($user, $request->get('password'))) {
            return response()->json(
                ['error' => "You already used this password. So, please try different password."],
                Response::HTTP_BAD_REQUEST
            );
        }

        UserPasswordHistoryService::create($user);

        $user->password = bcrypt($request->password);
        $user->is_temporary_password = false;

        if (!$user->save()) {
            return response()->json(['error' => "Failed to add the new user. "], Response::HTTP_BAD_REQUEST);
        }

        return ["item" => $user, "success" => true];
    }
}
