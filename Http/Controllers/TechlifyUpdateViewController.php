<?php

namespace Modules\LaravelCore\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\LaravelCore\Entities\TechlifyUpdateView;

class TechlifyUpdateViewController extends Controller
{

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        try {
            $rules = [
                "update_id" => "required|exists:techlify_updates,id",
            ];
            $this->validate(request(), $rules);

            $existingView = TechlifyUpdateView::where('update_id', $request->update_id)
                ->where('user_id', auth()->id())
                ->first();
                
            if ($existingView) {
                return response()->json(['item' => $existingView], Response::HTTP_OK);
            }

            $techlifyUpdateView = new TechlifyUpdateView();
            $techlifyUpdateView->update_id = $request->update_id;
            $techlifyUpdateView->user_id = auth()->id();
            $techlifyUpdateView->viewed_on = Carbon::now();
            $techlifyUpdateView->save();

            return response()->json(['item' => $techlifyUpdateView], Response::HTTP_OK);
        } catch (Exception $error) {
            return response()->json(['error' => $error], Response::HTTP_BAD_REQUEST);
        }
    }
}
