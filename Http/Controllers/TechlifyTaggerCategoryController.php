<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\LaravelCore\Entities\TechlifyTaggerCategory;
use Illuminate\Validation\Rule;

class TechlifyTaggerCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $filters = request([
            'sort_by',
            'search'
        ]);

        $perPage = request()->query('perPage', config('app.perPage', 25));
        return TechlifyTaggerCategory::filter($filters)
            ->paginate($perPage);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            "title" => [
                'required',
                Rule::unique('techlify_tagger_categories', 'title')
                    ->using(function ($q) {
                        $q->where('client_id', auth()->user()->client_id);
                        $q->whereNull('deleted_at');
                    })
            ]
        ];

        $this->validate(request(), $rules);

        $category = new TechlifyTaggerCategory();

        $category->title = request('title');
        $category->description = request('description', null);
        $category->client_id = auth()->user()->client_id;
        $category->creator_id = auth()->id();

        if (!$category->save()) {
            return response()->json(['error' => "Error on Save "], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return ['item' => $category];
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, TechlifyTaggerCategory $category)
    {
        $rules = [
            "title" => [
                'required',
                Rule::unique('techlify_tagger_categories', 'title')
                    ->ignore($this->category->id)
                    ->using(function ($q) {
                        $q->where('client_id', auth()->user()->client_id);
                        $q->whereNull('deleted_at');
                    })
            ]
        ];

        $this->validate(request(), $rules);

        $category = new TechlifyTaggerCategory();

        $category->title = request('title');
        $category->description = request('description', null);

        if (!$category->save()) {
            return response()->json(['error' => "Error on Save "], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return ['item' => $category];
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(TechlifyTaggerCategory $category)
    {
        if (!$category->delete()) {
            return response()->json(['error' => "Error on Delete "], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return ['item' => $category];
    }
}
