<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\LaravelCore\Entities\RestrictorStatus;

class RestrictorStatusController extends Controller
{
    /**
     * Get the list of restrictor statuses.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $perPage = $request->get('perPage', 25);
        $statuses = RestrictorStatus::query()
            ->paginate($perPage);

        return response()->json($statuses);
    }
}
