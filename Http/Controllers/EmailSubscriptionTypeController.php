<?php

namespace Modules\LaravelCore\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\LaravelCore\Entities\EmailSubscriptionType;

class EmailSubscriptionTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $filters = request([
            "sort_by",
            "search",
            "frequency_id"
        ]);

        $isLoadRelations = filter_var(request()->query('isLoadRelations', true), FILTER_VALIDATE_BOOLEAN);
        $relations = ['frequency'];
        $perPage = request()->query('perPage', config('app.perPage', 25));

        $relations = request()->query('relations', $relations);
        if (is_string($relations) && !is_array($relations)) {
            $relations = explode(',', $relations);
        }

        if ($isLoadRelations === true && collect($relations)->count() > 0) {
            return EmailSubscriptionType::with($relations)->filter($filters)
                ->paginate($perPage);
        }
        return EmailSubscriptionType::filter($filters)->paginate($perPage);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            "title" => "required",
            "frequency_id" => "required|exists:email_subscription_frequencies,id",
            "code" => "required|unique:email_subscription_types,code",
        ];

        $request->validate($rules);
        $esType = new EmailSubscriptionType();

        try {
            $esType->title = request('title');
            $esType->code = request('code');
            $esType->frequency_id = request('frequency_id');
            $esType->description = request('description', null);
            $esType->save();
        } catch (Exception $e) {
            report($e);
            return response()->json(['error' => $e->getMessage(), 'trace' => $e->getTrace()], Response::HTTP_BAD_REQUEST);
        }
        return response()->json(['item' => $esType], Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, EmailSubscriptionType $esType)
    {
        $rules = [
            "title" => "required",
            "frequency_id" => "required|exists:email_subscription_frequencies,id",
            "code" => "required|unique:email_subscription_types,code," . $esType->id,
        ];

        $request->validate($rules);
        try {
            $esType->title = request('title');
            $esType->code = request('code');
            $esType->frequency_id = request('frequency_id');
            $esType->description = request('description', null);
            $esType->save();
        } catch (Exception $e) {
            report($e);
            return response()->json(['error' => $e->getMessage(), 'trace' => $e->getTrace()], Response::HTTP_BAD_REQUEST);
        }
        return response()->json(['item' => $esType], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(EmailSubscriptionType $esType)
    {
        try {
            DB::beginTransaction();
            $esType->code = Carbon::now()->timestamp . '_' . $esType->code;
            $esType->delete();
            DB::commit();
            return ["status" => "success"];
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return response()->json(['error' => $e->getMessage(), 'trace' => $e->getTrace()], Response::HTTP_BAD_REQUEST);
        }
    }
}