<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\LaravelCore\Entities\TechlifyTaggerCategory;
use Modules\LaravelCore\Entities\TechlifyTaggerTag;
use Illuminate\Validation\Rule;

class TechlifyTaggerTagController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $filters = request([
            'sort_by',
            'categories',
            'model_type',
            'model_id',
            'search'
        ]);

        $perPage = request()->query('perPage', config('app.perPage', 25));
        return TechlifyTaggerTag::filter($filters)->with(['category'])
            ->paginate($perPage);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            "title" => "required"
        ];
        $this->validate(request(), $rules);

        $duplicateTag = TechlifyTaggerTag::where('title', request('title'))->first();
        if (
            $duplicateTag != null && (
                ($duplicateTag->category_id != null && $duplicateTag->category->title === request('category_title', null))
                ||
                ($duplicateTag->category_id == null && request('category_title', null) == null))
        ) {
            //Duplicate Tag belongs to same category, so just return the tag.
            $duplicateTag->load(['category']);
            return ['item' => $duplicateTag];
        } else if ($duplicateTag != null) {
            //Duplicate Tag belongs to another category, so we cannot proceed with the request.
            return response()->json(
                ['error' => "There is already a tag present with same name, under different Category "],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $tag = new TechlifyTaggerTag();
        $tag->title = request('title');
        $tag->description = request('description', null);
        $tag->client_id = auth()->user()->client_id;
        $tag->creator_id = auth()->id();

        /**Validate for Category and add it */
        $category = null;
        if (request('category_title', null) !== null) {
            /**If there is a category present for tag */
            $category = TechlifyTaggerCategory::where('title', request('category_title'))->first();
            if ($category == null) {
                $request->replace([
                    'title' => request('category_title'),
                    'description' => request('category_title')
                ]);
                $categoryResponse = (new TechlifyTaggerCategoryController)->store($request);
                if (isset ($categoryResponse['item'])) {
                    //Success
                    $category = $categoryResponse['item'];
                } else {
                    //Error
                    return $categoryResponse;
                }
            }
        }
        $tag->category_id = $category->id ?? null;

        if (!$tag->save()) {
            return response()->json(['error' => "Error on Save "], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $tag->load(['category']);
        return ['item' => $tag];
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, TechlifyTaggerTag $tag)
    {
        $rules = [
            "title" => [
                'required',
                Rule::unique('techlify_tagger_tags', 'title')
                    ->ignore($this->tag->id)
                    ->using(function ($q) {
                        $q->where('client_id', auth()->user()->client_id);
                        $q->whereNull('deleted_at');
                    })
            ]
        ];

        $this->validate(request(), $rules);

        $tag = new TechlifyTaggerTag();

        $tag->title = request('title');
        $tag->description = request('description', null);
        $tag->category_id = request('category_id', null);

        if (!$tag->save()) {
            return response()->json(['error' => "Error on Save "], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $tag->load(['category']);
        return ['item' => $tag];
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(TechlifyTaggerTag $tag)
    {
        if (!$tag->delete()) {
            return response()->json(['error' => "Error on Delete "], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return ['item' => $tag];
    }
}
