<?php

namespace Modules\LaravelCore\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\LaravelCore\Entities\EntityFile;
use Modules\LaravelCore\Entities\FileManager;

class EntityFileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $filters = request([
            "title",
            "entity_type",
            "entity_id",
            "details",
            'tag_ids',
        ]);

        $perPage = request()->query('perPage', config('app.perPage', 25));

        return EntityFile::filter($filters)
            ->paginate($perPage);
    }

    /**
     * Store Entity File.
     *
     * @OA\PathItem(
     *     path="/api/entity-files",
     *     @OA\Post(
     *         tags={"Entity Files"},
     *         summary="Upload and save a new entity file",
     *         description="Creates a new entity file record with the provided data, including related tags and file metadata.",
     *         operationId="storeEntityFile",
     *         @OA\RequestBody(
     *             required=true,
     *             description="Data for the new entity file",
     *             @OA\JsonContent(
     *                 required={"title", "file", "entity_type", "entity_id", "file_size"},
     *                 @OA\Property(property="title", type="string", description="Title of the file"),
     *                 @OA\Property(property="file", type="string", description="File content or path"),
     *                 @OA\Property(property="entity_type", example="ChecksheetSubmissionAnswer",type="string", description="Type of the entity the file is associated with"),
     *                 @OA\Property(property="entity_id", example="1",type="integer", description="Identifier of the entity the file is linked to"),
     *                 @OA\Property(property="file_size", type="integer", description="Size of the file in bytes"),
     *                 @OA\Property(property="details", type="string", description="Additional details about the file (optional)", example=""),
     *                 @OA\Property(property="tag_ids", type="string", description="Comma-separated list of tag IDs to associate with the file (optional)")
     *             )
     *         ),
     *         @OA\Response(
     *             response=201,
     *             description="File created successfully",
     *             @OA\JsonContent(
     *                 @OA\Property(property="item", type="object", description="The newly created entity file object")
     *             )
     *         ),
     *         @OA\Response(
     *             response=400,
     *             description="Bad request, invalid data input or failed operation",
     *             @OA\JsonContent(
     *                 @OA\Property(property="errors", type="string", description="Error message explaining what went wrong"),
     *                 @OA\Property(property="stack_trace", type="array", @OA\Items(type="string"), description="Stack trace details")
     *             )
     *         ),
     *         security={{"bearerAuth":{}}}
     *     )
     * )
     */
    public function store(Request $request): JsonResponse
    {
        $rules = [
            "title" => "required",
            "file" => "required",
            "entity_type" => "required",
            "entity_id" => "required",
            "file_size" => "required"
        ];

        $request->validate($rules);

        $file = new EntityFile();
        $file->title = $request->title;
        $file->file = $request->file;
        $file->file_size = $request->file_size;
        $file->entity_type = $request->entity_type;
        $file->entity_id = $request->entity_id;
        $file->details = $request->details ?: '';
        $file->data = [];
        $file->creator_id = auth()->id();
        $file->client_id = auth()->user()->client_id;

        try {
            $file->save();
            // save the tags
            if ($request->filled('tag_ids')) {
                $file->addTags(explode(',', $request->input('tag_ids')));
            }

            return response()->json([
                'item' => $file,
            ], Response::HTTP_CREATED);
        } catch (Exception $ex) {
            return response()->json([
                'errors' => $ex->getMessage(),
                'stack_trace' => $ex->getTrace(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(EntityFile $file, Request $request)
    {
        $rules = [
            "title" => "required",
            "file" => "required",
            "entity_type" => "required",
            "entity_id" => "required",
            "file_size" => "required"
        ];

        request()->validate($rules);

        $file->title = request('title');
        $file->file = request('file');
        $file->file_size = request('file_size');
        $file->entity_type = request('entity_type');
        $file->entity_id = request('entity_id');
        $file->details = request('details', '');
        $file->data = [];

        try {
            $file->save();
            // save the tags
            if ($request->filled('tag_ids')) {
                $file->addTags(explode(',', $request->input('tag_ids')));
            }

            return response()->json([
                'item' => $file,
            ], Response::HTTP_CREATED);
        } catch (Exception $ex) {
            return response()->json([
                'errors' => $ex->getMessage(),
                'stack_trace' => $ex->getTrace(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(EntityFile $file)
    {
        $disk = config('laravelcore.entity_file_disk', (new EntityFile)->disk);
        try {
            FileManager::delete($disk, $file->file);
            $file->delete();
            return response()->json([
                'item' => $file,
            ], Response::HTTP_OK);
        } catch (Exception $ex) {
            return response()->json([
                'errors' => $ex->getMessage(),
                'stack_trace' => $ex->getTrace(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Upload a file and attach to entity.
     *
     * @OA\PathItem(
     *     path="/api/entity-files-upload",
     *     @OA\Post(
     *         tags={"Entity Files"},
     *         summary="Upload a file and optionally link it to an entity",
     *         description="Allows uploading a file with optional parameters to link the uploaded file to an entity. If 'entity_id' and 'entity_type' are provided, the file is associated with the specified entity.",
     *         operationId="uploadEntityFile",
     *         @OA\RequestBody(
     *             required=true,
     *             description="File upload request with optional entity association",
     *             @OA\MediaType(
     *                 mediaType="multipart/form-data",
     *                 @OA\Schema(
     *                     required={"fileData"},
     *                     @OA\Property(
     *                         property="fileData",
     *                         type="string",
     *                         format="binary",
     *                         description="The file to upload"
     *                     ),
     *                     @OA\Property(
     *                         property="entity_id",
     *                         type="integer",
     *                         example="1",
     *                         description="The ID of the entity to which the file will be linked (optional)"
     *                     ),
     *                     @OA\Property(
     *                         property="entity_type",
     *                         type="string",
     *                         example="CheckSheetSubmissionAnswer",
     *                         description="The type of the entity to which the file will be linked (optional)"
     *                     ),
     *                     @OA\Property(
     *                         property="sub_path",
     *                         type="string",
     *                         example="checksheets",
     *                         description="Within the disk, this is the sub-folder where the file will be stored (optional)"
     *                     ),
     *                     @OA\Property(
     *                       property="disk",
     *                       type="string",
     *                       example="public",
     *                       description="The disk to store the file (optional)"
     *                      )
     *                 )
     *             ),
     *         ),
     *         @OA\Response(
     *             response=201,
     *             description="File uploaded successfully",
     *             @OA\JsonContent(
     *                 @OA\Property(
     *                     property="item",
     *                     type="object",
     *                     description="Details of the uploaded file or the created entity file"
     *                 )
     *             )
     *         ),
     *         @OA\Response(
     *             response=422,
     *             description="Unprocessable Entity, invalid input data",
     *             @OA\JsonContent(
     *                 @OA\Property(
     *                     property="error",
     *                     type="string",
     *                     description="Error message explaining what went wrong during file upload"
     *                 )
     *             )
     *         ),
     *         security={{"bearerAuth":{}}}
     *     )
     * )
     */
    public function upload(Request $request)
    {
        $maxUploadSize = config('laravelcore.entity_file_max_upload_file_size', 20480);

        request()->validate([
            'fileData' => "required|max:$maxUploadSize", //max size of 20MB
            'disk' => 'nullable|string',
            'sub_path' => 'nullable|string',
        ]);

        $disk = request(
            'disk',
            config('laravelcore.entity_file_disk')
        );


        $subPath = request('sub_path', config('laravelcore.entity_file_sub_path'));

        if (auth()->user()->client_id != null) {
            //If user has valid client, then store the file in client sub folder
            $subPath = $subPath . '/' . auth()->user()->client_id;
        }
        if ($request->has('entity_type')) {
            //If request has entity type specified file get stored in specified path
            $subPath = "$subPath/{$request->entity_type}";
        }

        $fileResponse = $this->_upload($disk, $subPath);

        if (isset($fileResponse['error'])) {
            return response()->json($fileResponse, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if ($request->has('entity_id')) {
            $requestObj = new Request([
                'title' => $fileResponse['file_name'],
                'file' => $fileResponse['file'],
                'file_size' => $fileResponse['file_size'],
                'entity_id' => $request->entity_id,
                'entity_type' => $request->entity_type,
            ]);
            return $this->store($requestObj);
        }
        return response()->json([
            "item" => $fileResponse,
        ], Response::HTTP_CREATED);
    }

    /**
     * Upload file with FileManager.
     *
     * @param $disk
     * @param $subPath
     * @return array
     */
    public function _upload($disk, $subPath): array
    {
        // Call the FileManager upload method to save the file.
        return FileManager::upload($disk, $subPath);
    }
}
