<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Exception;
use Illuminate\Http\Response;
use Modules\LaravelCore\Entities\ClientFeature;
use Modules\LaravelCore\Entities\Feature;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        try {
            $filters = request([]);

            $perPage = request()->query('perPage');

            $features = Feature::filter($filters)
                ->with('clientFeature')
                ->paginate($perPage);
        } catch (Exception $error) {
            return response()->json([
                'errors' => 'Request cannot be processed',
                'location' => 'index'
            ], Response::HTTP_BAD_REQUEST);
        }

        return $features;
    }

    public function toggleFeature(ClientFeature $feature)
    {
        try {
            $feature->is_enabled = !$feature->is_enabled;
            $feature->save();
        } catch (Exception $error) {
            return response()->json([
                'errors' => 'Request cannot be processed',
                'location' => 'toggleFeature',
                'feature' => $feature
            ], Response::HTTP_BAD_REQUEST);
        }

        return response()->json(['item' => $feature], Response::HTTP_OK);
    }
}
