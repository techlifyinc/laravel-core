<?php

namespace Modules\LaravelCore\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\LaravelCore\Entities\OauthAccess;

class OauthAccessController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function userLoginHistory()
    {
        $filters = request(['date_from', 'sort_by', 'date_to', 'user_id']);
        $perPage = request()->query('perPage', config('app.perPage', 25));
        return OauthAccess::filter($filters)->paginate($perPage);
    }
}
