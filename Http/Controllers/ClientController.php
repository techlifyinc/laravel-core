<?php

namespace Modules\LaravelCore\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\LaravelCore\Entities\Client;
use Modules\LaravelCore\Entities\ClientStatus;
use Modules\LaravelCore\Entities\FileManager;
use Modules\LaravelCore\Entities\OauthAccess;
use Modules\LaravelCore\Events\ClientCreatedEvent;

class ClientController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filters = request(['name', 'address', 'phone', 'email', 'sort_by', 'num_items']);

        $parts = [];
        if (isset($filters['num_items']) && "" != trim($filters['num_items'])) {
            $parts = explode("|", $filters['num_items']);
        }

        $clients = Client::filter($filters)
            ->with('creator')
            ->paginate(count($parts) ? $parts[0] : 25, ['*'], 'page', count($parts) > 1 ? $parts[1] : 1);

        return $clients;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
        ];

        $this->validate(request(), $rules);

        $client = new Client();
        $client->name = request('name');
        $client->logo = request('logo', '');
        $client->letterhead_image = request('letterhead_image', '');
        $client->phone = request('phone', '');
        $client->address = request('address', '');
        $client->email = request('email', '');
        $client->tin = request('tin', '');
        $client->creator_id = auth()->id();

        if (!$client->save()) {
            return response()->json(['error' => 'Failed to add client'], 422);
        }

        /* Lets setup our event */
        event(new ClientCreatedEvent($client));

        return ['item' => $client];
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);

        if (null == $client) {
            return response()->json(['error' => 'Invalid Client data sent'], 422);
        }

        $client->load('creator');

        return ['item' => $client];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $client = Client::find($id);

        if (null == $client) {
            return response()->json(['error' => 'Invlaid client data sent'], 422);
        }
        $rules = [
            'name' => 'required',
        ];

        $this->validate(request(), $rules);

        $client->name = request('name');
        $client->logo = request('logo', '');
        $client->letterhead_image = request('letterhead_image', '');
        $client->phone = request('phone', '');
        $client->address = request('address', '');
        $client->email = request('email', '');
        $client->tin = request('tin', '');

        if (!$client->save()) {
            return response()->json(['error' => 'Failed to update client'], 422);
        }

        return ['item' => $client];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);

        if (null == $client) {
            return response()->json(['error' => 'Invalid Client data sent'], 422);
        }

        if (!$client->delete()) {
            return response()->json(['error' => 'Client deletion failed'], 422);
        }
        return ['item' => $client];
    }

    /**
     * Upload the file.
     * @param int $id
     * @return Response
     */
    public function upload()
    {
        $disk = env('COMPANY_FILE_DISK', (new Client())->disk);
        $subPath = env('COMPANY_FILE_SUB_PATH', (new Client)->subPath);

        return $this->_upload($disk, $subPath);
    }

    public function _upload($disk, $subPath)
    {
        $maxUploadSize = env('COMPANY_FILE_MAX_UPLOAD_FILE_SIZE', 20480);

        request()->validate([
            'fileData' => "required|max:$maxUploadSize", //max size of 20MB
        ]);

        //Call the parent controller method to save the file.
        $data = FileManager::upload($disk, $subPath);

        return response()->json([
            "item" => $data,
        ], Response::HTTP_CREATED);
    }


    public function enableClient(Request $request, Client $client)
    {
        try {
            $client->status_id = ClientStatus::ACTIVE;
            $client->save();

            return response()->json([
                "item" => $client,
            ], Response::HTTP_OK);
        } catch (Exception $e) {
            return response()->json([
                "error" => $e->getmessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function disableClient(Request $request, Client $client)
    {
        try {
            $client->status_id = ClientStatus::DISABLED;
            $client->save();

            // Fetch user ids for this client
            $userIds = User::where('client_id', $client->id)
                ->pluck('id');

            // Disabling all active users by deleting their access codes
            OauthAccess::whereIn('user_id', $userIds)
                ->delete();

            return response()->json([
                "item" => $client,
            ], Response::HTTP_OK);
        } catch (Exception $e) {
            return response()->json([
                "error" => $e->getmessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
