<?php

namespace Modules\LaravelCore\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Modules\LaravelCore\Entities\TechlifyUpdate;
use Modules\LaravelCore\Entities\TechlifyUpdateView;
use Modules\LaravelCore\Entities\UserType;

class TechlifyUpdateController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return mixed
     */
    public function index()
    {
        $filters = request([
            "search",
            "version",
            "for_current_user",
            "sort_by"
        ]);

        $perPage = request()->query('perPage');

        return TechlifyUpdate::filter($filters)
            ->paginate($perPage);
    }

    public function create(Request $request)
    {
        try {
            $rules = [
                "title" => "required",
                "released_on" => "required",
                "version" => "required",
                "description" => "required",
            ];
            $this->validate(request(), $rules);

            $techlifyUpdate = new TechlifyUpdate();
            $techlifyUpdate->title = $request->title;
            $techlifyUpdate->description = $request->description;
            $techlifyUpdate->is_pinned = $request->is_pinned ?? false;
            $techlifyUpdate->version = $request->version;
            $techlifyUpdate->released_on = $this->convertDateToTz($request->released_on);
            $techlifyUpdate->learn_more_link = $request->learn_more_link;
            $techlifyUpdate->creator_id = auth()->id();
            $techlifyUpdate->save();

            return response()->json(['item' => $techlifyUpdate], Response::HTTP_OK);
        } catch (Exception $error) {
            return response()->json([
                'error' => $error,
                'stack_trace' => $error->getTrace()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function update(Request $request, TechlifyUpdate $techlifyUpdate)
    {
        try {
            $rules = [
                "title" => "required",
            ];
            $this->validate(request(), $rules);

            $techlifyUpdate->title = $request->title;
            $techlifyUpdate->description = $request->description;
            $techlifyUpdate->version = $request->version;
            $techlifyUpdate->is_pinned = $request->is_pinned ?? false;
            $techlifyUpdate->released_on = $this->convertDateToTz($request->released_on);
            $techlifyUpdate->learn_more_link = $request->learn_more_link;
            $techlifyUpdate->save();

            return response()->json(['item' => $techlifyUpdate], Response::HTTP_OK);
        } catch (Exception $error) {
            return response()->json([
                'error' => $error,
                'stack_trace' => $error->getTrace()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy(TechlifyUpdate $techlifyUpdate)
    {
        try {
            $techlifyUpdate->delete();
            TechlifyUpdateView::where('update_id', $techlifyUpdate->id)
                ->delete();
            return response()->json(['item' => $techlifyUpdate], Response::HTTP_OK);
        } catch (Exception $error) {
            return response()->json([
                'error' => $error,
                'stack_trace' => $error->getTrace()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function getUpdateForCurrentUser(Request $request)
    {
        try {
            $techlifyUpdate = TechlifyUpdate::whereDoesntHave('views', function (Builder $query) {
                $query->where('user_id', auth()->id());
            })
                ->where('created_at', '>=', auth()->user()->created_at)
                ->first();

            return response()->json(['item' => $techlifyUpdate], Response::HTTP_OK);
        } catch (Exception $error) {
            return response()->json([
                'error' => $error,
                'stack_trace' => $error->getTrace()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function updateAllViewForCurrentUser(Request $request)
    {
        try {
            $techlifyUpdates = TechlifyUpdate::whereDoesntHave('views', function (Builder $query) {
                $query->where('user_id', auth()->id());
            })
                ->get();
            foreach ($techlifyUpdates as $techlifyUpdate) {
                $existingView = TechlifyUpdateView::where('update_id', $techlifyUpdate->id)
                    ->where('user_id', auth()->id())
                    ->first();
                if ($existingView) {
                    continue;
                }
                $techlifyUpdateView = new TechlifyUpdateView();
                $techlifyUpdateView->update_id = $techlifyUpdate->id;
                $techlifyUpdateView->user_id = auth()->id();
                $techlifyUpdateView->viewed_on = Carbon::now();
                $techlifyUpdateView->save();
            }
            return response()->json(['item' => "Sucessfully Updated."], Response::HTTP_OK);
        } catch (Exception $error) {
            return response()->json([
                'error' => $error,
                'stack_trace' => $error->getTrace()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public static function convertDateToTz($date = null)
    {
        return $date ? Carbon::parse($date)->setTimezone(config('app.timezone', 'UTC')) : null;
    }
}
