<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\LaravelCore\Entities\ActivityLog;
use Modules\LaravelCore\Facades\DBO as DB;

class AuditLogController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $filters = request([
            "sort_by",
            "object_id",
            "user_id",
            "action",
            "object_filter_type",
            "object_type",
            "date_from",
            "date_to"
        ]);

        $perPage = request()->query('perPage', config('app.perPage', 25));
        return ActivityLog::filter($filters)->with(['user'])->paginate($perPage);
    }

    public function objectType()
    {
        return [
            'data' => ActivityLog::select(
                "object_type",
                DB::raw("COUNT(*) as count"),
            )
                ->groupBy('object_type')
                ->get()
        ];
    }

    public function show(ActivityLog $log)
    {
        $log->load(['user']);
        return ["item" => $log];
    }

    public function recentLog(Request $request)
    {
        $rules = [
            "model_id" => "required",
            "model_type" => "required"
        ];

        $this->validate(request(), $rules);

        return [
            "item" =>
            ActivityLog::with('user')
                ->where('object_id', request('model_id'))
                ->where('object_type', request('model_type'))
                ->orderBy('created_at', 'DESC')
                ->first()
        ];
    }
}