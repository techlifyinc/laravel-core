<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Exception;
use Modules\LaravelCore\Entities\NotificationUser;
use Modules\LaravelCore\Entities\NotificationUserTimeline;

class NotificationUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $filters = request(
            [
                "sort_by",
                "status_ids",
                "notification_id",
                "user_id"
            ]
        );

        // Filter notifications to be read for current user
        if (filter_var(
            request()->query('current_user', true),
            FILTER_VALIDATE_BOOLEAN
        )
        ) {
            $filters['user_id'] = auth()->id();
        }

        $isLoadRelations = filter_var(
            request()->query('isLoadRelations', true),
            FILTER_VALIDATE_BOOLEAN
        );
        $relations = ['status'];
        $perPage = request()->query('perPage', config('app.perPage', 25));

        $relations = request()->query('relations', $relations);
        if (is_string($relations) && !is_array($relations)) {
            $relations = explode(',', $relations);
        }

        if ($isLoadRelations === true && collect($relations)->count() > 0) {
            return NotificationUser::with($relations)->filter($filters)
                ->paginate($perPage);
        }
        return NotificationUser::filter($filters)->paginate($perPage);
    }

    /**
     * Update Status.
     *
     * @param Request          $request Request
     * @param NotificationUser $nUser   Notification User
     *
     * @return void
     */
    public function updateStatus(Request $request, NotificationUser $nUser)
    {
        try {
            $rules = [
                "status_id" 
                    => "required|exists:techlify_notification_user_statuses,id",
            ];

            $request->validate($rules);
            $nUser->status_id = request('status_id');
            NotificationUserTimeline::makeEntry($nUser);
            $nUser->save();
            $nUser->load(['status', 'notification']);
            return ["item" => $nUser];
        } catch (Exception $e) {
            report($e);
            return response()->json(
                ['error' => $e->getMessage(), 'trace' => $e->getTrace()],
                Response::HTTP_BAD_REQUEST
            );
        }
    }
}
