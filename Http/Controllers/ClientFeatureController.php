<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Exception;
use Illuminate\Http\Response;
use Modules\LaravelCore\Entities\ClientFeature;
use Modules\LaravelCore\Entities\Feature;

class ClientFeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('administration::index');
    }

    public function enable(Feature $feature)
    {
        try {
            $clientFeature = ClientFeature::where('feature_id', $feature->id)->first();
            if (!$clientFeature) {
                $clientFeature = new ClientFeature();
                $clientFeature->feature_id = $feature->id;
                $clientFeature->client_id = auth()->user()->client_id;
                $clientFeature->is_enabled = true;
                $clientFeature->creator_id = auth()->user()->id;
                $clientFeature->save();

                return response()->json(["msg" => "Feature Enabled"], Response::HTTP_OK);
            } else {

                if ($clientFeature->is_enabled) {
                    return response()->json(["msg" => "Feature Already Enabled"], Response::HTTP_OK);
                }

                $clientFeature->is_enabled = true;
                $clientFeature->save();
            }
        } catch (Exception $error) {
            return response()->json([
                'errors' => 'Request cannot be processed',
                'location' => 'toggleFeature',
                'feature' => $clientFeature
            ], Response::HTTP_BAD_REQUEST);
        }
        return response()->json(["msg" => "Feature Enabled"], Response::HTTP_OK);
    }

    public function disable(Feature $feature)
    {
        try {
            $clientFeature = ClientFeature::where('feature_id', $feature->id)->first();
            if (!$clientFeature) {
                $clientFeature = new ClientFeature();
                $clientFeature->feature_id = $feature->id;
                $clientFeature->client_id = auth()->user()->client_id;
                $clientFeature->is_enabled = false;
                $clientFeature->creator_id = auth()->user()->id;
                $clientFeature->save();

                return response()->json(["msg" => "Feature Disabled"], Response::HTTP_OK);
            } else {


                if (!$clientFeature->is_enabled) {
                    return response()->json(["msg" => "Feature Already Disabled"], Response::HTTP_OK);
                }

                $clientFeature->is_enabled = false;
                $clientFeature->save();
            }
        } catch (Exception $error) {
            return response()->json([
                'errors' => 'Request cannot be processed',
                'location' => 'toggleFeature',
                'feature' => $clientFeature
            ], Response::HTTP_BAD_REQUEST);
        }
        return response()->json(["msg" => "Feature Disabled"], Response::HTTP_OK);
    }
}
