<?php

namespace Modules\LaravelCore\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\LaravelCore\Entities\Notification;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $filters = request(
            [
                "sort_by",
                "type_ids",
                "status_ids",
                "related_model_id",
                "related_model"
            ]
        );

        $isLoadRelations = filter_var(
            request()->query('isLoadRelations', true),
            FILTER_VALIDATE_BOOLEAN
        );
        $relations = ['type', 'status'];
        $perPage = request()->query('perPage', config('app.perPage', 25));

        $relations = request()->query('relations', $relations);
        if (is_string($relations) && !is_array($relations)) {
            $relations = explode(',', $relations);
        }

        if ($isLoadRelations === true && collect($relations)->count() > 0) {
            return Notification::with($relations)->filter($filters)
                ->paginate($perPage);
        }
        return Notification::filter($filters)->paginate($perPage);
    }
}
