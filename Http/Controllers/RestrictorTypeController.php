<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\LaravelCore\Entities\RestrictorType;

class RestrictorTypeController extends Controller
{
    /**
     * Get the list of restrictor types.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $perPage = $request->get('perPage', 25);

        $types = RestrictorType::query()
            ->paginate($perPage);

        return response()->json($types);
    }
}
