<?php

namespace Modules\LaravelCore\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Modules\LaravelCore\Entities\TechlifyImport;

class TechlifyImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $filters = request([
            'model_type',
            'sort_by',
        ]);

        $perPage = request()->query('perPage', 25);

        $histories = TechlifyImport::query()
            ->filter($filters)
            ->with('creator')
            ->paginate($perPage);

        return response()->json($histories);
    }
}
