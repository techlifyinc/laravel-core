<?php

namespace Modules\LaravelCore\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\LaravelCore\Entities\NotificationSubscription;

class NotificationSubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $filters = request(
            [
                "sort_by",
                "entity_ids",
                "type_ids",
                "subscriber_id",
                "creator_id"
            ]
        );

        $isLoadRelations = filter_var(
            request()->query('isLoadRelations', true),
            FILTER_VALIDATE_BOOLEAN
        );
        $relations = ['entity', 'subscriber'];
        $perPage = request()->query('perPage', config('app.perPage', 25));

        $relations = request()->query('relations', $relations);
        if (is_string($relations) && !is_array($relations)) {
            $relations = explode(',', $relations);
        }

        if ($isLoadRelations === true && collect($relations)->count() > 0) {
            return NotificationSubscription::with($relations)
                ->filter($filters)
                ->paginate($perPage);
        }
        return NotificationSubscription::filter($filters)->paginate($perPage);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function unique()
    {
        $filters = request(
            [
                "sort_by",
                "entity_ids",
                "type_ids",
                "subscriber_id",
                "creator_id"
            ]
        );
        $perPage = request()->query('perPage', config('app.perPage', 25));

        return NotificationSubscription::select('subscriber_id', 'entity_id')
            ->with(['entity'])
            ->filter($filters)
            ->groupBy('subscriber_id', 'entity_id')     //Get unique subscribers
            ->paginate($perPage);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request Request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            "entity_id" =>
            "required|exists:techlify_notification_subscription_entities,id",
            "type_id" => "required|exists:techlify_notification_types,id",
            "subscriber_id" => "required"
        ];

        $request->validate($rules);

        $subscription = new NotificationSubscription();
        try {
            $subscription->entity_id = request('entity_id');
            $subscription->type_id = request('type_id');
            $subscription->subscriber_id = request('subscriber_id');
            $subscription->creator_id = auth()->id();
            $subscription->client_id = auth()->user()->client_id;
            $subscription->save();
            $subscription->refresh();
        } catch (Exception $e) {
            report($e);
            return response()->json(
                [
                    'error' => $e->getMessage(),
                    'trace' => $e->getTrace()
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
        return response()->json(['item' => $subscription], Response::HTTP_OK);
    }


    /**
     * Update a resource in storage.
     * 
     * @param Request                  $request      Request
     * @param NotificationSubscription $subscription Notication Subscription
     * 
     * @return Response
     */
    public function update(Request $request, NotificationSubscription $subscription)
    {
        $rules = [
            "entity_id"
            => "required|exists:techlify_notification_subscription_entities,id",
            "type_id" => "required|exists:techlify_notification_types,id",
            "subscriber_id" => "required"
        ];

        $request->validate($rules);

        try {
            $subscription->entity_id = request('entity_id');
            $subscription->type_id = request('type_id');
            $subscription->subscriber_id = request('subscriber_id');
            $subscription->save();
            $subscription->refresh();
        } catch (Exception $e) {
            report($e);
            return response()->json(
                ['error' => $e->getMessage(), 'trace' => $e->getTrace()],
                Response::HTTP_BAD_REQUEST
            );
        }
        return response()->json(['item' => $subscription], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param NotificationSubscription $subscription Notification Subscription.
     * 
     * @return Response
     */
    public function destroy(NotificationSubscription $subscription)
    {
        try {
            $subscription->delete();
            return ["status" => "success"];
        } catch (Exception $e) {
            report($e);
            return response()->json(
                ['error' => $e->getMessage(), 'trace' => $e->getTrace()],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * Subscribe or Unsubscribe notification.
     *
     * @param NotificationSubscription $subscription Notification Subscription
     *
     * @return void
     */
    public function toggle(NotificationSubscription $subscription)
    {
        try {
            $subscription->is_subscribed = !$subscription->is_subscribed;
            $subscription->save();
            return ["status" => "success"];
        } catch (Exception $e) {
            report($e);
            return response()->json(
                ['error' => $e->getMessage(), 'trace' => $e->getTrace()],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * Subscribe for User.
     *
     * @param Request $request Request
     *
     * @return Response
     */
    public function subscribeForUser(Request $request)
    {
        $rules = [
            "entity_id"
            => "required|exists:techlify_notification_subscription_entities,id",
            "type_id" => "required|exists:techlify_notification_types,id",
        ];

        $request->validate($rules);

        $subscription = new NotificationSubscription();
        try {
            $subscription->entity_id = request('entity_id');
            $subscription->type_id = request('type_id');
            $subscription->subscriber_id = auth()->id();
            $subscription->creator_id = auth()->id();
            $subscription->client_id = auth()->user()->client_id;
            $subscription->save();
            $subscription->refresh();
        } catch (Exception $e) {
            report($e);
            return response()->json(
                ['error' => $e->getMessage(), 'trace' => $e->getTrace()],
                Response::HTTP_BAD_REQUEST
            );
        }
        return response()->json(['item' => $subscription], Response::HTTP_OK);
    }
}
