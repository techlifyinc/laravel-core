<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\LaravelCore\Entities\EmailSubscriptionFrequency;

class EmailSubscriptionFrequencyController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $filters = request([
            "sort_by",
            "search"
        ]);

        $perPage = request()->query('perPage', config('app.perPage', 25));
        return EmailSubscriptionFrequency::filter($filters)->paginate($perPage);
    }
}
