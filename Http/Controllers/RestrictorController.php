<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\LaravelCore\Http\Helpers\RestrictorHelper;
use Modules\LaravelCore\Http\Requests\RestrictorRequest;
use Modules\LaravelCore\Entities\Restrictor;

class RestrictorController extends Controller
{
    /**
     * Update the restrictor record.
     *
     * @param RestrictorRequest $request
     * @param Restrictor $restrictor
     * @return JsonResponse
     */
    public function update(RestrictorRequest $request, Restrictor $restrictor): JsonResponse
    {
        $restrictor = $this->prepareAndSave($restrictor, $request);
        return response()->json($restrictor);
    }

    /**
     * Get the restrictor.
     *
     * @param Restrictor $restrictor
     * @return JsonResponse
     */
    public function show(Restrictor $restrictor): JsonResponse
    {
        return response()->json($restrictor);
    }

    /**
     * Get the list of restrictors.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $filters = $request->only([
           'type_ids',
           'level_ids',
           'status_ids',
            'sort_by',
        ]);

        $perPage = $request->get('perPage', 25);

        $restrictors = Restrictor::filter($filters)
            ->paginate($perPage);

        return response()->json($restrictors);
    }

    /**
     * Create a restrictor record.
     *
     * @param RestrictorRequest $request
     * @return JsonResponse
     */
    public function store(RestrictorRequest $request): JsonResponse
    {
        $restrictor = new Restrictor();
        $restrictor = $this->prepareAndSave($restrictor, $request, true);
        return response()->json($restrictor);
    }

    /**
     * Create/Update restrictor record from request.
     *
     * @param Restrictor $restrictor
     * @param RestrictorRequest $request
     * @param bool $isCreateMode
     * @return Restrictor
     */
    private function prepareAndSave(
        Restrictor $restrictor,
        RestrictorRequest $request,
        bool $isCreateMode = false
    ): Restrictor
    {
        $restrictor->type_id = $request->get('type_id');
        $data = array();
        if ($request->get('type_id') === 1) {
            $data['start_time'] = $request->get('start_time');
            $data['end_time'] = $request->get('end_time');
        } else {
            $data['ip_range'] = $request->get('ip_range');
        }
        $restrictor->data = $data;

        $restrictor->level_id = $request->get('level_id');
        $restrictor->restrictable_type = RestrictorHelper::getRestrictableTypeFromNumber(
            $request->get('level_id')
        );
        $restrictor->restrictable_id = $request->get('restricted_object_id');
        $restrictor->notes = $request->get('notes', '');
        // set a client and creator only on create mode.
        if ($isCreateMode) {
            $user = auth()->user();
            $restrictor->creator_id = $user->id;
            $restrictor->client_id = $user->client_id;
        }
        $restrictor->save();

        return $restrictor;
    }
}
