<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Exception;
use Modules\LaravelCore\Entities\Note;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $filters = request([
            "model_type",
            "sort_by",
            "related_model_id",
            "creator_id"
        ]);

        $perPage = request()->query('perPage', config('app.perPage', 25));
        return Note::filter($filters)->paginate($perPage);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $note = new Note();
        try {
            $rules = [
                "note" => "required",
                "model_type" => "required",
                "related_model_id" => "required"
            ];

            $request->validate($rules);

            $note->note = request('note');
            $note->related_model_id = request('related_model_id');
            $note->model_type = request('model_type');
            $note->creator_id = auth()->id();
            $note->client_id = auth()->user()->client_id;
            $note->save();
            $note->load('creator');

        } catch (Exception $e) {
            return response()->json(['message' => 'Request cannot be processed', "error" => $e], Response::HTTP_BAD_REQUEST);
        }
        return response()->json(['item' => $note], Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Note $note)
    {

        try {
            $rules = [
                "note" => "required",
                "model_type" => "required",
                "related_model_id" => "required"
            ];

            $request->validate($rules);

            $note->note = request('note');
            $note->related_model_id = request('related_model_id');
            $note->model_type = request('model_type');
            $note->save();
        } catch (Exception $e) {
            return response()->json(['errors' => 'Request cannot be processed'], Response::HTTP_BAD_REQUEST);
        }
        return response()->json(['item' => $note], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Note $note)
    {
        if ($note->delete()) {
            return ["status" => "success"];
        }
        return response()->json(['errors' => 'Request cannot be processed'], Response::HTTP_BAD_REQUEST);
    }
}
