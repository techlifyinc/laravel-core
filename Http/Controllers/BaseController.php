<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class BaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Base function to get paginated results from the builder
     * We can make use of this function in all index methods
     * This will automatically fetch and parse the pagination query params and paginate the data
     * Eg usage: $this->getPaginatedResults(<builder>)
     * @param Builder $builder
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    protected function getPaginatedResults(Builder $builder)
    {
        $parts = [];
        if (request('num_items', false)) {
            $parts = explode("|", request('num_items'));
        }

        return $builder->paginate
        (
            count($parts) ? $parts[0] : request()->query('perPage', config('app.perPage', 25)),
            ['*'],
            'page',
            count($parts) > 1 ? $parts[1] : request()->query('page', 1)
        );
    }
}