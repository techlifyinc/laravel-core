<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\LaravelCore\Entities\NotificationUserTimeline;

class NotificationUserTimelineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $filters = request(
            [
                "sort_by",
                "status_ids",
                "notification_user_id",
            ]
        );

        $isLoadRelations = filter_var(
            request()->query('isLoadRelations', true),
            FILTER_VALIDATE_BOOLEAN
        );
        $relations = ['status'];
        $perPage = request()->query('perPage', config('app.perPage', 25));

        $relations = request()->query('relations', $relations);
        if (is_string($relations) && !is_array($relations)) {
            $relations = explode(',', $relations);
        }

        if ($isLoadRelations === true && collect($relations)->count() > 0) {
            return NotificationUserTimeline::with($relations)->filter($filters)
                ->paginate($perPage);
        }
        return NotificationUserTimeline::filter($filters)->paginate($perPage);
    }
}
