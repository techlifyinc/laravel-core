<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\LaravelCore\Entities\RestrictorLevel;

class RestrictorLevelController extends Controller
{
    /**
     * Get the list of restrictor levels.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $perPage = $request->get('perPage', 25);

        $levels = RestrictorLevel::query()
            ->paginate($perPage);

        return response()->json($levels);
    }
}
