<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Modules\LaravelCore\Entities\RbacClientConfiguration;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class RbacClientConfigurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $config = RbacClientConfiguration::query()
            ->orderBy('client_id', 'desc')
            ->first();
        return response()
            ->json(['data' => $config], Response::HTTP_OK);
    }

    public function create(Request $request)
    {
        $rules = [
            "max_failed_consecutive_login_attempts" => "required",
            "is_track_old_passwords" => "required",
            "max_old_passwords_block" => "required",
            "is_password_require_number" => "required",
            "is_password_require_symbol" => "required",
            "is_password_require_capital_letter" => "required",
        ];
        $this->validate(request(), $rules);

        $config = RbacClientConfiguration::query()
            ->where('client_id', auth()->user()->client_id)
            ->first();

        if ($config) {
            return response()->json(['error' => "Only one active config allowed."], Response::HTTP_BAD_REQUEST);
        }

        try {
            $config = new RbacClientConfiguration();
            $config->max_failed_consecutive_login_attempts = $request->max_failed_consecutive_login_attempts;
            $config->is_track_old_passwords = $request->is_track_old_passwords;
            $config->max_old_passwords_block = $request->max_old_passwords_block;
            $config->is_password_require_number = $request->is_password_require_number;
            $config->is_password_require_symbol = $request->is_password_require_symbol;
            $config->is_password_require_capital_letter = $request->is_password_require_capital_letter;
            $config->min_password_length = $request->min_password_length ?: null;
            $config->client_id = auth()->user()->client_id;
            $config->creator_id = auth()->id();
            $config->save();

            return response()->json(['item' => $config], Response::HTTP_OK);
        } catch (Exception $error) {
            return response()->json(['error' => $error], Response::HTTP_BAD_REQUEST);
        }
    }

    public function update(Request $request, RbacClientConfiguration $config)
    {
        $rules = [
            "max_failed_consecutive_login_attempts" => "required",
            "is_track_old_passwords" => "required",
            "max_old_passwords_block" => "required",
            "is_password_require_number" => "required",
            "is_password_require_symbol" => "required",
            "is_password_require_capital_letter" => "required",
        ];
        $this->validate(request(), $rules);

        try {
            $config->max_failed_consecutive_login_attempts = $request->max_failed_consecutive_login_attempts;
            $config->is_track_old_passwords = $request->is_track_old_passwords;
            $config->max_old_passwords_block = $request->max_old_passwords_block;
            $config->is_password_require_number = $request->is_password_require_number;
            $config->is_password_require_symbol = $request->is_password_require_symbol;
            $config->is_password_require_capital_letter = $request->is_password_require_capital_letter;
            $config->min_password_length = $request->min_password_length ?: null;
            $config->save();

            return response()->json(['item' => $config], Response::HTTP_OK);
        } catch (Exception $error) {
            return response()->json(['error' => $error], Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy(RbacClientConfiguration $config)
    {
        try {
            $config->delete();
            return response()->json(['item' => $config], Response::HTTP_OK);
        } catch (Exception $error) {
            return response()->json(['error' => $error], Response::HTTP_BAD_REQUEST);
        }
    }

    public function getConfigForCurrentClient(Request $request)
    {
        try {
            $config = RbacClientConfiguration::where('client_id', auth()->user()->client_id)
                ->first();
            return response()->json(['item' => $config], Response::HTTP_OK);
        } catch (Exception $error) {
            return response()->json(['error' => $error], Response::HTTP_BAD_REQUEST);
        }
    }
}
