<?php

namespace Modules\LaravelCore\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\LaravelCore\Entities\Module;
use Modules\LaravelCore\Entities\Permission;
use Modules\LaravelCore\Entities\Role;
use Modules\LaravelCore\Entities\UserType;

class RoleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $filters = request([
            'module_code',
            'client_id',
        ]);
        // default count need to be loaded
        $withCount = [
            'roleUsers' => function ($query) {
                $query->where(
                    'client_id',
                    auth()->user()->user_type_id == UserType::APP_ADMIN
                        ? request('client_id')
                        : auth()->user()->client_id
                );
            },
        ];
        // Add the count as per api.
        if ($request->get('withCount')) {
            $withCount = array_merge(
                $withCount,
                explode(',', $request->get('withCount'))
            );
        }

        $roles = Role::query()
            ->filter($filters)
            ->where('is_disabled', false)
            ->orderBy("label")
            ->with([
                'module',
                'creator',
            ])
            ->withCount($withCount);

        if (filter_var($request->get('loadPermissions', false), FILTER_VALIDATE_BOOLEAN)) {
            $roles = $roles->with("permissions");
        }

        return response()->json(["data" => $roles->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "label" => "required|string",
        ];

        if (request('module_code')) {
            $rules["module_code"] = "exists:modules,code";
        }

        $this->validate(request(), $rules);

        $role = new Role();
        $role->label = request('label');
        $role->description = request('description', '');
        $role->is_editable = true;
        $role->client_id = auth()->user()->client_id;
        $role->creator_id = auth()->id();
        $role->slug = $role->client_id . "-" . strtolower($role->label);

        if (request('module_code')) {
            $module = Module::where('code', request('module_code'))
                ->first();

            if ($module) {
                $role->module_id = $module->id;
            }
        }

        if (auth()->user()->user_type_id == UserType::APP_ADMIN) {
            $role->client_id = request('client_id', auth()->user()->client_id);
            $role->is_editable = request('is_editable', true);
            $role->is_global = request('is_global', false);
        }

        if (!$role->save()) {
            return response()->json(['error' => "Failed to save the role. "], 422);
        }

        return ["item" => $role];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            "label" => "required|string",
        ];

        if (request('module_code')) {
            $rules["module_code"] = "exists:modules,code";
        }

        $this->validate(request(), $rules);

        $role = Role::find($id);
        if (!$role) {
            return response()->json(['error' => "Invalid Role data sent. "], 422);
        }

        $role->label = request('label');
        $role->description = request('description', '');
        $role->slug = $role->client_id . "-" . strtolower($role->label);
        $role->is_editable = true;


        if (request('module_code')) {
            $module = Module::where('code', request('module_code'))
                ->first();

            if ($module) {
                $role->module_id = $module->id;
            }
        }


        if (auth()->user()->user_type_id == UserType::APP_ADMIN) {
            $role->is_editable = request('is_editable', true);
            $role->is_global = request('is_global', false);
        }

        if (!$role->save()) {
            return response()->json(['error' => "Failed to save the role. "], 422);
        }

        return ["item" => $role];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        if (!$role) {
            return response()->json(['error' => "Invalid Role data sent. "], 422);
        }

        if (!$role->delete()) {
            return response()->json(['error' => "Failed to delete the role. "], 422);
        }

        return ["item" => $role];
    }

    /**
     * Adds a permission to a role
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addPermission(Role $role, Permission $permission)
    {
        if ($role->hasPermission($permission)) {
            return response()->json(['error' => "The Role already has the specified permission. "], 422);
        }

        if ($role->is_global || !$role->is_editable) {
            return response()->json(['error' => "You don't have an access to modify this role."], 422);
        }

        $role->givePermission($permission);
        $role->load('permissions');

        return ["role" => $role];
    }

    /**
     * Removes a permission from a role
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removePermission(Role $role, Permission $permission)
    {
        if (!$role->hasPermission($permission->slug)) {
            return response()->json(['error' => "The Role does not have the specified permission. "], 422);
        }

        if ($role->is_global || !$role->is_editable) {
            return response()->json(['error' => "You don't have an access to modify this role."], 422);
        }

        $role->removePermission($permission);
        $role->load('permissions');
        return ["role" => $role];
    }

    public function show(Request $request)
    {
        try {
            $role = Role::where('id', $request->id)
                ->with('permissions')
                ->withCount(
                    [
                        'roleUsers' => function ($query) use ($request) {
                            $query->where('client_id', auth()->user()->user_type_id == UserType::APP_ADMIN ? $request->client_id : auth()->user()->client_id);
                        },
                    ]
                )
                ->first();

            return response()->json(['item' => $role], Response::HTTP_OK);
        } catch (Exception $error) {
            return response()->json(['error' => "Failed to get the role. "], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }
}
