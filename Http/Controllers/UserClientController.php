<?php

namespace Modules\LaravelCore\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\LaravelCore\Entities\UserClient;

class UserClientController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $perPage = request()->query('perPage');
        $userClient = UserClient::where('user_id', auth()->id());

        if (empty($perPage)) {
            return ["data" => $userClient->get()];
        }

        return $userClient->paginate($perPage);
    }
}
