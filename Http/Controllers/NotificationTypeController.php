<?php

namespace Modules\LaravelCore\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Modules\LaravelCore\Entities\NotificationType;

class NotificationTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $filters = request(
            [
                "sort_by",
                "search",
                "is_admin"
            ]
        );

        $isLoadRelations = filter_var(
            request()->query('isLoadRelations', true),
            FILTER_VALIDATE_BOOLEAN
        );
        $relations = [];
        $perPage = request()->query('perPage', config('app.perPage', 25));

        $relations = request()->query('relations', $relations);
        if (is_string($relations) && !is_array($relations)) {
            $relations = explode(',', $relations);
        }

        if ($isLoadRelations === true && collect($relations)->count() > 0) {
            return NotificationType::with($relations)->filter($filters)
                ->paginate($perPage);
        }
        return NotificationType::filter($filters)->paginate($perPage);
    }


    /**
     * Store a newly created resource in storage.
     * 
     * @param Request $request Request
     * 
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            "title" => "required",
            "code" => "required|unique:techlify_notification_types,code",
        ];

        $request->validate($rules);
        $notificationType = new NotificationType();

        try {
            $notificationType->title = request('title');
            $notificationType->code = request('code');
            $notificationType->is_admin = request('is_admin', false);
            $notificationType->description = request('description', null);
            $notificationType->save();
        } catch (Exception $e) {
            report($e);
            return response()->json(
                ['error' => $e->getMessage(), 'trace' => $e->getTrace()],
                Response::HTTP_BAD_REQUEST
            );
        }
        return response()->json(
            ['item' => $notificationType],
            Response::HTTP_CREATED
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request          $request          Request
     * @param NotificationType $notificationType Notification Type     
     *
     * @return Response
     */
    public function update(Request $request, NotificationType $notificationType)
    {
        $rules = [
            "title" => "required",
            "code" => "required|unique:techlify_notification_types,code,"
            . $notificationType->id,
        ];

        $request->validate($rules);
        try {
            $notificationType->title = request('title');
            $notificationType->code = request('code');
            $notificationType->is_admin = request('is_admin', false);
            $notificationType->description = request('description', null);
            $notificationType->save();
        } catch (Exception $e) {
            report($e);
            return response()->json(
                ['error' => $e->getMessage(), 'trace' => $e->getTrace()],
                Response::HTTP_BAD_REQUEST
            );
        }
        return response()->json(['item' => $notificationType], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param NotificationType $notificationType Notification Type
     *
     * @return Response
     */
    public function destroy(NotificationType $notificationType)
    {
        try {
            DB::beginTransaction();
            $notificationType->code = Carbon::now()->timestamp . '_'
                . $notificationType->code;
            $notificationType->delete();
            DB::commit();
            return ["status" => "success"];
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return response()->json(
                ['error' => $e->getMessage(), 'trace' => $e->getTrace()],
                Response::HTTP_BAD_REQUEST
            );
        }
    }
}