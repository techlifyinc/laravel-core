<?php

namespace Modules\LaravelCore\Listeners;

use Illuminate\Support\Facades\Mail;
use App\Mail\ClientSubscriptionMail;

class CreateDefaultClientSubscriptionsAccessControlsListener
{

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $subscription = $event->subscription;

        if ($subscription->client->email) {
            Mail::to($subscription->client->email)->queue(new ClientSubscriptionMail($subscription));
        }
    }
}
