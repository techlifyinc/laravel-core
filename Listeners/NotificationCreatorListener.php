<?php

namespace Modules\LaravelCore\Listeners;

use Modules\LaravelCore\Entities\Notification;
use Modules\LaravelCore\Events\NotificationCreatorEvent;
use Modules\LaravelCore\Jobs\SendMobileNotificationJob;

class NotificationCreatorListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NotificationCreatorEvent $event Notification Creator Event
     *
     * @return void
     */
    public function handle(NotificationCreatorEvent $event)
    {
        $eventNotification = $event->notification;
        $notification = new Notification();

        $notification->title = $eventNotification['title'];
        $notification->body = $eventNotification['body'] ?? null;
        $notification->type_id = $eventNotification['type_id'];
        $notification->status_id = $eventNotification['status_id'];
        $notification->related_model_id
            = $eventNotification['related_model_id'] ?? null;
        $notification->related_model = $eventNotification['related_model'] ?? null;
        $notification->constraints = $eventNotification['constraints'] ?? null;
        $notification->creator_id = $eventNotification['creator_id'];
        $notification->client_id = $eventNotification['client_id'];

        $notification->save();

        SendMobileNotificationJob::dispatch(
            $notification->title,
            $notification->body,
            $eventNotification['info'],
            $eventNotification['user_tokens']
        );
    }
}
