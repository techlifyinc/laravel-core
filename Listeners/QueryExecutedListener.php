<?php

namespace Modules\LaravelCore\Listeners;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\File;

/**
 * Listener class, that listens to the all queries happening in the system.
 * Used to print query logs for debugging.
 * To enable the logs, set the env variable IS_ENABLE_QUERY_LOGS=true
 * 
 * 
 * 
 * In event service provider you need to register the event.
 * Imports details
 * use App\Listeners\QueryExecutedListener;
 * use Illuminate\Database\Events\QueryExecuted;
 * 
 * Then map the event in $listen array, like below
 *  QueryExecuted::class => [
 *           QueryExecutedListener::class,
 *      ]
 * 
 * 
 * Eg usage: 
 *  public function show($id, Request $request)
 *  {
 *      QueryExecutedListener::startLogging();
 *      . //Code logic goes here
 *      .
 *      QueryExecutedListener::stopLogging();
 */
class QueryExecutedListener
{
    static $isLogsEnabled = false;

    /**Method used to enable query logs */
    public static function startLogging()
    {
        self::$isLogsEnabled = true;
    }

    /**Method used to disable query logs */
    public static function stopLogging()
    {
        self::$isLogsEnabled = false;
    }

    public function handle(QueryExecuted $query)
    {
        //Log queries if it is enabled in env via app config and log is activated
        if (config('app.is_enable_query_logs', false) && self::$isLogsEnabled) {
            File::append(
                storage_path('/logs/query.log'),
                '[' . date('Y-m-d H:i:s') . ']' . PHP_EOL . $this->getSqlWithBindings($query) . PHP_EOL . PHP_EOL
            );
        }
    }

    public function getSqlWithBindings(QueryExecuted $query)
    {
        $sql = $query->sql;
        $bindings = $query->bindings;
        return preg_replace_callback('/\?/', function ($match) use ($sql, &$bindings) {
            return json_encode(array_shift($bindings));
        }, $sql);
    }
}