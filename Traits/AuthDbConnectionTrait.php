<?php

namespace Modules\LaravelCore\Traits;

trait AuthDbConnectionTrait
{
    //Set the database connection for model
    public function getConnectionName()
    {
        $this->connection = config('database.auth_db_connection', 'mysql');
        return parent::getConnectionName();
    }
}