<?php

namespace Modules\LaravelCore\Traits;

trait AppDbConnectionTrait
{
    //Set the database connection for model to app default db.
    public function getConnectionName()
    {
        $this->connection = config('database.default', 'mysql');
        return parent::getConnectionName();
    }
}