<?php

namespace Modules\LaravelCore\Entities;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TechlifyUpdate extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [];

    protected $casts = [
        'released_on' => 'datetime'
    ];

    protected $with = [
        'creator'
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, "creator_id", "id");
    }

    public function views()
    {
        return $this->hasMany(TechlifyUpdateView::class, "update_id", "id");
    }

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['search']) && "" != trim($filters['search'])) {
            $query->where('title', 'LIKE', '%' . $filters['search'] . '%');
        }
        if (isset($filters['version']) && "" != trim($filters['version'])) {
            $query->where('version', 'LIKE', "%" . $filters['version'] . "%");
        }
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }
        if (isset($filters['for_current_user']) && filter_var($filters['for_current_user'], FILTER_VALIDATE_BOOLEAN)) {
            $query->whereDoesntHave('views', function ($query) {
                $query->where('user_id', auth()->id());
            })
                //To fix illegal operator issue, check if user has valid created_at
                ->when(auth()->user()->created_at != null, function ($query) {
                    $query->where('created_at', '>=', auth()->user()->created_at);
                });
        }
    }
}