<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailSubscriptionType extends Model
{
    // use SoftDeletes;

    protected $fillable = [];

    protected $table = 'email_subscription_types';

    public function frequency()
    {
        return $this->belongsTo(EmailSubscriptionFrequency::class, 'frequency_id', 'id');
    }

    public function currentUserSubscription()
    {
        return $this->hasOne(EmailSubscriptionUser::class, 'email_type_id', 'id')
            ->where('user_id', auth()->id());
    }

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['search']) && "" != trim($filters['search'])) {
            $query->where('title', 'like', '%' . $filters['search'] . '%');
        }

        if (isset($filters['frequency_id']) && "" != trim($filters['frequency_id'])) {
            $query->where('frequency_id', $filters['frequency_id']);
        }
    }
}
