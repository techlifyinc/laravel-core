<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Laravel\Passport\Token;

/**
 * The TechlifyUser class used to represent the User class and has all the core functionality.
 *
 * @property mixed $id
 * @property mixed $client_id
 */
class TechlifyUser extends Authenticatable
{
    use LoggableModel;
    use Notifiable;
    use HasApiTokens;
    use LaravelRbac;

    protected $table = 'users';

    /**
     * User Type
     *
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(UserType::class, 'user_type_id');
    }

    /**
     * The client user belongs to.
     *
     * @return BelongsTo
     */
    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    /**
     * The user can have many clients.
     *
     * @return HasMany
     */
    public function clients(): HasMany
    {
        return $this->hasMany(UserClient::class, 'user_id', 'id');
    }

    /**
     * Disable the user.
     *
     * @return Model|Builder
     */
    public function disableUser(): Model|Builder
    {
        // Disable all the tokens
        $this->tokens()->get()->each(function (Token $token) {
            $token->revoke();
        });

        return UserClient::query()
            ->updateOrCreate(
                [
                    'client_id' => $this->client_id,
                    'user_id' => $this->id
                ],
                [
                    'client_id' => $this->client_id,
                    'user_id' => $this->id,
                    'creator_id' => auth()?->id(),
                    'is_enabled' => false,
                ]
            );
    }

    /**
     * Enable the user.
     *
     * @return Model|Builder
     */
    public function enableUser(): Model|Builder
    {
        return UserClient::query()
            ->updateOrCreate(
                [
                    'client_id' => $this->client_id,
                    'user_id' => $this->id
                ],
                [
                    'client_id' => $this->client_id,
                    'user_id' => $this->id,
                    'creator_id' => auth()?->id(),
                    'is_enabled' => true,
                    'enabled_on' => now(),
                ]
            );
    }
}
