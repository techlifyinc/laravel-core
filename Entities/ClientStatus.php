<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;

class ClientStatus extends Model
{
    protected $fillable = [];

    const ACTIVE = 1;
    const PENDING_REVIEW = 2;
    const DISABLED = 3;
}
