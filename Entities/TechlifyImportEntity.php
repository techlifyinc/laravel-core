<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;

class TechlifyImportEntity extends Model
{
    /**
     * Static function to quickly create an import entity
     */
    public static function createImportEntity($importId, $modelType, $modelId, $rawData = []): TechlifyImportEntity
    {
        $importEntity = new TechlifyImportEntity();
        $importEntity->import_id = $importId;
        $importEntity->model_type = $modelType;
        $importEntity->model_id = $modelId;
        $importEntity->raw_data = $rawData;
        $importEntity->save();

        return $importEntity;
    }
}
