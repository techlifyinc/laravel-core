<?php

namespace Modules\LaravelCore\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Modules\LaravelCore\Entities\Client;
use Modules\LaravelCore\Entities\UserType;

class TechlifyModel extends Model
{
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('TechlifyScope', function ($query) {
            /**
             * If we're running in console and not logged in, then we don't do the client_id check
             * Consider logged in user details for unit testing
             */
            if ((!auth()->check()) && app()->runningInConsole()) {
                return;
            }

            /**
             * Let's ensure that we're limiting access by client_id
             */
            if (auth()->user() && auth()->user()->user_type_id != UserType::APP_ADMIN) {
                /* lets get the namespace of the class called*/
                $classCalled = get_called_class();
                /* Lets create the instance of the class */
                $item = new $classCalled();
                $query->where($item->getTable() . '.client_id', auth()->user()->client_id)
                    ->orWhere($item->getTable() . '.client_id', null);

                if (Schema::hasColumn($item->getTable(), 'is_global')) {
                    $query->orWhere($item->getTable() . '.is_global', true);
                }
            }
        });
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }
}
