<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $fillable = [];

    protected $table = 'techlify_fm_features';

    public function clientFeature()
    {
        return $this->hasOne(ClientFeature::class, 'feature_id', 'id');
    }

    public function scopeFilter($query, $filters)
    {
    }
}
