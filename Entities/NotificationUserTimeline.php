<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;

class NotificationUserTimeline extends Model
{
    protected $fillable = [];

    protected $table = 'techlify_notification_user_timelines';

    /**
     * Notification User Status
     *
     * @return mixed
     */
    public function status()
    {
        return $this->belongsTo(NotificationUserStatus::class, 'status_id', 'id');
    }

    /**
     * Apply filter on model.
     *
     * @param mixed $query   QueryBuilder
     * @param mixed $filters Filters
     *
     * @return void
     */
    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['status_ids']) && "" != trim($filters['status_ids'])) {
            $query->whereIn('status_id', json_decode($filters['status_ids']));
        }

        if (isset($filters['notification_user_id']) && "" != trim($filters['notification_user_id'])) {
            $query->where('notification_user_id', $filters['notification_user_id']);
        }
    }

    /**
     * Create a new NotoficationUserTimeline entry.
     *
     * @param NotificationUser $nUser Notification User.
     *
     * @return void
     */
    public static function makeEntry(NotificationUser $nUser)
    {
        $timeline = new NotificationUserTimeline();
        $timeline->notification_user_id = $nUser->id;
        $timeline->status_id = $nUser->status_id;
        $timeline->save();
    }
}
