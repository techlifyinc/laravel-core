<?php
namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Modules\LaravelCore\Entities\Helpers\TenantSubscriptionHelper;

class TenantSubscriptionScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        /**
         * If we're running in console and not logged in, then we don't do the tenant check
         * Consider logged in user details for unit testing
         */
        if ((!auth()->check()) && app()->runningInConsole()) {
            return;
        }

        /**
         * Let's ensure that we're limiting access by tenant permission for other than app admin
         */
        if (auth()->user() && auth()->user()->user_type_id != UserType::APP_ADMIN) {
            $activeSubscriptions = TenantSubscriptionHelper::getActiveSubscriptions();
            if (count($activeSubscriptions) == 0) {
                //If there is no active subscriptions, then retun all permissions
                return;
            }
            $allowedPermissionIds = TenantSubscriptionHelper::getAllowedPermissionIds();
            //Add a condition to query builder to restrict permissions
            $builder->whereIn('permissions.id', $allowedPermissionIds);
        }
    }

}