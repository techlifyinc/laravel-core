<?php

namespace Modules\LaravelCore\Entities;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserPasswordHistoryService extends Model
{

    public static function generateValidations()
    {
        $config = RbacClientConfigService::fetchCurrentClientConfig();
        $validations = array();
        if (!$config) {
            return $validations;
        }
        if ($config->is_password_require_capital_letter) {
            array_push($validations, 'regex:/(.*[A-Z].*)$/');
        }
        if ($config->is_password_require_symbol) {
            array_push($validations, 'regex:/(.*\W.*)$/');
        }
        if ($config->is_password_require_number) {
            array_push($validations, 'regex:/(.*\d.*)$/');
        }
        if ($config->min_password_length) {
            array_push($validations, 'min:' . $config->min_password_length);
        }

        return $validations;
    }


    // Create Password entry

    /**
     * Create record in user password history table.
     *
     * @param $user
     * @return RbacUserPasswordHistory|void
     * @throws Exception
     */
    public static function create($user)
    {
        try {
            $clientId = auth()->user()->client_id;
            $clientConfig = RbacClientConfigService::fetchCurrentClientConfig($clientId);

            if ($clientConfig && !$clientConfig->is_track_old_passwords) {
                return;
            }

            $passwordHistory = new RbacUserPasswordHistory();
            $passwordHistory->user_id = $user->id;
            $passwordHistory->old_password = $user->password;
            $passwordHistory->client_id = $clientId;
            $passwordHistory->save();

            return $passwordHistory;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function checkForPasswordBlock($user, $password)
    {
        try {
            $clientId = auth()->user()->client_id;

            $clientConfig = RbacClientConfigService::fetchCurrentClientConfig($clientId);

            if (!$clientConfig->max_old_passwords_block || !$clientConfig->is_track_old_passwords) {
                return;
            }

            $maxOldPasswordTake = $clientConfig->max_old_passwords_block
                ? $clientConfig->max_old_passwords_block - 1
                : $clientConfig->max_old_passwords_block;
            $oldPasswords = RbacUserPasswordHistory::where('client_id', $clientId)
                ->where('user_id', $user->id)
                ->latest()
                ->take($maxOldPasswordTake)
                ->pluck('old_password')
                ->toArray();

            $oldPasswords[] = $user->password;

            $oldPasswordExists = false;
            foreach ($oldPasswords as $oldPassword) {
                if (Hash::check($password, $oldPassword)) {
                    $oldPasswordExists = true;
                    break;
                }
            }
            return $oldPasswordExists;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
