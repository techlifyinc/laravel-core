<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use League\OAuth2\Server\Exception\OAuthServerException;

/**
 * Description of LaravelRbac
 *
 * @author
 */
trait LaravelRbac
{
    use SoftDeletes;

    /**
     * Find the user by username.
     *
     * @param $username
     * @return mixed
     * @throws OAuthServerException
     */
    public function findForPassport($username): mixed
    {
        $user = $this->where('email', $username)
            ->first();
        if (!$user) {
            // Throw incorrect email exception.
            $this->throwIncorrectEmailException();
        }

        $userTypes = explode(',', config('laravelcore.non_client_user_types', 1));

        if (in_array($user->user_type_id, $userTypes) || UserClient::all()->isEmpty()) {
            return $user;
        }

        $userClient = UserClient::where('user_id', $user->id)
            ->where('client_id', $user->client_id)
            ->where('is_enabled', true)
            ->whereHas('client', function ($q) {
                $q->where('status_id', ClientStatus::ACTIVE);
            })
            ->first();
        if (!$userClient) {
            LoginAttemptService::logUserAttempt($user, false);
            $anotherUserClient = UserClient::where('user_id', $user->id)
                ->where('is_enabled', true)
                ->whereHas('client', function ($q) {
                    $q->where('status_id', ClientStatus::ACTIVE);
                })
                ->first();
            if (!$anotherUserClient) {
                // No active user_client record found
                // Throw user disabled
                $this->throwUserAccountDisabledException();
            }
            $user->client_id = $anotherUserClient->client_id;
            $user->user_type_id = $anotherUserClient->user_type_id;
            $user->save();
        }
        return $user;
    }

    /**
     * Validated the entered password with user password.
     *
     * @throws OAuthServerException
     */
    public function validateForPassportPasswordGrant($password): bool
    {
        if (Hash::check($password, $this->password)) {
            LoginAttemptService::logUserAttempt($this, true);
            return true;
        }
        LoginAttemptService::logUserAttempt($this, false);
        // Get the remaining login attempts count
        $numberOfLoginAttemptsRemaining = LoginAttemptService::numberOfLoginAttemptsRemaining($this);
        if ($numberOfLoginAttemptsRemaining >= 1) {
            // Throw incorrect password exception.
            $this->throwIncorrectPasswordException($numberOfLoginAttemptsRemaining);
        } else {
            // Throw account locked exception.
            $this->throwAccountLockedException();
        }
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function type()
    {
        return $this->hasOne(UserType::class, 'id', 'user_type_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id')->with('subscriptions');
    }

    public function userClients()
    {
        return $this->hasMany(UserClient::class, 'user_id', 'id');
    }

    public function userClient()
    {
        return $this->hasOne(UserClient::class, 'user_id', 'id');
    }

    /**
     * Get the login attempts for the user
     *
     * @return HasMany
     */
    public function loginAttampts(): HasMany
    {
        return $this->hasMany(RbacLoginAttempt::class, 'user_id', 'id');
    }

    /**
     * Get the creator of the user.
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(TechlifyUser::class);
    }

    public function attachRole($role, $clientId = null)
    {
        $clientId = $clientId ?: auth()->user()->client_id;
        return $this->assignRole($role, $clientId);
    }

    public function assignRole($role, $clientId = null)
    {
        $clientId = $clientId ?: auth()->user()->client_id;
        $slug = (is_string($role)) ? $role : $role->slug;

        return $this->roles()->attach(
            Role::where("slug", $slug)->firstOrFail(),
            ['client_id' => $clientId]
        );
    }

    public function detachRole($role)
    {
        return $this->removeRole($role);
    }

    /**
     * Detach role from user.
     *
     * @param int|Role $role
     */
    public function removeRole($role)
    {
        $slug = (is_string($role)) ? $role : $role->slug;
        $this->roles()->wherePivot('client_id', auth()->user()->client_id)->detach(Role::where("slug", $slug)->firstOrFail());
    }

    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('slug', $role);
        }

        return !!$role->intersect($this->roles)->count();
    }

    /**
     * Check if user has permission to current operation
     *
     * @param string $slug
     * @return bool
     *
     * @todo Make this more efficient
     */
    public function hasPermission($permission)
    {
        $slug = (is_string($permission)) ? $permission : $permission->slug;
        //For app admins
        if ($this->user_type_id == UserType::APP_ADMIN) {
            return true;
        }

        //For client admins
        if ($this->user_type_id == UserType::CLIENT_ADMIN) {
            $requestedPermission = Permission::where('slug', $slug)
                ->first();
            if (!$requestedPermission) {
                //If permission is not allowed, then return false
                return false;
            }
            //If permission is allowed, then return true
            return true;
        }

        //For normal users
        foreach ($this->roles as $role) {
            if ($role->hasPermission($slug)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Adding Filtering to users
     *
     * @param type $query
     * @param type $filters
     *
     * Filters: 'search', 'name', 'email', 'enabled', 'role_ids', 'sort_by', 'num_items'
     */
    public function scopeFilter($query, $filters)
    {
        if (isset($filters['search'])) {
            $query->where(function ($q) use ($filters) {
                $q->where('name', 'LIKE', '%' . $filters['search'] . '%')
                    ->orWhere('email', 'LIKE', '%' . $filters['search'] . '%');
            });
        }

        if (isset($filters['name']) && "" != trim($filters['name'])) {
            $query->where('name', 'LIKE', '%' . $filters['name'] . '%');
        }

        if (isset($filters['email']) && "" != trim($filters['email'])) {
            $query->where('email', 'LIKE', '%' . $filters['email'] . '%');
        }

        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['num_items']) && is_numeric($filters['num_items'])) {
            $query->limit($filters['num_items']);
        }

        if (isset($filters['user_type_ids'])) {
            $query->whereIn('user_type_id', explode(',', $filters['user_type_ids']));
        }

        if (isset($filters['role_ids'])) {
            $query->whereHas('roles', function ($q) use ($filters) {
                $q->whereIn('roles.id', explode(',', $filters['role_ids']));
            });
        }

        if (auth()->user()->user_type_id == UserType::APP_ADMIN) {
            if (isset($filters['client_id'])) {
                $query->whereHas('clients', function ($q) use ($filters) {
                    $q->where('client_id', $filters['client_id']);
                })->orWhere('client_id', $filters['client_id']);
            } else {
                // Return only admin users
                $query->whereNull('client_id');
            }
        } else {
            // Not an app admin user
            $query->where(function ($q) {
                $q->whereHas('clients', function ($q) {
                    $q->where('client_id', auth()->user()->client_id);
                })->orWhere('client_id', auth()->user()->client_id);
            });
        }

        if (
            isset($filters['enabled']) &&
            ($filters['enabled'] == '1' || $filters['enabled'] == 'true' || $filters['enabled'] === true)
        ) {
            $query->whereHas('userClient', function ($q) {
                $q->where('is_enabled', true)
                    ->where('client_id', auth()->user()->client_id);
            });
        }

        if (isset($filters['not_in_module_id']) && "" != trim($filters['not_in_module_id'])) {
            $clientId = auth()->user()->user_type_id == UserType::APP_ADMIN ? $filters['client_id'] : auth()->user()->client_id;
            $query->whereNotIn('id', function ($query) use ($filters, $clientId) {
                $query->select('user_id')
                    ->from('module_users')
                    ->where('module_id', $filters['not_in_module_id'])
                    ->where('client_id', $clientId);
            });
        }
    }

    /**
     * Get the last login attribute for the user.
     *
     * @return string|null
     */
    public function getLastLoginAttribute(): string|null
    {
        $lastLogin = $this->loginAttampts()
            ->where('is_successful', true)
            ->orderBy('created_at', 'desc')
            ->first();

        return $lastLogin?->created_at;
    }

    /**
     * Throw invalid email exception when user entered email does not exist in system.
     *
     * @throws OAuthServerException
     */
    public function throwIncorrectEmailException()
    {
        throw new OAuthServerException(
            'The credentials entered are incorrect please try again.',
            11,
            'incorrect_email'
        );
    }

    /**
     * Throw invalid email exception when user-entered email does not exist in a system.
     *
     * @throws OAuthServerException
     */
    public function throwIncorrectPasswordException($count)
    {
        throw new OAuthServerException(
            'The password entered is incorrect, you have ' . $count . ' more attempts. Your account would be locked afterwards.',
            12,
            'incorrect_password'
        );
    }

    /**
     * Throw account locked exception.
     *
     * @throws OAuthServerException
     */
    public function throwAccountLockedException()
    {
        throw new OAuthServerException(
            'Your account has been locked, please contact your System Administrator. ',
            13,
            'account_locked'
        );
    }

    /**
     * Throw exception for user account disabled
     *
     * @throws OAuthServerException
     */
    public function throwUserAccountDisabledException()
    {
        throw new OAuthServerException(
            'User account disabled, contact system administrator',
            14,
            'user_account_disabled'
        );
    }
}
