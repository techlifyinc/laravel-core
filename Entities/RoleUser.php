<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\LaravelCore\Traits\AppDbConnectionTrait;

class RoleUser extends Model
{
    use AppDbConnectionTrait;
    protected $guarded = [];
    protected $table = "role_user";
    public $timestamps = false;

    public function userClients()
    {
        return $this->hasMany(UserClient::class, 'user_id', 'user_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }
}
