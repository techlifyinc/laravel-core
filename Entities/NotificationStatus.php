<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationStatus extends Model
{
    use SoftDeletes;

    const PENDING = 1;
    const PROCESSED = 2;

    protected $fillable = [];

    protected $table = 'techlify_notification_statuses';

    /**
     * Apply filters on model.
     *
     * @param mixed $query   QueryBuilder
     * @param mixed $filters Filters
     *
     * @return void
     */
    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['search']) && "" != trim($filters['search'])) {
            $query->where('title', 'like', '%' . $filters['search'] . '%');
        }
    }
}
