<?php
namespace Modules\LaravelCore\Entities;


class LogAction
{

    const CREATED = "created";
    const UPDATED = "updated";
    const DELETED = "deleted";
    const VIEWED = "read";

}
