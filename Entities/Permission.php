<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;
use EloquentFilter\Filterable;

use Modules\LaravelCore\Entities\ModelFilters\PermissionFilter;
use Modules\LaravelCore\Entities\Role;
use Modules\LaravelCore\Entities\TenantSubscriptionScope;
use Modules\LaravelCore\Subscription\Entities\SubscriptionPackage;
use Modules\LaravelCore\Traits\AppDbConnectionTrait;

class Permission extends Model
{
    use AppDbConnectionTrait;
    use Filterable;

    public function modelFilter()
    {
        return $this->provideFilter(PermissionFilter::class);
    }
    protected $casts = [
        "is_app_permission" => 'boolean',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function subscriptionPackages()
    {
        return $this->belongsToMany(SubscriptionPackage::class, 'subscription_package_permissions', 'permission_id', 'package_id')
            ->whereNull('subscription_package_permissions.deleted_at');
    }

    public static function boot()
    {
        parent::boot();

        //Add Tenant Subscription scope
        static::addGlobalScope(new TenantSubscriptionScope);
    }
}
