<?php

namespace Modules\LaravelCore\Entities;

use Modules\LaravelCore\Traits\AppDbConnectionTrait;

class RolePermission extends TechlifyModel
{

    use AppDbConnectionTrait;
    protected $table = "permission_role";
}
