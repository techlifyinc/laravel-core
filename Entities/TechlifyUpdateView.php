<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;

class TechlifyUpdateView extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [];
}
