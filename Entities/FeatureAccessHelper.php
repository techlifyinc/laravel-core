<?php

namespace Modules\LaravelCore\Entities;

use Modules\LaravelCore\Entities\Feature;
use Exception;
use Illuminate\Http\Response;

class FeatureAccessHelper
{
    private static $featuresCache = null;
    private static $clientFeaturesCache = null;

    public static function hasAccess($code)
    {
        try {
            $isRunningUnitTestCase = config('app.env') == 'testing';
            //Unset cache if app is running in unit test mode
            if (self::$featuresCache == null || $isRunningUnitTestCase) {
                self::$featuresCache = Feature::get();
            }

            if (self::$clientFeaturesCache == null || $isRunningUnitTestCase) {
                self::$clientFeaturesCache = ClientFeature::get();
            }

            $feature = self::$featuresCache->where('code', $code)
                ->first();
            $clientFeature = self::$clientFeaturesCache->where('feature_id', $feature->id)
                ->first();

            if ($clientFeature) {
                return $clientFeature->is_enabled;
            }
            return $feature->is_enabled_default;
        } catch (Exception $error) {
            return response()->json([
                "message" => $error->getMessage(),
                'error' => $error,
                'location' => "FeatureAccess.hasAccess()",
                'code' => $code,
                'feature' => $feature,
                'clientFeature' => $feature->clientFeature
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}