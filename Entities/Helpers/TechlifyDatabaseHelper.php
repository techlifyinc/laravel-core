<?php

namespace Modules\LaravelCore\Entities\Helpers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

/**
 * Let's group various reusable helper functions
 * relating to DB operations
 */
class TechlifyDatabaseHelper
{
    public static function createIndexIfNonExistent($tableName, $column)
    {
        Schema::table($tableName, function (Blueprint $table) use ($tableName, $column) {
            $indexName = $column . '_index';
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $indexesFound = $sm->listTableIndexes($tableName);
            if (!array_key_exists($indexName, $indexesFound)) {
                $table->index($column, $indexName);
            }
        });
    }
    public static function dropIndexIfExistent($tableName, $column)
    {
        Schema::table($tableName, function (Blueprint $table) use ($tableName, $column) {
            $indexName = $column . '_index';
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $indexesFound = $sm->listTableIndexes($tableName);
            if (array_key_exists($indexName, $indexesFound)) {
                $table->dropIndex($indexName);
            }
        });
    }
}