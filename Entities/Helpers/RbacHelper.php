<?php

namespace Modules\LaravelCore\Entities\Helpers;

use Illuminate\Support\Facades\DB;
use Modules\LaravelCore\Entities\Permission;
use Modules\LaravelCore\Entities\Role;

/**
 * Let's have some components that just helps us with RBAC redundant code
 * 
 */
class RbacHelper
{
    /**
     * Assign permission to given roles.
     *
     * @param string $permissions Slugs of permissions to be added to roles
     * @param array $roles Slugs of roles that we add the permissions to
     * @return void
     */
    public static function addPermissionsToRoles(array $permissionSlugs = [], array $roleSlugs = []): void
    {
        $records = [];

        $roles = Role::whereIn('slug', $roleSlugs)
            ->get();

        $permissions = Permission::whereIn('slug', $permissionSlugs)
            ->get();

        foreach ($roles as $role) {
            foreach ($permissions as $permission) {
                $records[] = [
                    'permission_id' => $permission->id,
                    'role_id' => $role->id
                ];
            }
        }

        DB::table('permission_role')
            ->insert($records);
    }

    /**
     * Revoke permission from roles.
     *
     * @param string $permissionSlugs Slugs of permissions to be remove to roles
     * @param array $roleSlugs Slugs of roles that we remove the permissions from
     * @return void
     */
    public static function revokePermissionFromRoles(array $permissionSlugs, array $roleSlugs): void
    {
        /* Yes, I know we can delete by the slug... but let's load to ensure we're not emptying our DB */
        $roles = Role::whereIn('slug', $roleSlugs)
            ->get();

        $permissions = Permission::whereIn('slug', $permissionSlugs)
            ->get();

        foreach ($roles as $role) {
            $role->permissions()->detach($permissions);
        }
    }
}
