<?php

namespace Modules\LaravelCore\Entities\Helpers;

use Carbon\Carbon;
use Modules\LaravelCore\Subscription\Entities\SubscriptionPackagePermission;
use Modules\LaravelCore\Subscription\Entities\SubscriptionStatus;
use Modules\LaravelCore\Subscription\Entities\TenantSubscription;

class TenantSubscriptionHelper
{
    private static $activeSubscriptionsCache = null;
    private static $allowedPermissionIdsCache = null;

    public static function getActiveSubscriptions()
    {
        if (self::$activeSubscriptionsCache == null) {
            self::$activeSubscriptionsCache = TenantSubscription::where('subscription_tenant_subscriptions.status_id', SubscriptionStatus::ACTIVE)
                ->where('subscription_tenant_subscriptions.tenant_id', auth()->user()->client_id)
                ->where(function ($q) {
                    $q->whereRaw("'" . Carbon::now()->format('Y-m-d') . "' between `subscription_tenant_subscriptions`.`started_at` and `subscription_tenant_subscriptions`.`ended_at`")
                        ->orWhereRaw("'" . Carbon::now()->format('Y-m-d') . "' between `subscription_tenant_subscriptions`.`trail_started_at` and `subscription_tenant_subscriptions`.`trail_ended_at`")
                        ->orWhere(function ($q1) {
                            $q1->whereNull('subscription_tenant_subscriptions.ended_at')
                                ->whereDate('subscription_tenant_subscriptions.started_at', '<=', Carbon::now()->format('Y-m-d'));
                        });
                })
                ->get();
        }
        return self::$activeSubscriptionsCache;
    }

    public static function getAllowedPermissionIds()
    {
        if (self::$allowedPermissionIdsCache == null) {
            self::$allowedPermissionIdsCache = SubscriptionPackagePermission::whereRelation('package', 'subscription_packages.is_enabled', true)
                ->whereIn('subscription_package_permissions.package_id', self::getActiveSubscriptions()->pluck('package_id')->unique()->all())
                ->get('permission_id');
        }
        return self::$allowedPermissionIdsCache;
    }
}