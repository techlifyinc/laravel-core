<?php

namespace Modules\LaravelCore\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\LaravelCore\Entities\Role;
use Modules\LaravelCore\Entities\TechlifyModel;
use Modules\LaravelCore\Entities\UserType;


class NotificationSubscription extends TechlifyModel
{
    use SoftDeletes;
    use Appendable;

    protected $fillable = [];

    protected $table = 'techlify_notification_subscriptions';

    protected $casts = ['is_subscribed' => 'boolean'];

    protected $appends = ['subscription_types', 'subscriber_display_name'];
    /**
     * Get Subscriber Display Name attribute.
     * 
     * @return mixed
     */
    public function getSubscriberDisplayNameAttribute()
    {
        return $this->subscriber->name ?? $this->subscriber->label ?? $this->subscriber->title ?? '';
    }

    /**
     * Get Subscription Types attribute.
     *
     * @return void
     */
    public function getSubscriptionTypesAttribute()
    {
        $notificationTypes = $this->subscriptions()
            ->with('type')
            ->where('entity_id', $this->entity_id)
            ->get();
        if (count($notificationTypes) > 0) {
            return $notificationTypes->pluck('type.title')->values()->join(', ');
        }
        return '';
    }


    /**
     * Notification Subscription Entity
     *
     * @return mixed
     */
    public function entity()
    {
        return $this->belongsTo(
            NotificationSubscriptionEntity::class,
            'entity_id',
            'id'
        );
    }

    /**
     * Notification Type
     *
     * @return void
     */
    public function type()
    {
        return $this->belongsTo(NotificationType::class, 'type_id', 'id');
    }

    /**
     * Notification Subscriptions
     *
     * @return mixed
     */
    public function subscriptions()
    {
        return $this->hasMany(
            NotificationSubscription::class,
            'subscriber_id',
            'subscriber_id'
        );
    }

    /**
     * Notification Subscriber
     *
     * @return mixed
     */
    public function subscriber()
    {
        //Default consider subsciber as individual and switch based on the entity_id
        $relationClass = User::class;
        if ($this->entity_id == NotificationSubscriptionEntity::ROLE) {
            $relationClass = Role::class;
        }
        if ($this->entity_id == NotificationSubscriptionEntity::USER_TYPE) {
            $relationClass = UserType::class;
        }
        return $this->hasOne($relationClass, 'id', 'subscriber_id');
    }

    /**
     * Apply filter on model.
     *
     * @param mixed $query   QueryBuilder
     * @param mixed $filters Filters
     *
     * @return void
     */
    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['entity_ids']) && "" != trim($filters['entity_ids'])) {
            $query->whereIn('entity_id', json_decode($filters['entity_ids'], true));
        }

        if (isset($filters['type_ids']) && "" != trim($filters['type_ids'])) {
            $query->whereIn('type_id', json_decode($filters['type_ids'], true));
        }

        if (isset($filters['subscriber_id']) && "" != trim($filters['subscriber_id'])) {
            $query->where('subscriber_id', $filters['subscriber_id']);
        }

        if (isset($filters['creator_id']) && "" != trim($filters['creator_id'])) {
            $query->where('creator_id', $filters['creator_id']);
        }
    }
}
