<?php

namespace Modules\LaravelCore\Entities;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\LaravelCore\Http\Filters\TechlifyImportFilter;

class TechlifyImport extends TechlifyModel
{
    use Filterable;

    protected $table = "techlify_imports";

    protected $appends = [
        'no_of_records'
    ];

    /**
     * Get the filter class.
     *
     * @return string|null
     */
    public function modelFilter(): ?string
    {
        return $this->provideFilter(TechlifyImportFilter::class);
    }

    public function getNoOfRecordsAttribute(): int
    {
        return $this->records()->count();
    }

    /**
     * Get the import entities.
     *
     * @return HasMany
     */
    public function records(): HasMany
    {
        return $this->hasMany(TechlifyImportEntity::class, "import_id");
    }
}
