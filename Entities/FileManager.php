<?php

namespace Modules\LaravelCore\Entities;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class FileManager
{
    protected $fillable = [];

    /**
     * Upload a file to disk.
     *
     * @param $disk
     * @param $subPath
     * @param array $config
     * @return array
     */
    public static function upload($disk, $subPath, array $config = []): array
    {
        $rawFile = Request::file('fileData');

        // Get milliseconds from current time.
        $currentDateTime = Carbon::now();
        $milliseconds = $currentDateTime->timestamp * 1000 + $currentDateTime->millisecond;

        $fileName = $milliseconds . "_" . $rawFile->getClientOriginalName();

        $fileUrl = $subPath . '/' . $fileName;

        $file = '';

        if (array_key_exists('resize', $config) &&  $config['resize']) {
            $file = Image::make($rawFile); // resize =true
            $size = intval(config('laravelcore.image_resize', 1500));
            if ($file->width() > $size) {
                $file->widen($size);
            }
            if ($file->height() > $size) {
                $file->heighten($size);
            }
        }

        if (array_key_exists('watermark', $config) &&  $config['watermark']) {
            if (empty($file)) {
                $file = Image::make($rawFile);  //resize=false, watermark=true
            }
            $watermark_img = Image::make(config('laravelcore.watermark_path'));
            $watermark_img->widen(intval($file->width() / 2));
            $watermark_img->opacity(50);
            $file->insert($watermark_img, 'center', 10, 10);
            $format = pathinfo($fileName, PATHINFO_EXTENSION);
            $file->encode($format);
        }

        if (empty($file)) {
            $file = File::get($rawFile);  //resize=false, watermark=false
        }

        try {
            Storage::disk($disk)
                ->put($fileUrl, $file);
        } catch (Exception $e) {
            return ['error' => $e->getMessage(), 'trace' => $e->getTrace()];
        }

        $fileSize =  filesize($rawFile);
        return [
            'url' => Storage::disk($disk)->url($fileUrl),
            'file' => $fileUrl,
            'file_size' => $fileSize,
            'file_name' =>$rawFile->getClientOriginalName()
        ];
    }


    public static function delete($disk, $file)
    {
        try {
            $deleted = Storage::disk($disk)->delete($file);
            if (!$deleted) {
                return response()->json(['errors' => 'Request cannot be processed'], Response::HTTP_BAD_REQUEST);
            }
        } catch (Exception $e) {
            report($e);
            return response()->json(['errors' => 'Request cannot be processed'], Response::HTTP_BAD_REQUEST);
        }

        return $deleted ?: false;
    }
}
