<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\LaravelCore\Events\NoteCreatedEvent;

class Note extends TechlifyModel
{
    use SoftDeletes;

    use Uuid;

    protected $fillable = [];

    protected $table = "techlify_model_notes";

    // We'll dispatch events for the following actions
    protected $dispatchesEvents = [
        'created' => NoteCreatedEvent::class,
    ];

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['model_type']) && "" != trim($filters['model_type'])) {
            $query->where('model_type', $filters['model_type']);
        }

        if (isset($filters['related_model_id']) && "" != trim($filters['related_model_id'])) {
            $query->where('related_model_id', $filters['related_model_id']);
        }

        if (isset($filters['creator_id']) && is_numeric($filters['creator_id'])) {
            $query->where('creator_id', $filters['creator_id']);
        }

        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }
    }
}
