<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestrictorType extends Model
{
    use SoftDeletes;

    const TYPE_TIME_BASED = 1;
    const TYPE_IP_BASED = 2;

    protected $fillable = [];

    protected $table = 'techlify_rbac_restrictor_types';
}
