<?php

namespace Modules\LaravelCore\Entities;

use App\Models\User;
use Carbon\Carbon;

class ActivityLog extends TechlifyModel
{

    protected $table = "model_logging_activity_logs";
    protected $casts = [
        "object" => "array",
        "data"   => "array",
        "pre_object"   => "array"
    ];

    public function user()
    {
        return $this->belongsTo(User::class)
            ->withTrashed()
            ->select(['id', 'name', 'email']);
    }

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['object_id']) && !empty($filters['object_id'])) {
            $query->where('object_id', $filters['object_id']);
        }

        if (isset($filters['user_id']) && !empty($filters['user_id'])) {
            $query->where('user_id', $filters['user_id']);
        }

        if (isset($filters['action']) && !empty($filters['action'])) {
            $query->where('action', $filters['action']);
        }

        if (isset($filters['object_type']) && "" != trim($filters['object_type'])) {
            $query->where('object_type',  $filters['object_type']);
        }

        if (isset($filters['object_filter_type']) && "" != trim($filters['object_filter_type'])) {
            $query->where('object_type', 'like', '%' . $filters['object_filter_type'] . '%');
        }

        if (isset($filters['date_from']) && "" != $filters['date_from']) {
            $query->whereDate('created_at', '>=', Carbon::parse($filters['date_from'])->setTimezone(config('app.timezone', 'UTC')));
        }

        if (isset($filters['date_to']) && "" != $filters['date_to']) {
            $query->whereDate('created_at', '<=', Carbon::parse($filters['date_to'])->setTimezone(config('app.timezone', 'UTC')));
        }
    }
}
