<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Modules\LaravelCore\Events\DocumentCreatedEvent;
use Modules\LaravelCore\Http\Traits\AutoloadRelations;

class EntityFile extends TechlifyModel
{
    use SoftDeletes;
    use AutoloadRelations;
    use TaggableModel;

    protected $table = 'entity_files';

    public $subPath = "entity-files";
    public $disk = "techlify-inc";

    protected $fillable = [];

    protected $appends = ['url'];

    protected $casts = [
        "data" => "array"
    ];

    // We'll dispatch events for the following actions
    protected $dispatchesEvents = [
        'created' => DocumentCreatedEvent::class,
    ];

    public function setSubPath($newSubPath)
    {
        $this->subPath = $newSubPath;
        return $this;
    }

    public function setDisk($newDisk)
    {
        $this->disk = $newDisk;
        return $this;
    }

    public function getUrlAttribute()
    {
        if (!$this->file) {
            return '';
        }

        $disk = config('laravelcore.entity_file_disk', $this->disk);

        return Storage::disk($disk)
            ->url($this->file);
    }

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['title']) && "" != trim($filters['title'])) {
            $query->where("title", "LIKE", "%" . $filters['title'] . "%");
        }
        if (isset($filters['entity_type']) && "" != trim($filters['entity_type'])) {
            $query->where("entity_type", $filters['entity_type']);
        }
        if (isset($filters['entity_id']) && "" != trim($filters['entity_id'])) {
            $query->where("entity_id", $filters['entity_id']);
        }
        if (isset($filters['details']) && "" != trim($filters['details'])) {
            $query->where("details", "LIKE", "%" . $filters['details'] . "%");
        }
        if (isset($filters['tag_ids'])) {
            $query->whereHas('tags', function ($q) use ($filters) {
                $q->whereIn('techlify_tagger_tagged_models.tag_id', explode(',', $filters['tag_ids']));
            });
        }
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }
    }

    /**
     * Specify the whitelisted relations.
     *
     * @return string[]
     */
    protected function getAllowedRelations(): array
    {
        return [
            'creator',
            'tags',
            'tags.category',
        ];
    }
}
