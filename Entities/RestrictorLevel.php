<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestrictorLevel extends Model
{
    use SoftDeletes;

    const USER_TYPE = 1;
    const ROLE = 2;
    const INDIVIDUAL = 3;

    protected $fillable = [];

    protected $table = 'techlify_rbac_restrictor_levels';
}
