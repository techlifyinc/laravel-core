<?php

namespace Modules\LaravelCore\Entities;


class EmailSubscriptionUserHelper
{
    public static function getSubscriptionsByUser($userId)
    {
        return EmailSubscriptionUser::filter(['user_id' => $userId])->with(['user', 'emailType.frequency'])->get();
    }

    public static function getSubscriptionsByFrequency($frequencyId)
    {
        return EmailSubscriptionUser::filter(['frequency_id' => $frequencyId])->with(['user', 'emailType.frequency'])->get();
    }

    public static function hasSubscription($userId, $emailTypeId)
    {
        $count =  EmailSubscriptionUser::filter(['user_id' => $userId, 'email_type_id' => $emailTypeId])->count();
        return ($count > 0);
    }
}
