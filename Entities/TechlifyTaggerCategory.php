<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

class TechlifyTaggerCategory extends TechlifyModel
{
    use SoftDeletes;

    protected $fillable = [];

    protected $table = "techlify_tagger_categories";

    public function scopeFilter($query, $filters)
    {
        if (isset ($filters['search']) && "" != trim($filters['search'])) {
            $query->where('title', 'LIKE', '%' . $filters['search'] . '%');
        }

        if (isset ($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }
    }
}
