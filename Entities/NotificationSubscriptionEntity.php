<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationSubscriptionEntity extends Model
{
    use SoftDeletes;

    const INDIVIDUAL = 1;
    const ROLE = 2;
    const USER_TYPE = 3;

    protected $fillable = [];

    protected $table = 'techlify_notification_subscription_entities';

    /**
     * Apply filter on model.
     *
     * @param mixed $query   QueryBuilder
     * @param mixed $filters Filters
     * 
     * @return void
     */
    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['search']) && "" != trim($filters['search'])) {
            $query->where('title', 'like', '%' . $filters['search'] . '%');
        }
    }
}
