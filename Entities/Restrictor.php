<?php

namespace Modules\LaravelCore\Entities;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use Modules\LaravelCore\Http\Filters\RestrictorFilter;
use Modules\LaravelCore\Http\Traits\AutoloadRelations;

/**
 * An entity which allows users to access the app based on configuration.
 *
 * @property int $type_id
 * @property array $data
 * @property int $level_id
 * @property int $restricted_object_id
 * @property string $notes
 * @property int $creator_id
 * @property int $client_id
 * @property string $restrictable_type
 * @property int $restrictable_id
 */
class Restrictor extends TechlifyModel
{
    use SoftDeletes;
    use AutoloadRelations;
    use Filterable;

    protected $guarded = [];

    protected $table = 'techlify_rbac_restrictors';

    protected $casts = ['data' => 'array'];

    public function level()
    {
        return $this->belongsTo(RestrictorLevel::class, 'level_id', 'id');
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(RestrictorType::class, 'type_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(RestrictorStatus::class, 'status_id', 'id');
    }

    /**
     * Get the restricted object.
     *
     * @return MorphTo
     */
    public function restrictable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Get the allowed relations for autoload.
     *
     * @return string[]
     */
    protected function getAllowedRelations(): array
    {
        return ['type', 'level', 'status', 'restrictable'];
    }

    /**
     * Provide the filter class for restrictor.
     *
     * @return string|null
     */
    public function modelFilter(): ?string
    {
        return $this->provideFilter(RestrictorFilter::class);
    }
}
