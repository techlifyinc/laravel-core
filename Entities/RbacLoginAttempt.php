<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * A class to manage the login attempts.
 *
 * @property mixed $user_id
 * @property bool|mixed $is_successful
 * @property mixed $client_id
 */
class RbacLoginAttempt extends TechlifyModel
{
    use SoftDeletes;
    protected $casts = [
        'is_successful' => 'boolean',
    ];
}
