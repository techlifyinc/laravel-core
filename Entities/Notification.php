<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\LaravelCore\Entities\TechlifyModel;

class Notification extends TechlifyModel
{
    use SoftDeletes;

    protected $fillable = [];

    protected $table = 'techlify_notifications';

    protected $casts = ['constraints' => 'array'];

    /**
     * Notification Type
     *
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(NotificationType::class, 'type_id', 'id');
    }

    /**
     * Notification Status
     *
     * @return BelongsTo
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(NotificationStatus::class, 'status_id', 'id');
    }

    /**
     * Apply filters on model
     *
     * @param mixed $query   QueryBuilder
     * @param midex $filters Filters
     *
     * @return void
     */
    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['type_ids']) && "" != trim($filters['type_ids'])) {
            $query->whereIn('type_id', json_decode($filters['type_ids']));
        }

        if (isset($filters['status_ids']) && "" != trim($filters['status_ids'])) {
            $query->whereIn('status_id', json_decode($filters['status_ids']));
        }

        if (isset($filters['related_model_id']) && "" != trim($filters['related_model_id'])) {
            $query->where('related_model_id', $filters['related_model_id']);
        }

        if (isset($filters['related_model']) && "" != trim($filters['related_model'])) {
            $query->where('related_model', $filters['related_model']);
        }
    }
}
