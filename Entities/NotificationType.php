<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationType extends Model
{
    use SoftDeletes;

    protected $fillable = [];

    protected $table = 'techlify_notification_types';

    protected $casts = ['is_admin' => 'boolean'];

    /**
     * Current User Subscription
     *
     * @return mixed
     */
    public function currentUserSubscription()
    {
        return $this->hasOne(NotificationSubscription::class, 'type_id', 'id')
            ->where('entity_id', NotificationSubscriptionEntity::INDIVIDUAL)
            ->where('subscriber_id', auth()->id());
    }

    /**
     * Apply filter on model.
     *
     * @param mixed $query   QueryBuilder
     * @param mixed $filters Filters
     *
     * @return void
     */
    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['search']) && "" != trim($filters['search'])) {
            $query->where('title', 'like', '%' . $filters['search'] . '%');
        }

        if (isset($filters['is_admin']) && "" != trim($filters['is_admin'])) {
            $query->where(
                'is_admin',
                filter_var($filters['is_admin'], FILTER_VALIDATE_BOOLEAN)
            );
        }
    }

    /**
     * Get Notifiction Type
     *
     * @param mixed $code Code
     *
     * @return mixed
     */
    public static function getNotificationType($code)
    {
        return NotificationType::where('code', $code)->first();
    }
}
