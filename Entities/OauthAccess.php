<?php

namespace Modules\LaravelCore\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OauthAccess extends Model
{
    protected $table = 'oauth_access_tokens';
    protected $fillable = [];

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['date_from']) && "" != $filters['date_from']) {
            $query->whereDate('created_at', '>=', Carbon::parse($filters['date_from'])->setTimezone(config('app.timezone', 'UTC')));
        }

        if (isset($filters['date_to']) && "" != $filters['date_to']) {
            $query->whereDate('created_at', '<=', Carbon::parse($filters['date_to'])->setTimezone(config('app.timezone', 'UTC')));
        }

        if (isset($filters['user_id']) && "" != $filters['user_id']) {
            $query->where('user_id', $filters['user_id']);
        }
    }
}
