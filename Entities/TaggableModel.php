<?php

namespace Modules\LaravelCore\Entities;

use Modules\LaravelCore\Entities\TechlifyTaggerTag;
use Modules\LaravelCore\Entities\TechlifyTaggerTaggedModel;
use ReflectionClass;

/**
 * Description of LaravelRbac
 *
 * @author
 */
trait TaggableModel
{

    public function tags()
    {
        return $this->belongsToMany(TechlifyTaggerTag::class, (new TechlifyTaggerTaggedModel)->getTable(), 'model_id', 'tag_id', 'id', 'id')
            ->where((new TechlifyTaggerTaggedModel)->getTable() . '.model_type', (new ReflectionClass($this))->getShortName());
    }

    public function addTags(...$tagIds)
    {
        $tagIds = collect($tagIds)->flatten()->all();
        $syncModel = [];
        foreach ($tagIds as $tagId) {
            $syncModel[$tagId] = [
                'model_type' => (new ReflectionClass($this))->getShortName(),
                'creator_id' => auth()->id(),
                'client_id' => auth()->user()->client_id,
            ];
        }
        return $this->tags()->sync($syncModel);
    }

    public function removeAllTags()
    {
        return $this->tags()->detach();
    }

    public function removeTag($tagId)
    {
        return $this->tags()->detach($tagId);
    }
}
