<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class RbacClientConfigService extends Model
{
    /**
     * Get the access control config for given $clientId or get the default configuration.
     *
     * @param null $clientId
     * @return RbacClientConfiguration|null
     */
    public static function fetchCurrentClientConfig($clientId = null): RbacClientConfiguration|null
    {
        if (!$clientId) {
            $clientId = auth()?->user()?->client_id;
        }
        // If $clientId is null, then it will return the default configuration.
        return RbacClientConfiguration::where(function ($q) use ($clientId) {
            $q->where('client_id', $clientId)
                ->orWhereNull('client_id');
        })->withoutGlobalScope('TechlifyScope')
            ->first();
    }
}
