<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

class TechlifyTaggerTaggedModel extends TechlifyModel
{
    use SoftDeletes;

    protected $fillable = [];

    protected $table = "techlify_tagger_tagged_models";

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }
    }
}
