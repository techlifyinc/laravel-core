<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RbacUserPasswordHistory extends Model
{
    protected $fillable = [];
    use SoftDeletes;
}
