<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\LaravelCore\Traits\AuthDbConnectionTrait;

class UserClient extends Model
{
    use AuthDbConnectionTrait;
    protected $table = 'user_clients';
    protected $guarded = [];

    protected $with = ['client'];

    protected $casts = ['is_enabled' => 'boolean'];

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    /**
     * Get the user type.
     *
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(UserType::class, 'user_type_id');
    }
}
