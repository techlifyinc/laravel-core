<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestrictorStatus extends Model
{
    use SoftDeletes;

    const ACTIVE = 1;
    const DISABLED = 2;

    protected $fillable = [];

    protected $table = 'techlify_rbac_restrictor_statuses';
}
