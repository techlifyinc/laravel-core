<?php

namespace Modules\LaravelCore\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailSubscriptionUser extends TechlifyModel
{
    use LoggableModel;
    use SoftDeletes;

    protected $fillable = [];

    protected $table = 'email_subscription_users';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function emailType()
    {
        return $this->belongsTo(EmailSubscriptionType::class, 'email_type_id', 'id');
    }

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['user_id']) && "" != trim($filters['user_id'])) {
            $query->where('user_id', $filters['user_id']);
        }

        if (isset($filters['email_type_id']) && "" != trim($filters['email_type_id'])) {
            $query->where('email_type_id', $filters['email_type_id']);
        }

        if (isset($filters['creator_id']) && "" != trim($filters['creator_id'])) {
            $query->where('creator_id', $filters['creator_id']);
        }
    }
}
