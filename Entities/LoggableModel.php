<?php

namespace Modules\LaravelCore\Entities;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use ReflectionClass;
use Modules\LaravelCore\Entities\LoggerGuy;

/**
 * Trait that does logging for different model operations
 *
 * @author 
 * @since 
 */
trait LoggableModel
{

    protected $logCreated = true;

    protected $logUpdated = true;

    protected $logDeleted = true;


    public static function bootLoggableModel()
    {
        if (!auth()->check() || app()->runningInConsole()) {
            return;
        }

        static::created(function (Model $model) {
            if (!$model->logCreated) {
                return;
            }
            try {
                $reflect = new ReflectionClass($model);

                $model->setRelations([]);
                $model->setHidden($model->appends);

                LoggerGuy::logInserted(
                    $reflect->getShortName(),
                    $model[$model->getKeyName()],
                    $model,
                    ['reference' => $model[$model->logReference] ?? '', 'changes' => $model->getChanges()]
                );
            } catch (Exception $e) {
                Log::error($e);
            }
        });


        static::updated(function (Model $model) {
            if (!$model->logUpdated) {
                return;
            }
            try {
                $reflect = new ReflectionClass($model);
                if (method_exists(Model::class, 'getRawOriginal')) {
                    // Laravel >7.0
                    $pre_object = (new static)->setRawAttributes($model->getRawOriginal());
                } else {
                    // Laravel <7.0
                    $pre_object = (new static)->setRawAttributes($model->getOriginal());
                }

                $model->setRelations([]);
                $model->setHidden($model->appends);
                $pre_object->setRelations([]);
                $pre_object->setHidden($model->appends);

                LoggerGuy::logUpdated(
                    $reflect->getShortName(),
                    $model[$model->getKeyName()],
                    $model,
                    ['reference' => $model[$model->logReference] ?? '', 'changes' => $model->getChanges()],
                    $pre_object
                );
            } catch (Exception $e) {
                Log::error($e);
            }
        });

        static::deleted(function (Model $model) {
            if (!$model->logDeleted) {
                return;
            }
            try {
                $reflect = new ReflectionClass($model);
                if (method_exists(Model::class, 'getRawOriginal')) {
                    // Laravel >7.0
                    $pre_object = (new static)->setRawAttributes($model->getRawOriginal());
                } else {
                    // Laravel <7.0
                    $pre_object = (new static)->setRawAttributes($model->getOriginal());
                }

                $model->setRelations([]);
                $model->setHidden($model->appends);
                $pre_object->setRelations([]);
                $pre_object->setHidden($model->appends);

                LoggerGuy::logDeleted(
                    $reflect->getShortName(),
                    $model[$model->getKeyName()],
                    $model,
                    ['reference' => $model[$model->logReference] ?? '', 'changes' => $model->getChanges()],
                    $pre_object
                );
            } catch (Exception $e) {
                Log::error($e);
            }
        });
    }
}
