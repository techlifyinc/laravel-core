<?php

namespace Modules\LaravelCore\Entities\ModelFilters;

use Modules\LaravelCore\Entities\UserType;
use Modules\LaravelCore\Http\Filters\BaseFilter;

class PermissionFilter extends BaseFilter
{

    public function setup()
    {
        if (auth()->user()->user_type_id != UserType::APP_ADMIN) {
            $this->where("is_app_permission", false);
        }
        parent::setup();
    }

    public function search($search)
    {
        $this->where(function ($q) use ($search) {
            $q->where('label', 'like', "%$search%")
                ->orWhere('slug', 'like', "%$search%")
                ->orWhere('description', 'like', "%$search%");
        });
    }

    public function notInPackageIds($notInPackageIds)
    {
        $this->whereDoesntHave('subscriptionPackages', function ($query) use ($notInPackageIds) {
            $query->whereIn('subscription_packages.id', explode(',', $notInPackageIds));
        });
    }
}