<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int max_failed_consecutive_login_attempts
 * @property int $min_password_length
 * @property bool $is_password_require_number
 * @property bool $is_password_require_symbol
 * @property bool $is_password_require_capital_letter
 * @property bool $is_track_old_passwords
 * @property int $max_old_passwords_block
 */
class RbacClientConfiguration extends TechlifyModel
{
    use SoftDeletes;
    use LoggableModel;

    protected $casts = [
        "is_track_old_passwords" => "boolean",
        "is_password_require_number" => "boolean",
        "is_password_require_symbol" => "boolean",
        "is_password_require_capital_letter" => "boolean",
        "max_failed_consecutive_login_attempts" => "int",
    ];
}
