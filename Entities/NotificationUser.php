<?php

namespace Modules\LaravelCore\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotificationUser extends Model
{
    use SoftDeletes;

    protected $fillable = [];

    protected $table = 'techlify_notification_users';

    /**
     * Notificaiton.
     *
     * @return mixed
     */
    public function notification()
    {
        return $this->belongsTo(Notification::class, 'notification_id', 'id');
    }

    /**
     * Notitication User Status
     *
     * @return mixed
     */
    public function status()
    {
        return $this->belongsTo(NotificationUserStatus::class, 'status_id', 'id');
    }

    /**
     * Notifcation User.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Apply filter on model.
     *
     * @param mixed $query   QueryBuilder
     * @param mixed $filters Filters
     *
     * @return mixed
     */
    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['status_ids']) && "" != trim($filters['status_ids'])) {
            $query->whereIn('status_id', json_decode($filters['status_ids']));
        }

        if (isset($filters['notification_id'])
            && "" != trim($filters['notification_id'])
        ) {
            $query->where('notification_id', $filters['notification_id']);
        }

        if (isset($filters['user_id']) && "" != trim($filters['user_id'])) {
            $query->where('user_id', $filters['user_id']);
        }
    }
}
