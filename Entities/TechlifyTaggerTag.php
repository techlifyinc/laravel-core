<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

class TechlifyTaggerTag extends TechlifyModel
{
    use SoftDeletes;

    protected $fillable = [];

    protected $table = "techlify_tagger_tags";

    public function category()
    {
        return $this->hasOne(TechlifyTaggerCategory::class, 'id', 'category_id');
    }

    public function taggedModels()
    {
        return $this->hasMany(TechlifyTaggerTaggedModel::class, 'tag_id', 'id');
    }

    public function scopeFilter($query, $filters)
    {
        if (isset ($filters['search']) && "" != trim($filters['search'])) {
            $query->where('title', 'LIKE', '%' . $filters['search'] . '%');
        }

        if (isset ($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset ($filters['categories']) && "" != ($filters['categories'])) {
            $query->whereHas('category', function ($q1) use ($filters) {
                $q1->whereIn('title', explode(',', $filters['categories']));
            });
        }

        if (isset ($filters['model_type']) && "" != ($filters['model_type'])) {
            $query->whereHas('taggedModels', function ($q1) use ($filters) {
                $q1->where('model_type', $filters['model_type']);
            });
        }

        if (isset ($filters['model_id']) && "" != ($filters['model_id'])) {
            $query->whereHas('taggedModels', function ($q1) use ($filters) {
                $q1->where('model_id', $filters['model_id']);
            });
        }
    }
}
