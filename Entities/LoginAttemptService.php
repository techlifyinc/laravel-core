<?php

namespace Modules\LaravelCore\Entities;

use Illuminate\Database\Eloquent\Model;

class LoginAttemptService extends Model
{
    /**
     * Log the user login attempt.
     *
     * @param TechlifyUser $user
     * @param bool $isSuccessfulAttempt
     * @return void
     */
    public static function logUserAttempt(TechlifyUser $user, bool $isSuccessfulAttempt): void
    {
        if (!$user->id || !$user->client_id) {
            return;
        }
        // Create a new login attempt record.
        $attempt = new RbacLoginAttempt();
        $attempt->user_id = $user->id;
        $attempt->is_successful = $isSuccessfulAttempt;
        $attempt->client_id = $user->client_id;
        $attempt->save();

        // If it's a failed attempt, then let's check the number of login attempts remaining.
        if (!$isSuccessfulAttempt) {
            if (self::numberOfLoginAttemptsRemaining($user) == 0) {
                // disable the user
                $user->disableUser();
            }
        }
    }

    /**
     * Get the number of login attempts remaining.
     * No of failed login attempts will be counted after the last user enabled timestamp.
     *
     * @param TechlifyUser $user
     * @return int
     */
    public static function numberOfLoginAttemptsRemaining(TechlifyUser $user): int
    {
        $config = RbacClientConfigService::fetchCurrentClientConfig($user->client_id);

        // Get the allowed maximum login failed attempts from config.
        $allowedMaxFailedLoginAttempts = $config->max_failed_consecutive_login_attempts;

        $failedAttempts = RbacLoginAttempt::where('client_id', $user->client_id)
            ->where('user_id', $user->id)
            ->where('is_successful', false)
            ->orderBy('created_at', 'desc');
        // Find the user client record to get the last enabled_on timestamp
        $userClient = UserClient::query()
            ->where('user_id', $user->id)
            ->where('client_id', $user->client_id)
            ->whereHas('client', function ($q) {
                $q->where('status_id', ClientStatus::ACTIVE);
            })
            ->first();


        if ($userClient && $userClient->enabled_on) {
            $failedAttempts = $failedAttempts->where('created_at', '>', $userClient->enabled_on);
        }

        $lastSuccessfulAttempt = RbacLoginAttempt::where('user_id', $user->id)
            ->where('client_id', $user->client_id)
            ->where('is_successful', true)
            ->orderBy('created_at', 'desc')
            ->first();
        if ($lastSuccessfulAttempt) {
            $failedAttempts = $failedAttempts->where('id', '>', $lastSuccessfulAttempt->id);
        }

        // If failed attempts to exceed the max allowed attempts then disable the user.
        $failedAttempts = $failedAttempts->count();

        return max($allowedMaxFailedLoginAttempts - $failedAttempts, 0);
    }
}
