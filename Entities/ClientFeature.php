<?php

namespace Modules\LaravelCore\Entities;


class ClientFeature extends TechlifyModel
{
    protected $fillable = [];

    protected $table = 'techlify_fm_client_features';

    protected $cast = [
        'is_enabled' => 'boolean'
    ];

    public function feature()
    {
        return $this->hasOne(Feature::class, 'id', 'feature_id');
    }

    public function scopeFilter($query, $filters)
    {
        $query->where('client_id', auth()->user()->client_id);
    }
}
