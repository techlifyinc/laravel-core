<?php

namespace Modules\LaravelCore\Facades;

use Illuminate\Support\Facades\DB;

class DBO extends DB
{
    /**
     * Extend the default DB class with options for string casting
     * @param mixed $query Query to be parsed raw
     * @param boolean $toString Whether to convert the query expression to string.
     * $toString is defaulted to false, for supporting backward compatibility.
     */
    public static function raw($query, $toString = false)
    {
        if (!$toString) {
            return parent::raw($query);
        }
        return parent::raw($query)->getValue(parent::getQueryGrammar());
    }
}