# LaravelCore

This is the core module used by Techlify for projects. It contains the basis for 
- user management
- module management
- client management

## Installation

Install this package with composer using the following command:

```
composer require techlify-inc/laravel-core
```

Run migrations

```
$ php artisan migrate
```

Add the Rbac trait to your `User` model

```php

class User extends Authenticatable
{
    use TechlifyInc\LaravelRbac\Traits\LaravelRbac;
}
```

## Usage

### Roles

#### Creating roles

```php
use \TechlifyInc\LaravelRbac\Models\Role;

$adminRole = Role::create([
    'name' => 'Administrator',
    'slug' => 'admin'
]);

$managerRole = Role::create([
    'name' => 'Manager',
    'slug' => 'manager'
]);
```

#### Assigning And Removing Roles
	
You can simple attach role to user:
```php
use App\Models\User;

$user = User::find(1);
$user->attachRole($adminRole);
//or you can attach using the role slug
$user->attachRole("admin");
```
And the same if you want to detach role:
```php
$user->detachRole($adminRole);
//or you can remove using the role slug
$user->detachRole("admin");
```
### Checking for roles

You can simple check if user has role:
```php
use App\Models\User;

$user = User::find(1);
if ($user->hasRole('admin')) {
    
}
```

### Permissions

#### Creating permissions

```php
use \TechlifyInc\LaravelRbac\Models\Permission;

$createPermission = Permission::create([
    'name' => 'Create product',
    'slug' => 'product.create'
]);

$removePermission = Permission::create([
    'name' => 'Delete product',
    'slug' => 'product.remove'
]);
```

#### Attaching And Detaching permissions

You can attach permission to role very simple:
```php
use \TechlifyInc\LaravelRbac\Models\Role;

$adminRole = Role::find(1);
$adminRole->attachPermission($createPermission);
//or you can insert only slug
$adminRole->attachPermission("product.create");
```
And the same to detach permission:
```php
$adminRole->detachPermission($createPermission);
$adminRole->detachPermission("product.create");
```

### Checking for permissions

You can simple check if user has permission:
```php
use App\Models\User;

$user = User::find(1);
if ($user->hasPermission('product.create')) {
    
}

// OR for currently logged in user
if (auth()->user()->hasPermission('product.create'))
```

You can also enforce access control at route level using the middleware; this is enforced in several ways. 

1. Using permissions: 

```php
Route::get("customers", "CustomerController@index")
    ->middleware("TechlifyAccessControl:customer_view");

OR

Route::get("customers", "CustomerController@index")
    ->middleware("TechlifyAccessControl:ps:customer_view");
```

2. Using User Type: 

```php
Route::get("customers", "CustomerController@index")
    ->middleware("TechlifyAccessControl:ut:admin");
```

3. Using state to restrict access: 

```php
Route::get("customers", "CustomerController@index")
    ->middleware("TechlifyAccessControl:state:logged-in");
```

### User Management

```php

// Get the set of users
GET api/users

// Get a single user
GET api/users/{id}

// Add a new user
POST api/users

// Update a user record
PATCH api/users/{id}

// Delete a user record
DELETE api/users/{id}

```


### User Password Management

```php

// Change the current user password
POST api/user/current/update-password {current_password, new_password}

```

### User Session Management

```php

// Log out the currently logged in user
POST api/user/logout

// Get the User record of the currently logged in user
GET api/user/current

```

### Entity File Management
There is functionality built into the application providing a dynamic file manager. Entity Files is what it's called. 

Configurations

```php
// add this variable to your .env file to supply the maximum 
ENTITY_FILE_MAX_UPLOAD_FILE_SIZE=20480 // 20MB
ENTITY_FILE_DISK=....   // Which Disk
ENTITY_FILE_SUB_PATH=....   // Which Disk
```

The module works on it's own via the built in `<entity-files entityType="'Sale'" entityId="sale.id"></entity-files>` angular component in the `ngx-techlify-core` package. 