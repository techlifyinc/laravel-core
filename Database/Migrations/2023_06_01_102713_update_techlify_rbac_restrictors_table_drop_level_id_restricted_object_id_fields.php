<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTechlifyRbacRestrictorsTableDropLevelIdRestrictedObjectIdFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('techlify_rbac_restrictors', function (Blueprint $table) {
            $table->dropColumn(['restricted_object_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('techlify_rbac_restrictors', function (Blueprint $table) {
            $table->unsignedBigInteger('restricted_object_id')
                ->nullable();
        });
    }
}
