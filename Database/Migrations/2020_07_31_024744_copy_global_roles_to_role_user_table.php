<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\LaravelCore\Entities\Role;
use Modules\LaravelCore\Entities\RoleUser;

class CopyGlobalRolesToRoleUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_user', function (Blueprint $table) {
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $indexesFound = $sm->listTableIndexes('role_user');
            if (array_key_exists("role_user_user_id_foreign", $indexesFound)) {
                $table->dropForeign('role_user_user_id_foreign');
                $table->dropIndex('role_user_user_id_foreign');
                $table->dropForeign('role_user_role_id_foreign');
            }
            if (array_key_exists("primary", $indexesFound) && !Schema::hasColumn('role_user', 'id')) {
                $table->dropPrimary();
            }
        });

        if (!Schema::hasColumn('role_user', 'id')) {
            Schema::table('role_user', function (Blueprint $table) {
                $table->bigIncrements('id')
                    ->first();
            });
        }

        Schema::table('role_user', function (Blueprint $table) {
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $indexesFound = $sm->listTableIndexes('role_user');
            if (!array_key_exists("role_user_user_id_foreign", $indexesFound)) {
                $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            }
            if (!array_key_exists("role_user_role_id_foreign", $indexesFound)) {
                $table->foreign('role_id')
                    ->references('id')
                    ->on('roles')
                    ->onDelete('cascade');
            }
        });

        $roles = Role::get();

        foreach ($roles as $role) {
            if (!$role->is_global && $role->client_id) {
                $role->roleUsers()->update(['client_id' => $role['client_id']]);
            } else {
                $roleUsers = $role->roleUsers;
                $roleUsersArray = [];
                foreach ($roleUsers as $roleUser) {
                    $userClients = $roleUser->userClients;
                    foreach ($userClients as $userClient) {
                        array_push($roleUsersArray, new RoleUser([
                            'role_id' => $role->id,
                            'user_id' => $roleUser->user_id,
                            'client_id' => $userClient->client_id,
                        ]));
                    }
                }
                $role->roleUsers()->saveMany($roleUsersArray);
            }
        }
        RoleUser::whereNull('client_id')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        RoleUser::whereNotNull('client_id')->update(['client_id' => null]);
    }
}