<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTinFieldsToClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('clients', 'tin')) {
            return;
        }
        Schema::table('clients', function (Blueprint $table) {
            $table->string('tin')
                ->nullable()
                ->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('clients', 'tin')) {
            Schema::table('clients', function (Blueprint $table) {
                $table->dropColumn('tin');
            });
        }
    }
}
