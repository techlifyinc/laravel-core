<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTechlifyRbacRestrictorLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('techlify_rbac_restrictor_levels', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        $defaults = [
            ['id' => 1, 'title' => 'User Type', 'description' => 'RestrictorRequest applied to user type'],
            ['id' => 2, 'title' => 'Role', 'description' => 'RestrictorRequest applied to role'],
            ['id' => 3, 'title' => 'Individual', 'description' => 'RestrictorRequest applied to individual user'],
        ];

        DB::table('techlify_rbac_restrictor_levels')->insert($defaults);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('techlify_rbac_restrictor_levels');
    }
}