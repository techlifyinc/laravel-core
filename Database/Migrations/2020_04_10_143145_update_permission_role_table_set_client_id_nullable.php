<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UpdatePermissionRoleTableSetClientIdNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('permission_role', 'client_id')) {
            Schema::table('permission_role', function ($table) {
                $table->bigInteger('client_id')->unsigned()->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('permission_role', 'client_id')) {
            Schema::table('permission_role', function ($table) {
                $table->bigInteger('client_id')->unsigned()->nullable(false)->change();
            });
        }
    }
}
