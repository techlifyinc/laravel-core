<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTechlifyNotificationUserStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('techlify_notification_user_statuses')) {

            Schema::create(
                'techlify_notification_user_statuses',
                function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('title');
                    $table->text('description')->nullable()->default(null);
                    $table->timestamps();
                    $table->softDeletes();
                }
            );

            $defaults = [
                ['id' => 1, 'title' => 'Delivered', 'description' => 'Delivered'],
                ['id' => 2, 'title' => 'Read', 'description' => 'Read'],
            ];

            DB::table('techlify_notification_user_statuses')->insert($defaults);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('techlify_notification_user_statuses');
    }
}