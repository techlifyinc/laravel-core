<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        // Update the data accordingly
        DB::table('user_clients')->update([
            'is_enabled' => DB::raw('CASE WHEN is_enabled = 0 THEN 1 ELSE 0 END'),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('user_clients')->update([
            'is_enabled' => DB::raw('CASE WHEN is_enabled = 0 THEN 1 ELSE 0 END'),
        ]);
    }
};
