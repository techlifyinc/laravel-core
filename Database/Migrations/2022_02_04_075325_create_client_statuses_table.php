<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateClientStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('client_statuses')) {
            return;
        }

        Schema::create('client_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->timestamps();
        });

        $statuses = [
            [
                'title' => 'Active',
                'description' => '',
            ],
            [
                'title' => 'Pending Review',
                'description' => '',
            ],
            [
                'title' => 'Disabled',
                'description' => '',
            ],
        ];
        DB::table('client_statuses')
            ->insert($statuses);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_statuses');
    }
}