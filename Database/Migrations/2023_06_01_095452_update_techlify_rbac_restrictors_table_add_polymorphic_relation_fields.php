<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTechlifyRbacRestrictorsTableAddPolymorphicRelationFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('techlify_rbac_restrictors', function (Blueprint $table) {
            $table->morphs('restrictable', 'rbac_restrictor_restrictable_type_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('techlify_rbac_restrictors', function (Blueprint $table) {
            $table->dropMorphs('restrictable');
        });
    }
}
