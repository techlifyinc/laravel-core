<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeModelIdToNullableInTechlifyTagTaggedModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('techlify_tagger_tagged_models', function (Blueprint $table) {
            $table->unsignedBigInteger('model_id')->nullable()->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('techlify_tagger_tagged_models', function (Blueprint $table) {
            $table->unsignedBigInteger('model_id')->nullable(false)->change();
        });
    }
}
