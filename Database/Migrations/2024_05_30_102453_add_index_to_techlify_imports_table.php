<?php

use Illuminate\Database\Migrations\Migration;
use Modules\LaravelCore\Entities\Helpers\TechlifyDatabaseHelper;

class AddIndexToTechlifyImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        TechlifyDatabaseHelper::createIndexIfNonExistent('techlify_import_entities', 'import_id');
        TechlifyDatabaseHelper::createIndexIfNonExistent('techlify_imports', 'client_id');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        TechlifyDatabaseHelper::dropIndexIfExistent('techlify_import_entities', 'import_id');
        TechlifyDatabaseHelper::dropIndexIfExistent('techlify_imports', 'client_id');
    }
}
