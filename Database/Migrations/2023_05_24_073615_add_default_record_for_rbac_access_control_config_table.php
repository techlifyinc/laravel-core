<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultRecordForRbacAccessControlConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $defaultConfig = DB::table('rbac_client_configurations')
            ->whereNull('client_id')
            ->first();
        // add new record if not exist
        if (!$defaultConfig) {
            DB::table('rbac_client_configurations')
                ->insert([
                    'max_failed_consecutive_login_attempts' => 5,
                    'is_track_old_passwords' => false,
                    'max_old_passwords_block' => false,
                    'min_password_length' => 8,
                    'is_password_require_number' => true,
                    'is_password_require_symbol' => true,
                    'is_password_require_capital_letter' => true,
                ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // no need to delete the default records.
    }
}