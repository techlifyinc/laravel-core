<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPackagePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('subscription_package_permissions')) {
            Schema::create(
                'subscription_package_permissions',
                function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->foreignId('package_id')->index('package_id_index');
                    $table->foreignId('permission_id')->index('permission_id_index');
                    $table->text('details')->nullable();
                    $table->unsignedBigInteger('creator_id');
                    $table->timestamps();
                    $table->softDeletes();
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_package_permissions');
    }
}
