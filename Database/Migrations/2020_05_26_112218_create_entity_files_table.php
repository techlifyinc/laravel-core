<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntityFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('entity_files')) {
            return;
        }

        Schema::create('entity_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('file');
            $table->string('entity_type');
            $table->string('entity_id');
            $table->text('details');
            $table->text('data')
                ->comment('this will be an array casted to text, here we can store any data related to the file');
            $table->unsignedBigInteger('creator_id');
            $table->unsignedBigInteger('client_id')
                ->nullable()
                ->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_files');
    }
}
