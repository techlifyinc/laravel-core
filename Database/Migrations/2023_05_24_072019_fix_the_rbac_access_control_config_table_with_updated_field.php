<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTheRbacAccessControlConfigTableWithUpdatedField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('rbac_client_configurations', function (Blueprint $table) {
            // make client_id nullable
            $table->unsignedBigInteger('client_id')
                ->unique()
                ->nullable()
                ->change();
            // make the is_track_old_passwords password default to false
            $table->boolean('is_track_old_passwords')
                ->default(false)
                ->change();
            // make the max_old_passwords_block field default to false
            $table->boolean('max_old_passwords_block')
                ->default(false)
                ->change();
            // make the min_password_length password to nullable
            $table->boolean('min_password_length')
                ->nullable()
                ->change();
            // make the is_password_require_number field default to true
            $table->boolean('is_password_require_number')
                ->default(true)
                ->change();
            // make the is_password_require_symbol field default to true
            $table->boolean('is_password_require_symbol')
                ->default(true)
                ->change();
            // make the is_password_require_capital_letter field default to true
            $table->boolean('is_password_require_capital_letter')
                ->default(true)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        // Rollback the default values
        Schema::table('rbac_client_configurations', function (Blueprint $table) {
            // drop the unique foreign
            $table->dropForeign('rbac_client_configurations_client_id_unique');

            $table->boolean('is_track_old_passwords')
                ->change();
            $table->boolean('max_old_passwords_block')
                ->change();
            $table->boolean('is_password_require_number')
                ->change();
            $table->boolean('is_password_require_symbol')
                ->change();
            $table->boolean('is_password_require_capital_letter')
                ->change();
        });
    }
}
