<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionNSoftdeletesToTechlifyFmFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('techlify_fm_features', 'description') || Schema::hasColumn('techlify_fm_features', 'deleted_at')) {
            return;
        }
        Schema::table('techlify_fm_features', function (Blueprint $table) {
            $table->text('description')->after('title');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('techlify_fm_features', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('deleted_at');
        });
    }
}
