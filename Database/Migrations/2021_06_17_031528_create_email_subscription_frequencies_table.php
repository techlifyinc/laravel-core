<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateEmailSubscriptionFrequenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_subscription_frequencies', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('code');
            $table->timestamps();
            $table->softDeletes();
        });

        $defaults = [
            ['id' => 1, 'title' => 'Daily', 'code' => 'daily'],
            ['id' => 2, 'title' => 'Bi-Weekly', 'code' => 'bi-weekly'],
            ['id' => 3, 'title' => 'Weekly', 'code' => 'weekly'],
            ['id' => 4, 'title' => 'Fortnightly', 'code' => 'fortnightly'],
            ['id' => 5, 'title' => 'Monthly', 'code' => 'monthly'],
            ['id' => 6, 'title' => 'Quarterly', 'code' => 'quarterly'],
            ['id' => 7, 'title' => 'Half-Yearly', 'code' => 'half-Yearly'],
            ['id' => 8, 'title' => 'Yearly', 'code' => 'yearly'],
        ];

        DB::table('email_subscription_frequencies')->insert($defaults);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_subscription_frequencies');
    }
}