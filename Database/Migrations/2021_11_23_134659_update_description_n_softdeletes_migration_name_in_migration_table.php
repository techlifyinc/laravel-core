<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateDescriptionNSoftdeletesMigrationNameInMigrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('migrations')->where('migration', '=', '2021_11_08_142109_update_techlify_fm_features')
            ->update(["migration" => '2021_11_08_142109_add_description_n_softdeletes_to_techlify_fm_features']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('migrations')->where('migration', '=', '2021_11_08_142109_add_description_n_softdeletes_to_techlify_fm_features')
            ->update(["migration" => '2021_11_08_142109_update_techlify_fm_features']);
    }
}