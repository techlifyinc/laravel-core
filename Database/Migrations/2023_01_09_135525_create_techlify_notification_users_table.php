<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechlifyNotificationUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('techlify_notification_users')) {
            Schema::create(
                'techlify_notification_users',
                function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->unsignedBigInteger('user_id');
                    $table->unsignedBigInteger('notification_id');
                    $table->unsignedBigInteger('status_id');
                    $table->timestamps();
                    $table->softDeletes();
        
                    $table->unique(
                        ['user_id', 'notification_id', 'status_id'],
                        'user_notification_status_unique'
                    );
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('techlify_notification_users');
    }
}
