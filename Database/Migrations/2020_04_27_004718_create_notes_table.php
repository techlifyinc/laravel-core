<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('techlify_model_notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('note');
            $table->string('related_model_id');
            $table->string('model_type');
            $table->unsignedBigInteger('creator_id');
            $table->unsignedBigInteger('client_id')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('techlify_model_notes');
    }
}
