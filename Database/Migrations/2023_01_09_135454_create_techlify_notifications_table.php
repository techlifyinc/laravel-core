<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechlifyNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('techlify_notifications')) {            
            Schema::create(
                'techlify_notifications',
                function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('title');
                    $table->text('body')
                        ->nullable()
                        ->default(null);
                    $table->unsignedBigInteger('type_id');
                    $table->unsignedBigInteger('status_id');
                    $table->unsignedBigInteger('related_model_id')
                        ->nullable()
                        ->default(null);
                    $table->string('related_model')
                        ->nullable()
                        ->default(null);
                    $table->text('constraints')
                        ->nullable()
                        ->default(null);
                    $table->unsignedBigInteger('creator_id');
                    $table->unsignedBigInteger('client_id');
                    $table->timestamps();
                    $table->softDeletes();
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('techlify_notifications');
    }
}
