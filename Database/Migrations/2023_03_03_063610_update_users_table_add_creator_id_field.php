<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration class to add the creator_id field in users table.
 */
class UpdateUsersTableAddCreatorIdField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Add creator_id to users table
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('creator_id')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Remove creator_id from users table
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('creator_id');
        });
    }
}
