<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechlifyNotificationSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('techlify_notification_subscriptions')) {
            Schema::create(
                'techlify_notification_subscriptions',
                function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->unsignedBigInteger('type_id');
                    $table->unsignedBigInteger('subscriber_id');
                    $table->unsignedBigInteger('entity_id');
                    $table->boolean('is_subscribed')->default(true);
                    $table->unsignedBigInteger('creator_id');
                    $table->unsignedBigInteger('client_id');
                    $table->timestamps();
                    $table->softDeletes();
        
                    $table->unique(
                        ['type_id', 'entity_id', 'subscriber_id'],
                        'type_entity_subscriber_unique'
                    );
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('techlify_notification_subscriptions');
    }
}
