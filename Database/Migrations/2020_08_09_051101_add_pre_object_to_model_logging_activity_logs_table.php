<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPreObjectToModelLoggingActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('model_logging_activity_logs', function (Blueprint $table) {
            $table->text('pre_object')->nullable()->default(null);
            $table->unsignedBigInteger('client_id')->nullable()->default(null);
            $table->string('object_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('model_logging_activity_logs', function (Blueprint $table) {
            $table->dropColumn('pre_object');
            $table->dropColumn('client_id');
        });
    }
}
