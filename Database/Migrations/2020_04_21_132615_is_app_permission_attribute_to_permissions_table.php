<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IsAppPermissionAttributeToPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('permissions', 'is_app_permission')) {
            return;
        }
        Schema::table('permissions', function (Blueprint $table) {
            $table->boolean('is_app_permission')->default(false)->comment('This attribute is used to seperate app permissions for client permissions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('is_app_permission');
        });
    }
}
