<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTechlifyNotificationSubscriptionEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('techlify_notification_subscription_entities')) {
            Schema::create(
                'techlify_notification_subscription_entities',
                function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('title');
                    $table->text('description')->nullable()->default(null);
                    $table->timestamps();
                    $table->softDeletes();
                }
            );

            $defaults = [
                ['id' => 1, 'title' => 'Individual', 'description' => 'Individual'],
                ['id' => 2, 'title' => 'Role', 'description' => 'Role'],
                ['id' => 3, 'title' => 'User Type', 'description' => 'User Type'],
            ];

            DB::table('techlify_notification_subscription_entities')
                ->insert($defaults);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('techlify_notification_subscription_entities');
    }
}