<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTechlifyRbacRestrictorTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('techlify_rbac_restrictor_types', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('code')->unique();
            $table->text('description')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        $defaults = [
            ['id' => 1, 'title' => 'Time Based', 'code' => 'time-based', 'description' => 'Restricts the user access based on the time interval'],
            ['id' => 2, 'title' => 'IP Based', 'code' => 'ip-based', 'description' => 'Restricts the user access based on the IP ranges'],
        ];

        DB::table('techlify_rbac_restrictor_types')->insert($defaults);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('techlify_rbac_restrictor_types');
    }
}