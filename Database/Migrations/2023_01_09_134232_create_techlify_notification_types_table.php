<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechlifyNotificationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('techlify_notification_types')) {
            Schema::create(
                'techlify_notification_types', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('title');
                    $table->string('code')->unique();
                    $table->text('description')->nullable()->default(null);
                    $table->boolean('is_admin')->default(false);
                    $table->timestamps();
                    $table->softDeletes();
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('techlify_notification_types');
    }
}
