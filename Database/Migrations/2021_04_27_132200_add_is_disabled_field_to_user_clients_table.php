<?php

use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\LaravelCore\Entities\UserClient;

class AddIsDisabledFieldToUserClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_clients', function (Blueprint $table) {
            $table->boolean('is_disabled')
                ->default(false);
        });

        $disabledUsers = User::where('enabled', false)
            ->get();

        foreach ($disabledUsers as $disabledUser) {
            UserClient::where('user_id', $disabledUser->id)
                ->where('client_id', $disabledUser->client_id)
                ->update(['is_disabled' => true]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_clients', function (Blueprint $table) {
            $table->dropColumn('is_disabled');
        });
    }
}
