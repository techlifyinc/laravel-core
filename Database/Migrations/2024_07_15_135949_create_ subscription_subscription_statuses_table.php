<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionSubscriptionStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('subscription_subscription_statuses')) {
            Schema::create(
                'subscription_subscription_statuses',
                function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('title');
                    $table->text('description')->nullable();
                }
            );
        }
        DB::table('subscription_subscription_statuses')
            ->insert([
                [
                    'id' => 1,
                    'title' => 'Active',
                    'description' => 'Active'
                ],
                [
                    'id' => 2,
                    'title' => 'Inactive',
                    'description' => 'Inactive'
                ]
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_subscription_statuses');
    }
}
