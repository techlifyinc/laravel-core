<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\LaravelCore\Entities\RestrictorType;

class UpdateTechlifyRbacRestrictorTypesTableUpdateDescriptionForRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        RestrictorType::query()
            ->where('code', 'time-based')
            ->update([
                'description' => 'What are the time periods during which a user/role is allowed to log in.'
            ]);

        RestrictorType::query()
            ->where('code', 'ip-based')
            ->update([
                'description' => 'From what IP address (or IP address range) is a user/role allowed to log in.'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        RestrictorType::query()
            ->where('code', 'time-based')
            ->update([
                'description' => 'Restricts the user access based on the time interval'
            ]);

        RestrictorType::query()
            ->where('code', 'ip-based')
            ->update([
                'description' => 'Restricts the user access based on the IP ranges'
            ]);
    }
}
