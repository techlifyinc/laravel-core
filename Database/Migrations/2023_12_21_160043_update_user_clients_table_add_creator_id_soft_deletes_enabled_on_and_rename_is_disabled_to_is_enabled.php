<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('user_clients', function (Blueprint $table) {
            $table->foreignId('creator_id')
                ->nullable()
                ->after('client_id')
                ->constrained('users')
                ->nullOnDelete();
            $table->softDeletes();
            $table->renameColumn('is_disabled', 'is_enabled');
            $table->timestamp('enabled_on')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_clients', function (Blueprint $table) {
            $table->dropConstrainedForeignId('creator_id');
            $table->dropSoftDeletes();
            $table->renameColumn('is_enabled', 'is_disabled');
            $table->dropColumn('enabled_on');
        });
    }
};
