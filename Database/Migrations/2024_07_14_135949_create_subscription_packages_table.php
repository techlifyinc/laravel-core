<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('subscription_packages')) {
            Schema::create(
                'subscription_packages',
                function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('code')->unique('code_unique_index');
                    $table->string('title');
                    $table->text('description')->nullable();
                    $table->boolean('is_enabled')->default(true);
                    $table->unsignedBigInteger('creator_id');
                    $table->timestamps();
                    $table->softDeletes();
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_packages');
    }
}
