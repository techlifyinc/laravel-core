<?php

use Illuminate\Database\Migrations\Migration;
use Modules\LaravelCore\Entities\Restrictor;
use Modules\LaravelCore\Entities\Role;
use Modules\LaravelCore\Entities\TechlifyUser;
use Modules\LaravelCore\Entities\UserType;

class MigrateRestrictorsDataWithPolymorphicFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $restrictors = Restrictor::query()
            ->withoutGlobalScope('TechlifyScore')
            ->get();
        foreach ($restrictors as $restrictor) {
            $levelId = $restrictor->level_id;

            switch ($levelId) {
                case 1:
                    $restrictor->restrictable_type = UserType::class;
                    break;
                case 2:
                    $restrictor->restrictable_type = Role::class;
                    break;
                case 3:
                    $restrictor->restrictable_type = TechlifyUser::class;
                    break;
                // Add more cases as needed

                default:
                    // Handle other cases if required
                    break;
            }

            $restrictor->restrictable_id = $restrictor->restricted_object_id;
            $restrictor->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // no need to reverse.
    }
}
