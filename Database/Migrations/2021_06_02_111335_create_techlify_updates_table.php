<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechlifyUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('techlify_updates', function (Blueprint $table) {
            $table->id();
            $table->dateTimeTz('released_on');
            $table->string('version');
            $table->string('title');
            $table->text('description');
            $table->text('learn_more_link')
                ->nullable()
                ->default(null);
            $table->unsignedBigInteger('creator_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('techlify_updates');
    }
}
