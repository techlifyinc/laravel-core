<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\LaravelCore\Entities\Permission;

class UpdatePermissionsSetAppPermissions extends Migration
{

    private $permissions = [
        'client_create',
        'client_read',
        'client_update',
        'client_delete',
        'module_create',
        'module_read',
        'module_update',
        'module_delete',
        'module_package_read',
        'client_subscription_create',
        'client_subscription_read',
        'client_subscription_update',
        'client_subscription_delete',
        'client_payment_create',
        'client_payment_read',
        'client_payment_update',
        'client_payment_delete',
        'client_payment_set_paid',
        'module_user_read',
        'module_user_add',
        'module_user_delete'

    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach($this->permissions as $permissionToFind) {
            $permission = Permission::where('slug', $permissionToFind)->first();
            if($permission) {
                $permission->is_app_permission = true;
                $permission->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach($this->permissions as $permissionToFind) {
            $permission = Permission::where('slug', $permissionToFind)->first();
            if($permission) {
                $permission->is_app_permission = false;
                $permission->save();
            }
        }
    }
}
