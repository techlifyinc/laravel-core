<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTechlifyRbacRestrictorStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('techlify_rbac_restrictor_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('description')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        $defaults = [
            ['id' => 1, 'title' => 'Active', 'description' => 'Activate the restrictor'],
            ['id' => 2, 'title' => 'Disabled', 'description' => 'Disable the restrictor'],
        ];

        DB::table('techlify_rbac_restrictor_statuses')->insert($defaults);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('techlify_rbac_restrictor_statuses');
    }
}