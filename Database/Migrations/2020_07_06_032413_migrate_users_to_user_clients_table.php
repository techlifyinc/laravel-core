<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Modules\LaravelCore\Entities\UserClient;

class MigrateUsersToUserClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = User::where('user_type_id', '!=', 1)
            ->get();

        foreach ($users as $user) {
            $userClient = new UserClient();
            $userClient->user_id = $user->id;
            $userClient->client_id = $user->client_id;
            $userClient->user_type_id = $user->user_type_id;
            $userClient->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $users = User::get();

        foreach ($users as $user) {
            UserClient::where('user_id', $user->id)->delete();
        }
    }
}
