<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRbacClientConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rbac_client_configurations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id');
            $table->unsignedInteger('max_failed_consecutive_login_attempts')
                ->nullable()
                ->default(5);
            $table->boolean('is_track_old_passwords');
            $table->unsignedInteger('max_old_passwords_block');
            $table->unsignedInteger('min_password_length');
            $table->boolean('is_password_require_number');
            $table->boolean('is_password_require_symbol');
            $table->boolean('is_password_require_capital_letter');
            $table->unsignedBigInteger('creator_id')
                ->nullable()
                ->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rbac_client_configurations');
    }
}
