<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::statement('UPDATE user_clients uc
            JOIN users u ON uc.user_id = u.id
            SET uc.enabled_on = u.created_at');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        DB::table('user_clients')->update(['enabled_on' => null]);
    }
};
