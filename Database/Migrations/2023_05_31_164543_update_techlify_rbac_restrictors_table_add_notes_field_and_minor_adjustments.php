<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\LaravelCore\Entities\RestrictorStatus;

class UpdateTechlifyRbacRestrictorsTableAddNotesFieldAndMinorAdjustments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('techlify_rbac_restrictors', function (Blueprint $table) {
            $table->text('notes');
            $table->unsignedBigInteger('status_id')
                ->default(RestrictorStatus::ACTIVE)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('techlify_rbac_restrictors', function (Blueprint $table) {
            $table->dropColumn('notes');

            $table->unsignedBigInteger('status_id')
                ->change();
        });
    }
}
