<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechlifyImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('techlify_imports')) {
            Schema::create('techlify_imports', function (Blueprint $table) {
                $table->id();
                $table->date('date');
                $table->unsignedBigInteger('client_id');
                $table->unsignedBigInteger('creator_id');
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('techlify_imports')) {
            Schema::dropIfExists('techlify_imports');
        }
    }
}
