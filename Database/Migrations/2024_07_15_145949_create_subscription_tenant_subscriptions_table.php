<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionTenantSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('subscription_tenant_subscriptions')) {
            Schema::create(
                'subscription_tenant_subscriptions',
                function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->foreignId('tenant_id')->index('tenant_id_index');
                    $table->foreignId('package_id')->index('package_id_index');
                    $table->integer('cost_per_unit')->nullable();
                    $table->integer('units')->nullable();
                    $table->integer('total')->nullable();
                    $table->foreignId('status_id');
                    $table->text('details')->nullable();
                    $table->boolean('is_unit_limitation_enforced')->default(false);
                    $table->boolean('is_trail')->default(false);
                    $table->date('trail_started_at')->nullable();
                    $table->date('trail_ended_at')->nullable();
                    $table->date('started_at')->nullable();
                    $table->date('ended_at')->nullable();
                    $table->foreignId('creator_id');
                    $table->timestamps();
                    $table->softDeletes();
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_tenant_subscriptions');
    }
}
