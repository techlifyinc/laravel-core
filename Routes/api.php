<?php

use Illuminate\Support\Facades\Route;
use Modules\LaravelCore\Http\Controllers\ChangePasswordController;
use Modules\LaravelCore\Http\Controllers\PermissionController;
use Modules\LaravelCore\Http\Controllers\RbacClientConfigurationController;
use Modules\LaravelCore\Http\Controllers\RestrictorController;
use Modules\LaravelCore\Http\Controllers\RestrictorLevelController;
use Modules\LaravelCore\Http\Controllers\RestrictorStatusController;
use Modules\LaravelCore\Http\Controllers\RestrictorTypeController;
use Modules\LaravelCore\Http\Controllers\RoleController;
use Modules\LaravelCore\Http\Controllers\TechlifyImportController;
use Modules\LaravelCore\Http\Controllers\UserController;
use Modules\LaravelCore\Subscription\Http\Controllers\SubscriptionPackageController;
use Modules\LaravelCore\Subscription\Http\Controllers\SubscriptionPackagePermissionController;
use Modules\LaravelCore\Subscription\Http\Controllers\TenantSubscriptionController;

/* Roles */
Route::get("roles", [RoleController::class, 'index'])
    ->name("roles.index")
    ->middleware("TechlifyAccessControl:state:logged-in");
Route::post("roles", [RoleController::class, 'store'])
    ->middleware("TechlifyAccessControl:role_create");
Route::put("roles/{id}", [RoleController::class, 'update'])
    ->middleware("TechlifyAccessControl:role_update");
Route::delete("roles/{id}", [RoleController::class, 'destroy'])
    ->middleware("TechlifyAccessControl:role_delete");
Route::get("roles/{id}", [RoleController::class, 'show'])
    ->middleware("TechlifyAccessControl:state:logged-in");
Route::patch("roles/{role}/permissions/{permission}/add", [RoleController::class, 'addPermission'])
    ->middleware("TechlifyAccessControl:role_permission_add");
Route::patch("roles/{role}/permissions/{permission}/remove", [RoleController::class, 'removePermission'])
    ->middleware("TechlifyAccessControl:role_permission_remove");

/* Permissions */
Route::get("permissions", [PermissionController::class, 'index']);

/* Users */
Route::get("users", "UserController@index")
    ->middleware("TechlifyAccessControl:user_read");
Route::post("users", "UserController@store")
    ->middleware("TechlifyAccessControl:user_create");
Route::put("users/{id}", "UserController@update")
    ->middleware("TechlifyAccessControl:user_update");
Route::post("users/{id}/delete", "UserController@destroy")
    ->middleware("TechlifyAccessControl:user_delete");
Route::get("users/{id}", "UserController@show")
    ->middleware("TechlifyAccessControl:user_read");
Route::patch("users/{user}/enable", "UserController@enable")
    ->middleware("TechlifyAccessControl:user_enable");
Route::patch("users/{user}/disable", "UserController@disable")
    ->middleware("TechlifyAccessControl:user_disable");
Route::get("other-users", "UserController@otherClientUser")
    ->middleware("TechlifyAccessControl:user_read");

Route::put("users/profile/update", "UserController@updateCurrentUserProfile");

Route::post('/user/login', "SessionController@login");
Route::post('/user/logout', "SessionController@destroy");
Route::get('/user/current', [UserController::class, 'currentUser']);
Route::patch("user/current/update-password", [ChangePasswordController::class, 'updatePassword'])
    ->middleware('TechlifyAccessControl:state:logged-in');
Route::patch("user/set-password", [UserController::class, 'setPassword'])
    ->middleware('TechlifyAccessControl:state:logged-in');

/* Company Sign Up */
Route::post("company-sign-up", "UserController@companySignUp");

/* Forgot Password */
Route::post("forgot-password", "UserController@forgotPassword");

/* Client */
Route::get("clients", "ClientController@index")
    ->middleware("TechlifyAccessControl:client_read");
Route::post("clients", "ClientController@store")
    ->middleware("TechlifyAccessControl:client_create");
Route::put("clients/{id}", "ClientController@update")
    ->middleware("TechlifyAccessControl:company_information_update");
Route::delete("clients/{id}", "ClientController@destroy")
    ->middleware("TechlifyAccessControl:client_delete");
Route::get("clients/{id}", "ClientController@show")
    ->middleware("TechlifyAccessControl:company_information_read");
Route::post("clients/upload", "ClientController@upload")
    ->middleware("TechlifyAccessControl:company_information_update");
Route::get("clients/enable/{client}", "ClientController@enableClient")
    ->middleware("TechlifyAccessControl:client_read");
Route::get("clients/disable/{client}", "ClientController@disableClient")
    ->middleware("TechlifyAccessControl:client_read");

/* Subscription Status */
Route::get("client-subscription-statuses", "ClientSubscriptionStatusController@index");

/* Client Subscription */
Route::get("client-subscriptions", "ClientSubscriptionController@index")
    ->middleware("TechlifyAccessControl:client_subscription_read");
Route::post("client-subscriptions", "ClientSubscriptionController@store")
    ->middleware("TechlifyAccessControl:client_subscription_create");
Route::put("client-subscriptions/{subscription}", "ClientSubscriptionController@update")
    ->middleware("TechlifyAccessControl:client_subscription_update");
Route::delete("client-subscriptions/{id}", "ClientSubscriptionController@destroy")
    ->middleware("TechlifyAccessControl:client_subscription_delete");
Route::get("client-subscriptions/{id}", "ClientSubscriptionController@show")
    ->middleware("TechlifyAccessControl:client_subscription_read");
Route::post("client-subscriptions/trial", "ClientSubscriptionController@trialSubscription");

Route::put("client-subscription/set-accessible", "ClientSubscriptionController@setAccessibleByEntireTeam")
    ->middleware("TechlifyAccessControl:client_subscription_update");

/* Client Payments */
Route::get("client-payments", "ClientPaymentController@index")
    ->middleware("TechlifyAccessControl:client_payment_read");
Route::post("client-payments", "ClientPaymentController@store")
    ->middleware("TechlifyAccessControl:client_payment_create");
Route::put("client-payments/{id}", "ClientPaymentController@update")
    ->middleware("TechlifyAccessControl:client_payment_update");
Route::delete("client-payments/{id}", "ClientPaymentController@destroy")
    ->middleware("TechlifyAccessControl:client_payment_delete");
Route::patch("client-payments/{id}/set-paid", "ClientPaymentController@setPaid")
    ->middleware("TechlifyAccessControl:client_payment_set_paid");

/* Modules */
Route::get("modules", "ModuleController@index")
    ->middleware("TechlifyAccessControl:module_read");
Route::post("modules", "ModuleController@store")
    ->middleware("TechlifyAccessControl:module_create");
Route::put("modules/{id}", "ModuleController@update")
    ->middleware("TechlifyAccessControl:module_update");
Route::delete("modules/{id}", "ModuleController@destroy")
    ->middleware("TechlifyAccessControl:module_delete");
Route::get("modules/{id}", "ModuleController@show")
    ->middleware("TechlifyAccessControl:module_read");
Route::patch("modules/{id}/enable", "ModuleController@enable")
    ->middleware("TechlifyAccessControl:module_update");
Route::patch("modules/{id}/disable", "ModuleController@disable")
    ->middleware("TechlifyAccessControl:module_update");

Route::get("module-packages", "ModulePackageController@index")
    ->middleware("TechlifyAccessControl:module_package_read");

/* Module Users */
Route::get("module-users", "ModuleUserController@index")
    ->middleware("TechlifyAccessControl:module_user_read");
Route::post("module-users", "ModuleUserController@store")
    ->middleware("TechlifyAccessControl:module_user_add");
Route::post("module-users/invite", "ModuleUserController@inviteuser")
    ->middleware("TechlifyAccessControl:module_user_add");
Route::put("module-users/{id}", "ModuleUserController@update")
    ->middleware("TechlifyAccessControl:module_user_update");
Route::put("module-users-delete/{id}", "ModuleUserController@destroy")
    ->middleware("TechlifyAccessControl:module_user_delete");

Route::get("current-client-modules", "ModuleController@getCurrentClientModules");

Route::get('/notes', 'NoteController@index');
Route::post('/notes', 'NoteController@store');
Route::put('/notes/{note}', 'NoteController@update');
Route::delete('/notes/{note}', 'NoteController@destroy');

Route::get('/entity-files', 'EntityFileController@index');
Route::post('/entity-files', 'EntityFileController@store');
Route::put('/entity-files/{file}', 'EntityFileController@update');
Route::delete('/entity-files/{file}', 'EntityFileController@destroy');
Route::post('/entity-files-upload', 'EntityFileController@upload');

/* Switch Login */
Route::get("switch-company/{id}", "UserController@switchCompany");

/* User Clients */
Route::get("user-clients", "UserClientController@index");

Route::get('/audit-logs', 'AuditLogController@index');
Route::get('/audit-logs/{log}', 'AuditLogController@show');
Route::get('/recent-log-for-model', 'AuditLogController@recentLog');
Route::get('/audit-log-types', 'AuditLogController@objectType');


/* Techlify app updates */
Route::get("techlify-updates", "TechlifyUpdateController@index")
    ->middleware("TechlifyAccessControl:state:logged-in");
Route::get("techlify-updates/update-view", "TechlifyUpdateController@updateAllViewForCurrentUser")
    ->middleware("TechlifyAccessControl:state:logged-in");
Route::post("techlify-updates", "TechlifyUpdateController@create")
    ->middleware("TechlifyAccessControl:ut:app-admin");
Route::put("techlify-updates/{techlifyUpdate}", "TechlifyUpdateController@update")
    ->middleware("TechlifyAccessControl:ut:app-admin");
Route::delete("techlify-updates/{techlifyUpdate}", "TechlifyUpdateController@destroy")
    ->middleware("TechlifyAccessControl:ut:app-admin");
Route::get("current-user-techlify-updates", "TechlifyUpdateController@getUpdateForCurrentUser")
    ->middleware("TechlifyAccessControl:state:logged-in");

Route::post("techlify-update-views", "TechlifyUpdateViewController@store")
    ->middleware("TechlifyAccessControl:state:logged-in");

Route::get("email-subscription-frequencies", "EmailSubscriptionFrequencyController@index");

Route::get("email-subscription-types", "EmailSubscriptionTypeController@index");
Route::post("email-subscription-types", "EmailSubscriptionTypeController@store");
Route::put("email-subscription-types/{esType}", "EmailSubscriptionTypeController@update");
Route::delete("email-subscription-types/{esType}", "EmailSubscriptionTypeController@destroy");

Route::get("email-subscription-users", "EmailSubscriptionUserController@index");
Route::post("email-subscription-users", "EmailSubscriptionUserController@createOrUpdate");
Route::put("email-subscription-users/{esUser}", "EmailSubscriptionUserController@createOrUpdate");
Route::delete("email-subscription-users/{esUser}", "EmailSubscriptionUserController@destroy");
Route::post("email-subscription-for-user", "EmailSubscriptionUserController@subscribe");

Route::get("user-login-history", "OauthAccessController@userLoginHistory");

Route::get("techlify-tagger-tags", "TechlifyTaggerTagController@index");
Route::post("techlify-tagger-tags", "TechlifyTaggerTagController@store");
Route::put("techlify-tagger-tags/{tag}", "TechlifyTaggerTagController@update");
Route::delete("techlify-tagger-tags/{tag}", "TechlifyTaggerTagController@destroy");

Route::get('features', 'FeatureController@index')->middleware('TechlifyAccessControl:state:logged-in');

Route::get('feature/{feature}/enable', 'ClientFeatureController@enable')->middleware('TechlifyAccessControl:state:logged-in');

Route::get('feature/{feature}/disable', 'ClientFeatureController@disable')->middleware('TechlifyAccessControl:state:logged-in');

Route::get('access-control-configs', [RbacClientConfigurationController::class, 'index'])
    ->middleware('TechlifyAccessControl:state:logged-in');
Route::post('access-control-configs', [RbacClientConfigurationController::class, 'create'])
    ->middleware('TechlifyAccessControl:ut:client-admin');
Route::put('access-control-configs/{config}', [RbacClientConfigurationController::class, 'update'])
    ->middleware('TechlifyAccessControl:ut:client-admin');
Route::delete('access-control-configs/{config}', [RbacClientConfigurationController::class, 'destroy'])
    ->middleware('TechlifyAccessControl:ut:client-admin');
Route::get('access-control-configs/current-client', [RbacClientConfigurationController::class, 'getConfigForCurrentClient'])
    ->middleware('TechlifyAccessControl:state:logged-in');


// Notification Routes
Route::get("notification-types", "NotificationTypeController@index");
Route::post("notification-types", "NotificationTypeController@store");
Route::put(
    "notification-types/{notificationType}",
    "NotificationTypeController@update"
);
Route::delete(
    "notification-types/{notificationType}",
    "NotificationTypeController@destroy"
);

Route::get(
    "notification-subscription-entities",
    "NotificationSubscriptionEntityController@index"
);

Route::get("notification-subscriptions", "NotificationSubscriptionController@index");
Route::get(
    "notification-subscriptions-unique",
    "NotificationSubscriptionController@unique"
);
Route::post(
    "notification-subscriptions",
    "NotificationSubscriptionController@store"
);
Route::put(
    "notification-subscriptions/{subscription}",
    "NotificationSubscriptionController@update"
);
Route::patch(
    "notification-subscriptions-toggle/{subscription}",
    "NotificationSubscriptionController@toggle"
);
Route::post(
    "notification-subscriptions-for-user",
    "NotificationSubscriptionController@subscribeForUser"
);
Route::delete(
    "notification-subscriptions/{subscription}",
    "NotificationSubscriptionController@destroy"
);

Route::get("notifications", "NotificationController@index");

Route::get("notification-users", "NotificationUserController@index");
Route::patch(
    "notification-user-status/{nUser}",
    "NotificationUserController@updateStatus"
);

Route::get("notification-statuses", "NotificationStatusController@index");

Route::get("notification-user-statuses", "NotificationUserStatusController@index");

Route::get(
    "notification-user-timelines",
    "NotificationUserTimelineController@index"
);

Route::get('restrictor-types', [RestrictorTypeController::class, 'index']);

Route::get('restrictor-levels', [RestrictorLevelController::class, 'index']);

Route::get('restrictor-statuses', [RestrictorStatusController::class, 'index']);

Route::get('restrictors', [RestrictorController::class, 'index']);
Route::post('restrictors', [RestrictorController::class, 'store']);
Route::get('restrictors/{restrictor}', [RestrictorController::class, 'show']);
Route::put('restrictors/{restrictor}', [RestrictorController::class, 'update']);

// Techlify Import
Route::get(
    'import-histories',
    [TechlifyImportController::class, 'index']
)->middleware('TechlifyAccessControl:state:logged-in');

//Subscription Packages
Route::get('subscription-packages', [SubscriptionPackageController::class, 'index'])
    ->name('subscription-packages.index')
    ->middleware('TechlifyAccessControl:ut:app-admin');
Route::patch('subscription-packages/toggle/{subscriptionPackage}', [SubscriptionPackageController::class, 'toggle'])
    ->name('subscription-packages.toggle')
    ->middleware('TechlifyAccessControl:ut:app-admin');
Route::get('subscription-packages/{subscriptionPackage}', [SubscriptionPackageController::class, 'show'])
    ->name('subscription-packages.show')
    ->middleware('TechlifyAccessControl:ut:app-admin');

//Subscription package permission
Route::get('subscription-package-permissions', [SubscriptionPackagePermissionController::class, 'index'])
    ->name('subscription-package-permissions.index')
    ->middleware('TechlifyAccessControl:ut:app-admin');
Route::post('subscription-package-permissions', [SubscriptionPackagePermissionController::class, 'store'])
    ->name('subscription-package-permissions.store')
    ->middleware('TechlifyAccessControl:ut:app-admin');
Route::put('subscription-package-permissions/{packagePermission}', [SubscriptionPackagePermissionController::class, 'update'])
    ->name('subscription-package-permissions.update')
    ->middleware('TechlifyAccessControl:ut:app-admin');
Route::delete('subscription-package-permissions/{packagePermission}', [SubscriptionPackagePermissionController::class, 'destroy'])
    ->name('subscription-package-permissions.destroy')
    ->middleware('TechlifyAccessControl:ut:app-admin');


//Tenant Subscription
Route::get('tenant-subscriptions', [TenantSubscriptionController::class, 'index'])
    ->name('tenant-subscriptions.index')
    ->middleware('TechlifyAccessControl:ut:app-admin');
Route::post('tenant-subscriptions', [TenantSubscriptionController::class, 'store'])
    ->name('tenant-subscriptions.store')
    ->middleware('TechlifyAccessControl:ut:app-admin');
Route::put('tenant-subscriptions/{tenantSubscription}', [TenantSubscriptionController::class, 'update'])
    ->name('tenant-subscriptions.update')
    ->middleware('TechlifyAccessControl:ut:app-admin');
Route::delete('tenant-subscriptions/{tenantSubscription}', [TenantSubscriptionController::class, 'destroy'])
    ->name('tenant-subscriptions.destroy')
    ->middleware('TechlifyAccessControl:ut:app-admin');
Route::patch('tenant-subscriptions-status/{tenantSubscription}/{status}', [TenantSubscriptionController::class, 'updateStatus'])
    ->name('tenant-subscriptions.updateStatus')
    ->middleware('TechlifyAccessControl:ut:app-admin');
