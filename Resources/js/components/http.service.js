import axios from 'axios'

export default {
  /**
   * Wrapper for HTTP Get Request
   * @param url The url path to be sent to the server
   * @param {*} params The params to be passed to the server
   * @returns
   */
  GET(url, params) {
    return axios.get(url, {
      params: params,
    })
  },

  /**
   * Wrapper for HTTP POST Request
   * @param  url The url path to be sent to the server
   * @param {*} object The object containing the POST data to be sent
   */
  POST(url, object) {
    if (null == object) {
      object = { blank: 'blank' }
    }
    return axios.post(url, object)
  },
  /**
   * Wrapper for HTTP PUT Request
   * @param  url The url path to be sent to the server
   * @param {*} object The object containing the PUT data to be sent
   */
  PUT(url, object) {
    if (null == object) {
      object = { blank: 'blank' }
    }
    return axios.put(url, object)
  },

  /**
   * Wrapper for HTTP PATCH Request
   * @param  url The url path to be sent to the server
   * @param {*} object The object containing the PATCH data to be sent
   */
  PATCH(url, object) {
    if (null == object) {
      object = { blank: 'blank' }
    }
    return axios.patch(url, object)
  },

  /**
   * Wrapper for HTTP DELETE Request
   * @param  url The url path to be sent to the server
   */
  DELETE(url) {
    return axios.delete(url)
  },
}
