<?php

namespace Modules\LaravelCore\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Custom validation rule to ensure the new password is not the same as the user's email.
 */
class NotSameAsEmail implements Rule
{
    protected string $email;

    /**
     * Create a new rule instance.
     *
     * @param string $email
     * @return void
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return strtolower($value) !== strtolower($this->email);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'The :attribute cannot be the same as the email.';
    }
}
