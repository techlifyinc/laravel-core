<?php

return [
    'name' => 'LaravelCore',
    'app_frontend_link' => env('APP_FRONTEND_LINK', "localhost:4200"),
    'emailing' => [
        'welcome' => true,
        'forgot_password' => true,
        'get_started' => false,
    ],
    'email_templates' => [
        'forgot_password' => 'laravelcore::email.forgot-password-mail',
        'welcome' => 'laravelcore::email.welcome-mail',
        'client_welcome' => 'laravelcore::email.welcome-client-mail',
        'module_invite_user' => 'laravelcore::email.invite-user-mail',
        'module_added_to_user' => 'laravelcore::email.module-added-to-user-mail',
        'new_company_user' => 'laravelcore::email.new-company-user-mail',
    ],
    'email_sender' => [
        'mail_from_address' => env("MAIL_FROM_ADDRESS", 'no-reply@techlify.com'),
        'mail_from_name' => env("MAIL_FROM_NAME", 'no-reply@techlify.com'),
    ],
    'watermark_path' => public_path('watermark.png'),
    'image_resize' => 1500,
    'non_client_user_types' => env('NON_CLIENT_USER_TYPES', '1,4'),
    'entity_file_disk' => env('ENTITY_FILE_DISK', 'techlify-inc'),
    'entity_file_sub_path' => env('ENTITY_FILE_SUB_PATH', 'entity-files'),
    'entity_file_max_upload_file_size' => env('ENTITY_FILE_MAX_UPLOAD_FILE_SIZE', 20480)
];

// @todo Setup configuration to enable/disable social signup
