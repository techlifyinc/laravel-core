<?php

namespace Modules\LaravelCore\Tests\Traits;

use Illuminate\Http\Response;

/**
 * This is a trait to be added to test to validate the index method
 * - It fails without filter configuration 
 * - It fails without a valid factory
 * The parent class needs to be a child of TechlifyTestCase
 */
trait HasFilterRequirement
{
    /**
     * Configuration of filter
     * `filterKey` => Required | String | Key to be specified in filter model
     * `dbColumn` => Optional | String | Needed if needs to compared against the table column
     * `dbValue` => Optional | mixed or Array | Value to be inserted for db value.
     * `filterFor` => Required | mixed or Array | Value to be filtered and validated.
     * `count` => Optional | numeric | Count of factories to be generated
     * `filterQuery` => Optional | QueryBuilder | Optional query to be compared against the API filter value.
     */
    protected $filters = [];

    /**
     * The name of the factory model.
     *
     * @var class-string<\Illuminate\Database\Eloquent\Model>
     */
    protected $modelFactory;

    /**
     * The name of the factory model.
     *
     * @var class-string<\Illuminate\Database\Eloquent\Model>
     */
    protected $model;


    /**
     * Method that loops through each filter configuration and generate factory for configuration
     * It's mandatory to have `dbColumn` and `dbValue` in the filter configuration to generate factory
     */
    private function setupDataForFilter()
    {
        foreach ($this->filters as $filter) {
            //Setup data only if filter has valid dbColumn and valid dbVlaue
            if (isset($filter['dbColumn']) && isset($filter['dbValue'])) {
                if (is_array($filter['dbValue'])) {
                    foreach ($filter['dbValue'] as $value) {
                        $this->createFactory($filter, $value);
                    }
                } else {
                    $this->createFactory($filter, $filter['dbValue']);
                }
            }
        }
    }

    /**
     * Method that generate the dynamic query matching for the filter configuration.
     * `filterFor` in filter configuration is mandatory for dynamic generation.
     * If `filterFor` is an array, then it is matched in whereIn clause against the `dbColumn`.
     * If `filterFor` is constant, then it is matched in where clause against the `dbColumn`.
     * For more advanced support, use `filterQuery` configuration.
     * @param mixed $filter filter configuration.
     * @return mixed instance of query builder
     */
    private function generateQueryForFilter($filter)
    {
        if (is_array($filter['filterFor'])) {
            return $this->model::whereIn($filter['dbColumn'], $filter['filterFor']);
        }
        return $this->model::where($filter['dbColumn'], $filter['filterFor']);
    }

    /**
     * Method to get the data from the route.
     * Internally uses the base class method to get the data
     * @param mixed $filterModel model for filter route.
     * @return mixed json response
     */
    private function getFilterData($filterModel)
    {
        $response = $this->get(route($this->routeName, $filterModel));
        $response->assertStatus(Response::HTTP_OK);
        return json_decode($response->getContent())->data ?? [];
    }

    /**
     * Create a new factory for the provided model
     * Uses the value of `$this->modelFactory` to generate the model
     * @param mixed $filter filter configuration.
     * @param mixed $value value of the db column
     */
    private function createFactory($filter, $value)
    {
        $this->modelFactory::new()
            ->count($filter['count'] ?? 1)
            ->make([
                $filter['dbColumn'] => $value,
            ]);
    }

    /**
     * Let's test filters
     */
    public function testFiltering()
    {
        //Authenticate as client admin
        $this->authenticateClientAdmin();
        //Setup data
        $this->setupDataForFilter();

        //Foreach filter configuration, do the testing
        foreach ($this->filters as $filter) {
            //Filter has a value
            if (isset($filter['filterFor'])) {
                $filterModel = [
                    $filter['filterKey'] => is_array($filter['filterFor']) ?
                        join(',', $filter['filterFor']) :
                        $filter['filterFor']
                ];
                $responseResult = $this->getFilterData($filterModel);
                $dbQuery = $filter['filterQuery'] ??
                    (isset($filter['dbColumn']) ? $this->generateQueryForFilter($filter) : null);

                //Fail if there is no db query to lookup    
                if (!$dbQuery) {
                    $this->fail('Invalid query, either provide "filterQuery" or "dbColumn" in the filter model');
                }

                //execute the query
                $queryResult = $dbQuery->get();

                //1.check if count matches
                $this->assertCount(count($queryResult), $responseResult, 'Result count not matching');

                //2.check if all data is correct
                if (isset($filter['dbColumn'])) {
                    $dbColumn = $filter['dbColumn'];
                    foreach ($responseResult as $result) {
                        if (is_array($filter['filterFor'])) {
                            $this->assertContains($result->$dbColumn, $filter['filterFor'], 'Filter result are not matching');
                        } else {
                            $this->assertEquals($result->$dbColumn, $filter['filterFor'], 'Filter result are not matching');
                        }
                    }
                }
            }
        }

    }
}