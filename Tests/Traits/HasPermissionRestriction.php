<?php

namespace Modules\LaravelCore\Tests\Traits;


/**
 * Test that the route is blocked for an unauthenticated user
 *
 * @todo Rename this properly
 */
trait HasPermissionRestriction
{

    /**
     * Ensure that the app is requiring permission to access this URL
     *
     * @return void
     */
    public function testRequirePermission()
    {
        $data = [
            'name' => 'Test Employee',
            'email' => '',
        ];

        /**
         * Sometimes we have required parameters that we need to pass
         * e.g POST /payrolls/ach-export/{payroll}
         *
         * if we don't pass the payroll parameter, we'll get a 404
         */
        switch ($this->httpMethod) {
            default:
            case 'GET':
                $this->get(route($this->routeName), $data)
                    ->assertStatus(403);
                break;
            case 'POST':
                $route = route($this->routeName, $this->routeParameters, false);
                $this->post($route, $data)
                    ->assertStatus(403);
                break;
            case 'PUT':
                $route = route($this->routeName, $this->routeParameters, false);
                $this->put($route, $data)
                    ->assertStatus(403);
                break;
            case 'DELETE':
                $this->delete(route($this->routeName), $data)
                    ->assertStatus(403);
                break;
            case 'PATCH':
                $this->patch(route($this->routeName), $data)
                    ->assertStatus(403);
                break;
        }
    }
}
