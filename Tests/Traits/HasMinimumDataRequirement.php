<?php

namespace Modules\LaravelCore\Tests\Traits;

/**
 * This is a trait to be added to tests, that checks the method that
 * - It works with the minimum required data
 * - It fails without the minimum required data
 *
 * The parent class needs a variable called $minimumData
 * The parent class needs to be a child of TechlifyTestCase
 */
trait HasMinimumDataRequirement
{

    /**
     * Do a basic test without the minimum required data and ensure that it fails
     *
     * @return void
     */
    public function testCreateWithoutMinimumData()
    {
        parent::authenticateClientAdmin();
        /**
         * Let's loop through every combination of data without 1 value and ensure it fails
         */
        foreach ($this->minimumData as $key => $value) {
            $dataCopy = $this->minimumData;
            unset($dataCopy[$key]);

            // If we have route parameters, we are testing the update service
            if ($this->routeParameters) {
                $route = $this->hasMinimumDataRequirementsRoute
                    ?? $this->routeName;

                $response = $this->withHeaders(['Accept' => 'application/json'])
                    ->put(route($route, $this->routeParameters), $dataCopy);

                // Let's assert that this fails
                $this->assertContains($response->getStatusCode(), $this->errorResponses);

                continue;
            }

            // Added JSON headers, since we are simulating JSON request
            $response = $this->withHeaders(['Accept' => 'application/json'])
                ->post(route($this->routeName), $dataCopy);
            $this->assertContains($response->getStatusCode(), $this->errorResponses);
        }
    }

    /**
     * Do a basic test with the minimum required data and ensure that the record is created
     *
     * @return void
     */
    public function testCreateWithMinimumData()
    {
        parent::authenticateClientAdmin();

        // If we have route parameters, we are testing the update service
        if ($this->routeParameters) {
            $route = $this->hasMinimumDataRequirementsRoute
                ?? $this->routeName;

            $response = $this->withHeaders(['Accept' => 'application/json'])
                ->put(route($route, $this->routeParameters), $this->minimumData);
            $response->assertSuccessful();
            return;
        }

        // Added JSON headers, since we are simulating JSON request
        $response = $this->withHeaders(['Accept' => 'application/json'])
            ->post(route($this->routeName), $this->minimumData);
        $response->assertSuccessful();
    }
}