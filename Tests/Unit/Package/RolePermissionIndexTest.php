<?php

namespace Modules\LaravelCore\Tests\Unit\Package;

use Illuminate\Http\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Modules\LaravelCore\Http\Controllers\RoleController;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Orchestra\Testbench\TestCase;

class RolePermissionIndexTest extends TestCase
{
    use WithWorkbench;

    protected $routeName = 'roles.index';

    protected function defineRoutes($router)
    {
        $router->get("roles", [RoleController::class, 'index'])
            ->name("roles.index");
    }

    public function testRolesRouteIsSuccessful()
    {
        $user = $this->getMockBuilder(Authenticatable::class)->getMock();
        
        $this->be($user);

        $response = $this->get(route($this->routeName));
        $response->assertStatus(Response::HTTP_OK);
    }
}
