<?php

namespace Modules\LaravelCore\Tests\Unit\Package\Emails;

use Modules\LaravelCore\Emails\ForgotPassword;
use Orchestra\Testbench\TestCase;

class ForgotPasswordTest extends Testcase
{
    public function testCreateEmailWithoutClient()
    {
        $user = $this->getMockBuilder('App\Models\User')->getMock();
        $user->client = null;
        $forgotPasswordEmail = new ForgotPassword($user, 'test subject', 'password');
        $this->assertEmpty($forgotPasswordEmail->replyTo);
    }
}