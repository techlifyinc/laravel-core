<?php

namespace Modules\LaravelCore\Tests\Unit;

use Modules\LaravelCore\Tests\Traits\HasFilterRequirement;

/**
 * A parent test class for testing index service
 */
class TechlifyIndexBaseTest extends TechlifyTestCase
{

    /**
     * Filter requirement is madatory in indes test,
     * We need make sure the filter configurations are configured, while extending the `TechlifyIndexBaseTest`
     */
    use HasFilterRequirement;

    /**
     * Method used to test sorting 
     * @param mixed $sortField field to tested
     * @param mixed $sortOrder ASC|DESC order of sorting to be tested
     * public function testStatusSortAsc()
     * {
     *       $this->_testSorting('status_id', 'ASC');
     * }
     */
    protected function _testSorting($sortField, $sortOrder)
    {
        $this->authenticateClientAdmin(); // user id 2, client id 1
        $filters = ['sort_by' => $sortField . '|' . $sortOrder];
        $payload = $this->_loadDataForFilters($filters);

        $this->_checkSorting(
            $payload->data,
            $sortField,
            $sortOrder
        );
    }

    /**
     * Method used to test advanced combinational and custom filters
     * For simple filter testing, use `HasFilterRequirement`
     * @param mixed $filters array of filter settings
     * Eg: $filters = ['date_from'=>'2019-12-01','date_to'=>'2019-12-31']
     *  public function testDateFiltering()
     *  {
     *      $this->_testFiltering($filters);
     *  }
     */
    protected function _testFiltering($filters)
    {
        $this->authenticateClientAdmin(); // user id 2, client id 1

        $payload = $this->_loadDataForFilters($filters);

        foreach ($payload->data as $record) {
            foreach ($filters as $key => $filter) {
                $this->assertEquals($record[$key], $filter);
            }
        }
    }

    /**
     * We'll use this function to loop through data and ensure when it is sorted that they are in the
     * correct order
     *
     * @param array $data array items to be validated
     * @param string $key key to be checked in the data item
     * @param string $sortOrder default TRUE, order of sort to be validated
     */
    private function _checkSorting($data, $key, $sortOrder = true)
    {
        $previousItem = null;
        foreach ($data as $item) {
            //If previous item is set, compare the order
            if ($previousItem != null) {
                if ($sortOrder == 'ASC') {
                    $this->assertTrue($item->$key >= $previousItem->$key);
                } else {
                    $this->assertTrue($item->$key <= $previousItem->$key);
                }
            }

            //Set the previous item
            $previousItem = $item;
        }
    }
}
