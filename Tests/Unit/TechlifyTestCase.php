<?php

namespace Modules\LaravelCore\Tests\Unit;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Http\Response;

/**
 * A parent test case that has basic functionality for all tests
 */
class TechlifyTestCase extends TestCase
{
    // Name of the route, has to be set from Child Class.
    protected $routeName = '???';

    // HTTP method to use, by default 'GET' is used.
    protected $httpMethod = 'GET';

    // Any route params that has to be sent with request.
    protected $routeParameters = [];

    // Minimum required data for the test case execution.
    protected $minimumData = [];

    // Possible error responses, that can occur during minimum data validation
    protected $errorResponses = [Response::HTTP_UNPROCESSABLE_ENTITY];

    // Used to verify the clientID of the execution.
    protected $clientID;

    // To override the default login user id.
    protected $loginUserId = 2;  //Defaults to client admin

    /**
     * Authenticate as a client Admin User
     * @todo Rename the method later
     * @return void
     */
    protected function authenticateClientAdmin()
    {
        $user = User::find($this->loginUserId);
        $this->clientID = $user->client_id ?? null;
        $this->actingAs($user);
    }

    /**
     * Let's load data given a specific set of filters
     *
     * This is mainly used for index services
     *
     * @return StdObject the response object
     */
    protected function _loadDataForFilters($filters)
    {
        $response = $this->get(route($this->routeName, $filters));

        /* Ensure we get a good response */
        $response->assertStatus(200);

        return json_decode($response->getContent());
    }
}
