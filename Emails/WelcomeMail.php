<?php

namespace Modules\LaravelCore\Emails;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $modules;
    public $password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $modules, $password)
    {
        $this->user = $user;
        $this->modules = $modules;
        $this->password = $password;
        $this->subject = '[' . config('app.name') . '] ' . 'Welcome to ' . config('app.name');
        $this->replyTo = [['address' => $user->client->email, 'name' => $user->client->name]];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(
            config('laravelcore.email_sender.mail_from_address'),
            config('laravelcore.email_sender.mail_from_name')
        )
            ->view(config('laravelcore.email_templates.welcome'))
            ->with('user', $this->user, $this->modules, $this->password);
    }
}
