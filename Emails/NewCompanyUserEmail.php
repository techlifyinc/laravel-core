<?php

namespace Modules\LaravelCore\Emails;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewCompanyUserEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $modules;
    public $client;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $modules, $client)
    {
        $this->user = $user;
        $this->modules = $modules;
        $this->client = $client;
        $this->subject = '[' . config('app.name') . '] ' . 'Your account has been added to ' . $this->client->name;
        $this->replyTo = [['address' => $client->email, 'name' => $client->name]];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(
            config('laravelcore.email_sender.mail_from_address'),
            config('laravelcore.email_sender.mail_from_name')
        )
            ->view(config('laravelcore.email_templates.new_company_user'))
            ->with('user', $this->user, $this->modules, $this->client);
    }
}
