<?php

namespace Modules\LaravelCore\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendMobileNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $deviceTokens;
    protected $title;
    protected $body;
    protected $info;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($title, $body, $info, $deviceTokens)
    {
        $this->title = $title;
        $this->body = $body;
        $this->info = $info;
        $this->deviceTokens = $deviceTokens;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $headers = [
            'Content-Type: application/json',
            'Authorization: key=' . config('app.fcm_key'),
        ];
        $data = [
            'registration_ids' => $this->deviceTokens,
            'collapse_key' => 'type_a',
            'notification' =>
                [
                    'body' => $this->body,
                    'title' => $this->title,
                ],
            'data' => $this->info
        ];

        $dataFields = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataFields);
        curl_exec($ch);
        curl_close($ch);
    }
}
